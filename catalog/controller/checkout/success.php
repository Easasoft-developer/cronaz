<?php /* Opencart Module v1.0 for Citrus Payment Gateway - Copyrighted file (viatechs.in) - Please do not modify/refactor/disasseble/extract any or all part content  */ ?>
<?php 
                      

	class ControllerCheckoutSuccess extends Controller 
	{ 
		public function index() 
		{ 




$this->load->model('account/address');

		$this->data['addresses'] = $this->model_account_address->getAddresses();



		require_once 'TransactionRequestBean.php';
           require_once 'TransactionResponseBean.php';
  
				$response = $_POST;


    if(is_array($response)){
        $str = $response['msg'];
    }else if(is_string($response) && strstr($response, 'msg=')){
        $outputStr = str_replace('msg=', '', $response);
        $outputArr = explode('&', $outputStr);
        $str = $outputArr[0];
    }else {
        $str = $response;
    }

    $transactionResponseBean = new TransactionResponseBean();

    $transactionResponseBean->setResponsePayload($str);
    $transactionResponseBean->setKey($_SESSION['key']);
    $transactionResponseBean->setIv($_SESSION['iv']);

    $response = $transactionResponseBean->getResponsePayload();
// print_r($order_info);
// print_r($response);
	$implodedResp = explode("|", $response);
$new_arary=array();
foreach($implodedResp as $spli){
$t=explode('=',$spli);
	$new_arary[$t[0]]=$t[1];
}
 
	



$this->data['txn_status1'] = $new_arary['txn_status'];
$this->data['txn_msg1'] = $new_arary['txn_msg'];
$this->data['clnt_txn_ref1'] = $new_arary['clnt_txn_ref'];
$this->data['tpsl_bank_cd1'] = $new_arary['tpsl_bank_cd'];
$this->data['txn_amt1'] = $new_arary['txn_amt'];
$this->data['clnt_rqst_meta1'] = $new_arary['clnt_rqst_meta'];
$this->data['tpsl_txn_time1'] = $new_arary['tpsl_txn_time'];
$this->data['tpsl_txn_id1'] = $new_arary['tpsl_txn_id'];

			if (isset($this->session->data['order_id'])) 
			{
				$this->cart->clear();
				unset($this->session->data['shipping_method']);
				unset($this->session->data['shipping_methods']);
				unset($this->session->data['payment_method']);
				unset($this->session->data['payment_methods']);
				unset($this->session->data['guest']);
				unset($this->session->data['comment']);
				unset($this->session->data['order_id']);
				unset($this->session->data['coupon']);
				unset($this->session->data['reward']);
				unset($this->session->data['voucher']);
				unset($this->session->data['vouchers']);
			}   
			$this->language->load('checkout/success');
			$this->document->setTitle($this->language->get('heading_title'));
			$this->data['breadcrumbs'] = array();       
			$this->data['breadcrumbs'][] = array('href'=> 
			$this->url->link('common/home'),'text'=> $this->language->get('text_home'), 
			'separator' => false);      
			$this->data['breadcrumbs'][] = array('href'=> 
			$this->url->link('checkout/cart'),'text'=> 
			$this->language->get('text_basket'),'separator' => 
			$this->language->get('text_separator'));
			$this->data['breadcrumbs'][] = array('href'=> 
			$this->url->link('checkout/checkout', '', 'SSL'),'text'=> 
			$this->language->get('text_checkout'),'separator' => 
			$this->language->get('text_separator'));
			$this->data['breadcrumbs'][] = array('href'=> 
			$this->url->link('checkout/success'),'text'=> 
			$this->language->get('text_success'),'separator' => 
			$this->language->get('text_separator')      );   
			$this->data['heading_title'] = $this->language->get('heading_title');
			if ($this->customer->isLogged()) 
			{ 
				$this->data['text_message'] = sprintf($this->language->get('text_customer'), 
				$this->url->link('account/account', '', 'SSL'), 
				$this->url->link('account/order', '', 'SSL'), 
				$this->url->link('account/download', '', 'SSL'), 
				$this->url->link('information/contact'));
			} 
			else 
			{    
				$this->data['text_message'] = sprintf($this->language->get('text_guest'), 
				$this->url->link('information/contact'));
			}    
			$this->data['button_continue'] = $this->language->get('button_continue');  
			$this->data['continue'] = $this->url->link('common/home');
			$this->data['Hello'] = "How are You";
			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/common/process.tpl')) 
			{
				$this->template = $this->config->get('config_template') . '/template/common/process.tpl';
			} 
			else 
			{
				$this->template = 'default/template/common/success.tpl';
			}
			$this->children = array('common/column_left','common/column_right','common/content_top','common/content_bottom','common/footer','common/header');
			$this->response->setOutput($this->render());  
		}
	}
?>
