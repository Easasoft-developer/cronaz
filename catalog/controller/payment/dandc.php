<?php


error_reporting(-1);
ini_set('display_errors', 'On');


class ControllerPaymentDandc extends Controller {
  protected function index() {

 $this->language->load('payment/dandc');
    $this->data['button_confirm'] = $this->language->get('button_confirm');
    // $this->data['action'] = 'http://localhost/Merchant_Site_Dir/SFAResponse.php';
  
    $this->load->model('checkout/order');
    $order_info = $this->model_checkout_order->getOrder($this->session->data['order_id']);



$this->data['currency'] = $order_info['currency_code'];

include("Sfa/BillToAddress.php");
include("Sfa/CardInfo.php");
include("Sfa/Merchant.php");
include("Sfa/MPIData.php");
include("Sfa/ShipToAddress.php");
include("Sfa/PGResponse.php");
include("Sfa/PostLibPHP.php");
include("Sfa/PGReserveData.php");

$oMPI       =   new MPIData();
$oCI      = new CardInfo();
$oPostLibphp  = new PostLibPHP();
$oMerchant    = new Merchant();
$oBTA     = new BillToAddress();
$oSTA     = new ShipToAddress();
$oPGResp    = new PGResponse();
$oPGReserveData = new PGReserveData();

$oMerchant->setMerchantDetails("96072293","96072293","96072293","10.10.10.238","CRON_".rand().$this->session->data['order_id'],"Ord1234","http://cronaz.com/index.php?route=payment/mine","POST","INR","INV123","req.Sale",$this->currency->format($order_info['total'], $this->data['currency'] , false, false),"","Ext1","true","Ext3","Ext4","New PHP");


// $this->currency->format($order_info['total'], $this->data['currency'] , false, false)

// setMerchantDetails($astrMerchantID,$astrVendor , $astrPartner,$astrCustIPAddress,
//             $astrMerchantTxnID,$astrOrderReferenceNo, $astrRespURL,
//             $astrRespMethod,$astrCurrCode,$astrInvoiceNo, $astrMessageType,
//             $astrAmount,$astrGMTTimeOffset,$astrExt1, $astrExt2,
//             $astrExt3, $astrExt4, $astrExt5)




$oBTA->setAddressDetails ("CID","Tester","Aline1","Aline2","Aline3","Pune","A.P","48927489","IND","tester@soft.com");



// setAddressDetails($astrAddrLine1, $astrAddrLine2, $astrAddrLine3, $astrCity, $astrState, $astrZip, $astrCountryAlpha,$astrEmail)

$oSTA->setAddressDetails (html_entity_decode($order_info['shipping_address_1'], ENT_QUOTES, 'UTF-8'),"Add2","Add3", html_entity_decode($order_info['shipping_city'], ENT_QUOTES, 'UTF-8'),html_entity_decode($order_info['shipping_iso_code_2'], ENT_QUOTES, 'UTF-8'),html_entity_decode($order_info['shipping_postcode'], ENT_QUOTES, 'UTF-8'),"IND",$order_info['email']);


// echo(html_entity_decode($order_info['shipping_address_1'], ENT_QUOTES, 'UTF-8'));


// exit();

#$oMPI->setMPIRequestDetails("1245","12.45","356","2","2 shirts","12","20011212","12","0","","image/gif, image/x-xbitmap, image/jpeg, image/pjpeg, application/vnd.ms-powerpoint, application/vnd.ms-excel, application/msword, application/x-shockwave-flash, */*","Mozilla/4.0 (compatible; MSIE 5.5; Windows NT 5.0)");


$oPGResp=$oPostLibphp->postSSL($oBTA,$oSTA,$oMerchant,$oMPI,$oPGReserveData);
// print_r($oPGResp);exit();

function redirect($url) {
  if(headers_sent()){
  ?>
    <html><head>
      <script language="javascript" type="text/javascript">
        window.self.location='<?php print($url);?>';
      </script>
    </head></html>
  <?php
    exit;
  } else {
    header("Location: ".$url);
    exit;
  }
}

// print_r($oPGResp->getRespCode());exit();
if($oPGResp->getRespCode() == '000' ){
  $url  =$oPGResp->getRedirectionUrl();
  #$url =~ s/http/https/;
  #print "Location: ".$url."\n\n";
  #header("Location: ".$url);
 // redirect($url);
   echo "<script>window.location = '".$url."'</script>";
}else{
  print "Error Occured.<br>";
  print "Error Code:".$oPGResp->getRespCode()."<br>";
  print "Error Message:".$oPGResp->getRespMessage()."<br>";
}

# This will remove all white space
#$oResp =~ s/\s*//g;

# $oPGResp->getResponse($oResp);

#print $oPGResp->getRespCode()."<br>";

#print $oPGResp->getRespMessage()."<br>";

#print $oPGResp->getTxnId()."<br>";

#print $oPGResp->getEpgTxnId()."<br>";

 if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/payment/dandc.tpl')){
        $this->template = $this->config->get('config_template') . '/template/payment/dandc.tpl';
      } else {
        $this->template = 'default/template/payment/dandc.tpl';
      }
  
      $this->render();

  
    if ($order_info) {


      $this->data['text_config_one'] = trim($this->config->get('text_config_one')); 
      $this->data['text_config_two'] = trim($this->config->get('text_config_two')); 
      $this->data['orderid'] = date('His') . $this->session->data['order_id'];
      $this->data['callbackurl'] = $this->url->link('payment/dandc/callback');
      $this->data['orderdate'] = date('YmdHis');
      $this->data['currency'] = $order_info['currency_code'];
      $this->data['orderamount'] = $this->currency->format($order_info['total'], $this->data['currency'] , false, false);
      $this->data['billemail'] = $order_info['email'];
      $this->data['billphone'] = html_entity_decode($order_info['telephone'], ENT_QUOTES, 'UTF-8');
      $this->data['billaddress'] = html_entity_decode($order_info['payment_address_1'], ENT_QUOTES, 'UTF-8');
      $this->data['billcountry'] = html_entity_decode($order_info['payment_iso_code_2'], ENT_QUOTES, 'UTF-8');
      $this->data['billprovince'] = html_entity_decode($order_info['payment_zone'], ENT_QUOTES, 'UTF-8');;
      $this->data['billcity'] = html_entity_decode($order_info['payment_city'], ENT_QUOTES, 'UTF-8');
      $this->data['billpost'] = html_entity_decode($order_info['payment_postcode'], ENT_QUOTES, 'UTF-8');
      $this->data['deliveryname'] = html_entity_decode($order_info['shipping_firstname'] . $order_info['shipping_lastname'], ENT_QUOTES, 'UTF-8');
      $this->data['deliveryaddress'] = html_entity_decode($order_info['shipping_address_1'], ENT_QUOTES, 'UTF-8');
      $this->data['deliverycity'] = html_entity_decode($order_info['shipping_city'], ENT_QUOTES, 'UTF-8');
      $this->data['deliverycountry'] = html_entity_decode($order_info['shipping_iso_code_2'], ENT_QUOTES, 'UTF-8');
      $this->data['deliveryprovince'] = html_entity_decode($order_info['shipping_zone'], ENT_QUOTES, 'UTF-8');
      $this->data['deliveryemail'] = $order_info['email'];
      $this->data['deliveryphone'] = html_entity_decode($order_info['telephone'], ENT_QUOTES, 'UTF-8');
      $this->data['deliverypost'] = html_entity_decode($order_info['shipping_postcode'], ENT_QUOTES, 'UTF-8');
      
    }


  }
  
  public function callback() {
    if (isset($this->request->post['orderid'])) {
      $order_id = trim(substr(($this->request->post['orderid']), 6));
    } else {
      die('Illegal Access');
    }
  
    $this->load->model('checkout/order');
    $order_info = $this->model_checkout_order->getOrder($order_id);
  
    if ($order_info) {
      $data = array_merge($this->request->post,$this->request->get);
  
      //payment was made successfully
      if ($data['status'] == 'Y' || $data['status'] == 'y') {
        // update the order status accordingly
      }
    }
  }


 
}
?>