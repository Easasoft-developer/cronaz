<!DOCTYPE html>
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
<head>
<meta charset="UTF-8" />
<title><?php echo $title; ?></title>
<base href="<?php echo $base; ?>" />
<?php if ($description) { ?>
<meta name="description" content="<?php echo $description; ?>" />
<?php } ?>
<?php if ($keywords) { ?>
<meta name="keywords" content="<?php echo $keywords; ?>" />
<?php } ?>
<?php if ($icon) { ?>
<link href="<?php echo $icon; ?>" rel="icon" />
<?php } ?>

<?php foreach ($links as $link) { ?>
<link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
<?php } ?>

<link href="catalog/view/theme/mycustomtheme/stylesheet/bootstrap.css" rel="stylesheet" type="text/css" />

<link href='http://fonts.googleapis.com/css?family=PT+Sans' rel='stylesheet' type='text/css'>

<link href="catalog/view/theme/mycustomtheme/stylesheet/style.css" rel="stylesheet" type="text/css" />

<?php foreach ($styles as $style) { ?>
<link rel="<?php echo $style['rel']; ?>" type="text/css" href="<?php echo $style['href']; ?>" media="<?php echo $style['media']; ?>" />
<?php } ?>

<!--<link href="catalog/view/theme/mycustomtheme/stylesheet/stylesheet.css" rel="stylesheet" type="text/css" />-->

<!--<script src="catalog/view/javascript/jquery/jquery.min.js" type="text/javascript"></script>-->

<script type="text/javascript" src="catalog/view/javascript/jquery/jquery-1.7.1.min.js"></script>
<script src="catalog/view/theme/mycustomtheme/js/bootstrap.js" type="text/javascript"></script>



<script type="text/javascript" src="catalog/view/javascript/jquery/ui/jquery-ui-1.8.16.custom.min.js"></script>
<link rel="stylesheet" type="text/css" href="catalog/view/javascript/jquery/ui/themes/ui-lightness/jquery-ui-1.8.16.custom.css" />
<script type="text/javascript" src="catalog/view/javascript/common.js"></script>
<?php foreach ($scripts as $script) { ?>
<script type="text/javascript" src="<?php echo $script; ?>"></script>
<?php } ?>
<script type="text/javascript">
$(document).ready(function(e) {
    $(".carts").mouseover(function(){
		$(".minicart").css("display","block");
		
	});
	
	$(".carts").mouseleave(function(e) {
       $(".minicart").css("display","none"); 
    });
	
});


/*$(document).ready(function() {
 
    var div = $('#secondaryNavigation');
    var start = $(div).offset().top;
 
    $.event.add(window, "scroll", function() {
        var p = $(window).scrollTop();
        $(div).css('position',((p)>start) ? 'fixed' : 'static');
        $(div).css('top',((p)>start) ? '0px' : '');
		$(div).css('box-shadow',((p)>start) ? 'rgba(0, 0, 0, 0.3) 0px 1px 10px 0px' : '');
		$(".bannerContainer").css('margin-top',((p)>start) ? '56px' : '');
    });
 
});*/



</script>
</head>



<body>

<div class="topnav">

	<div class="container">

    	<ul class="topleftnav col-lg-3">

        	<li><a href="" class="active">Home</a></li>

            <li><a href="<?php echo HTTP_SERVER ; ?>index.php?route=information/information&information_id=4">Company</a></li>

            <li><a href="<?php echo HTTP_SERVER ; ?>index.php?route=information/contact">Contact Us</a></li>

        </ul>

        <ul class="toprightnav pull-right">
        
       

    <li><a href=""><?php if (!$logged) 
    { ?><span class="clrorg"><?php echo $text_welcome; ?></span></a> <?php 
    } 
    else { 
    ?>
    <?php echo $text_logged; ?>
    <?php 
    } 
    ?></li>
    
    <li> <a href="<?php echo $account; ?>"><?php echo $text_account; ?></a></li>
    
    <li class="carts"><a href="<?php echo $shopping_cart; ?>"><span class="glyphicon glyphicon-shopping-cart cart-icon"><span class="count" id="cart-total"><?php echo count($this->cart->getProducts());?></span></span></a>
    
    <div style="width: 400px;position: absolute;top: 40px;right: 195px;z-index: 99; background-color:#FFFFFF; display:none" class="minicart">
<?php
if(count($this->cart->getProducts())==0)
{
?>
<p style="text-align:center; padding:20; border: solid 1px #f4f4f4">shopping cart empty</p>
<?php
}
else
{
$products=$this->cart->getProducts();
?>


    <div class="cart-info">
      <table class="table-responsive" width="100%">
        <thead bgcolor="#f4f4f4">
          <tr>
            <td class="name text-center fs14"><strong>Product Name</strong></td>
            <td class="quantity text-center fs14"><strong>Quantity</strong></td>
            <td class="price text-center fs14"><strong>Unit Price</strong></td>
            <td class="total text-center fs14"><strong>Total</strong></td>
            <td class="total text-center fs14"><strong>Remove</strong></td>
          </tr>
        </thead>
        <tbody>
          <?php foreach ($products as $product) { ?>
            <?php if($product['recurring']): ?>
              <tr>
                  <td colspan="6" style="border:none;"><image src="catalog/view/theme/default/image/reorder.png" alt="" title="" style="float:left;" /><span style="float:left;line-height:18px; margin-left:10px;"> 
                      <strong><?php echo $text_recurring_item ?></strong>
                      <?php echo $product['profile_description'] ?>
                  </td>
                </tr>
            <?php endif; ?>
          <tr style="border-bottom:1px solid #f4f4f4">
           
            <td class="name text-center"><big class="product-name clrorg">
            <?php echo $product['name']; ?></big>
            
              
             </td>
           <!-- <td class="model text-center product-name"><?php echo $product['model']; ?></td>-->
            <td class="quantity text-center">
            
            <?php echo $product['quantity']; ?>
           <!-- <input type="text" readonly name="quantity[<?php echo $product['key']; ?>]" value="<?php echo $product['quantity']; ?>" size="1" />-->
              <!--&nbsp;
              <input type="image" src="catalog/view/theme/default/image/update.png" alt="<?php echo $button_update; ?>" title="<?php echo $button_update; ?>" />-->
             </td>
            <td class="price text-center"><?php echo $product['price']; ?></td>
            <td class="total text-center"><?php echo $product['total']; ?></td>
            <td> <a href="<?php echo HTTP_SERVER .'index.php?route=checkout/cart&remove='.$product['key'] ?>">
            <img src="catalog/view/theme/default/image/remove.png" alt="" /></a>
            </td>
          </tr>
          <?php } ?>
          
        </tbody>
      </table>
      <?php
      }
      ?>
    </div>
    
    
    </li>
    <li><a href="<?php echo $wishlist; ?>"><span class="glyphicon glyphicon-heart clrwht cart-icon">
    <span class="count" id="wishlist-total"> <?php echo isset($this->session->data['wishlist']) ? count($this->session->data['wishlist']) : 0;?></span></span></a></li>
    
    <li><a href="<?php echo $cuscompare; ?>"><span class="glyphicon glyphicon-refresh clrwht cart-icon">
     <span class="count" id="compare-total">
     <?php echo isset($this->session->data['compare']) ? count($this->session->data['compare']) : 0;?></span>
    </span></a></li>
</ul>


        </div>

    </div><!--container-->
</div><!--topnav-->





<?php if ($error) { ?>
    
    <div class="warning"><?php echo $error ?><img src="catalog/view/theme/default/image/close.png" alt="" class="close" /></div>
    
<?php } ?>
<div id="notification"></div>
<div id="banner" class="mb48">

<div class="bannerbg">
<?php
$url = "$_SERVER[REQUEST_URI]";
/*if($url=='/index.php?route=common/home' || $url=='/new-cronaz/')*/
if($url=='/index.php?route=common/home' || $url=='/index.php')
{
?>
<img src="catalog/view/theme/mycustomtheme/images/slide1.jpg" class="img-responsive" alt="" />
<?
}else{
?>
<img src="catalog/view/theme/mycustomtheme/images/inner-banner.jpg" class="img-responsive" alt="" />

<?php
}
?>
 </div>



<div class="navigation" id="secondaryNavigation">

<nav class="navbar navbar-default nav-tranparent">

  <div class="container">

    <!-- Brand and toggle get grouped for better mobile display -->

    <div class="navbar-header">

  
        
        <a class="navbar-brand hidden-lg hidden-md hidden-sm" href="<?php echo $home; ?>">
        <img src="<?php echo $logo; ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" class="img-responsive" />
        </a>

      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">

        <span class="sr-only">Toggle navigation</span>

        <span class="icon-bar"></span>

        <span class="icon-bar"></span>

        <span class="icon-bar"></span>

      </button>

    </div>



    <!-- Collect the nav links, forms, and other content for toggling -->

    <div class="collapse navbar-collapse p0" id="bs-example-navbar-collapse-1">

      <ul class="nav navbar-nav col-lg-4 mt10">
      
      
      <?php 
      $m=0;
      foreach ($categories as $category) { 
      if(2 >= $m)
      {
      ?>
    <li><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a>
      
    </li>
    <?php 
    }
    $m++;
    }
     ?>
      

  </ul>

    <?php if ($logo) { ?>
<a href="<?php echo $home; ?>"><img src="<?php echo $logo; ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" class="navbar-brand m0 hidden-xs p0 col-lg-3"  style="margin:0 auto" /></a>
  <?php } ?>   

 

      

      <ul class="nav navbar-nav navbar-right col-lg-5 mt10">
<?php
     $l=0;
      foreach ($categories as $category) { 
      if($l > 2)
      {
      ?>
    <li><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a><li>
      
    </li>
    <?php 
    }
    $l++;
    }
?>
        

        	<!--<form class="navbar-form navbar-right" role="search">

              <div class="form-group nav-search">

                <input type="text" class="form-control" placeholder="Search" name="search" id="search">

                <label for="email" class="glyphicon glyphicon-search search-icon" rel="search" title="search"></label>

              </div>

            </form>-->

           

        </li>

      </ul>

      

    </div><!-- /.navbar-collapse -->

    <!--<hr class="hidden-xs" />-->
    
   <!--<div class="row" style="margin-left:0px; margin-right:0px;border: solid 1px #f4f4f4;
border-radius: 2px; margin-top:20px">
   <div class="col-md-12" style="background-color:#FFF">
   <div class="col-md-6">
   <ul style="line-height:25px; font-size:14px">
   <?php
   for($i=0;$i<=2;$i++)
   {
   ?>
   <li style="font-weight:bold">Watches</li>
   <li>Watches</li>
   <li>Watches</li>
   <li>Watches</li>
   <li>Watches</li>
   <?php
   }
   ?>
   </ul>
   
   </div>
   
    <div class="col-md-6">dsgsdgsg</div>
   </div>
   </div> -->
    

  </div><!-- /.container -->

</nav>

</div><!--navigation-->



</div><!--banner-->




<div class="clearfix"></div>



</div>
  