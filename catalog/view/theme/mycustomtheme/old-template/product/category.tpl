<?php echo $header; ?>
<div class="container">
  <ul class="crumb hidden-xs mb28">
    	<!--<li><a href="">Home</a></li>
        <li><a href="">Men</a></li>
        <li class="active">Signature</li>-->
        
        
<?php foreach ($breadcrumbs as $breadcrumb) { ?>

    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    </li>

    <?php } ?>
        
    </ul>  </div>
<?php echo $column_left; ?>


<div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">

<h1 class="tittle clrorg"><?php echo $heading_title; ?></h1>

  <?php if ($thumb || $description) { ?>

  <div class="category-info">

    <?php if ($thumb) { ?>

    <div class="image"><img src="<?php echo $thumb; ?>" alt="<?php echo $heading_title; ?>" /></div>

    <?php } ?>

    <?php if ($description) { ?>

    <?php echo $description; ?>

    <?php } ?>

  </div>

  <?php } ?>

  <?php if ($categories) { ?>

  <h2 class="tittle"><?php //echo $text_refine; ?></h2>

  <div class="category-list">

    <?php if (count($categories) <= 5) { ?>

    <ul>

      <?php 
     
      foreach ($categories as $category) { ?>

      <li style="float:left; border: solid 1px #f4f4f4; margin-right:10px; margin-bottom:20px">
      <a href="<?php echo $category['href']; ?>">
      <?php
      if($category['catimage']=='')
      {
      ?>
            <img src="<?php echo HTTP_SERVER;?>image/cache/no_image-100x100.jpg" />

      <?
      }else{?>
      
            <img src="<?php echo $category['catimage']; ?>" />

      <?php
      }
      ?>
     <div style="text-align:center"><?php echo $category['name']; ?></div>
     </a>
</li>
      <?php } ?>

    </ul>

    <?php } else { ?>

    <?php for ($i = 0; $i < count($categories);) { ?>

    <ul>

      <?php $j = $i + ceil(count($categories) / 4); ?>

      <?php for (; $i < $j; $i++) { ?>

      <?php if (isset($categories[$i])) { ?>

      <li><a href="<?php echo $categories[$i]['href']; ?>"><?php echo $categories[$i]['name']; ?></a></li>

      <?php } ?>

      <?php } ?>

    </ul>

    <?php } ?>

    <?php } ?>

  </div>

  <?php } ?>

  <?php if ($products) { ?>

  <div class="product-filter mb28">

    <div class="display"><b><?php echo $text_display; ?></b> <?php echo $text_list; ?><span class="glyphicon glyphicon-list"></span> <b>/</b> <a onclick="display('grid');"><?php echo $text_grid; ?><span class="glyphicon glyphicon-th"></span></a></div>

    <div class="limit"><b><?php echo $text_limit; ?></b>

      <select onchange="location = this.value;">

        <?php foreach ($limits as $limits) { ?>

        <?php if ($limits['value'] == $limit) { ?>

        <option value="<?php echo $limits['href']; ?>" selected="selected"><?php echo $limits['text']; ?></option>

        <?php } else { ?>

        <option value="<?php echo $limits['href']; ?>"><?php echo $limits['text']; ?></option>

        <?php } ?>

        <?php } ?>

      </select>

    </div>
	<div class="product-compare"><a href="<?php echo $compare; ?>" id="compare-total"><?php echo $text_compare; ?></a></div>
	
    <div class="sort"><b><?php echo $text_sort; ?></b>

      <select onchange="location = this.value;">

        <?php foreach ($sorts as $sorts) { ?>

        <?php if ($sorts['value'] == $sort . '-' . $order) { ?>

        <option value="<?php echo $sorts['href']; ?>" selected="selected"><?php echo $sorts['text']; ?></option>

        <?php } else { ?>

        <option value="<?php echo $sorts['href']; ?>"><?php echo $sorts['text']; ?></option>

        <?php } ?>

        <?php } ?>

      </select>

    </div>

  </div>

  

      <ul class="arrivals-lsting col-lg-12 col-md-12 col-sm-12 col-xs-12 row">


    <?php foreach ($products as $product) { ?>
    
    
            
                <li class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                  
                     <?php if ($product['thumb']) { ?>

      <div class="productinner">
      <a href="<?php echo $product['href'];?>">
      <img src="<?php echo $product['thumb']; ?>" title="<?php echo $product['name']; ?>" alt="<?php echo $product['name']; ?>" class="img-responsive" />
      </a>
      </div>

      <?php } ?>
                 
                    <dl>
                        <dt><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></dt>
                        
                        
                        
              
                        
                        
      <?php if ($product['price']) { ?>

      <dd class="price"><span class="rupee">`</span>

        <?php if (!$product['special']) { ?>

        <?php echo $product['price']; ?>

        <?php } else { ?>

        <dd class="price clrorg"><?php echo $product['price']; ?></dd>
        <span class="price-new"><?php echo $product['special']; ?></span>

        <?php } ?>

        <?php if ($product['tax']) { ?>

        <br />

        <span class="price-tax"><?php echo $text_tax; ?> <?php echo $product['tax']; ?></span>

        <?php } ?>

      </dd>

      <?php } ?>
                        
                    </dl>
                   
             
               
            
    
    
    

   

     



      <?php if ($product['rating']) { ?>

      <div class="rating"><img src="catalog/view/theme/default/image/stars-<?php echo $product['rating']; ?>.png" alt="<?php echo $product['reviews']; ?>" /></div>

      <?php } ?>

      <div class="mb20">

        <input type="button" value="<?php echo $button_cart; ?>" onclick="addToCart('<?php echo $product['product_id']; ?>');" class="cart" />

      </div>
	  <div class="text-center mb20">
      <div class="wishlist"><a onclick="addToWishList('<?php echo $product['product_id']; ?>');"  data-toggle="tooltip" data-placement="top" data-html="true" title="Add To Wish List"><span class="glyphicon glyphicon-heart"></span></a></div>

      <div class="compare"><a onclick="addToCompare('<?php echo $product['product_id']; ?>');" data-toggle="tooltip" data-placement="top" data-html="true" title="Add To Compare	"><span class="glyphicon glyphicon-refresh"></span></a></div>

    </div>

    <?php } ?>

</li><!--product-->
</ul>
  <div class="pagination"><?php echo $pagination; ?></div>

  <?php } ?>

  <?php if (!$categories && !$products) { ?>

  <div class="content fs26"><?php echo $text_empty; ?></div>

  <div class="buttons">

    <div class="right text-center mt25"><a href="<?php echo $continue; ?>" class="button continue-btn"><?php echo $button_continue; ?></a></div>

  </div>
   
  <?php } ?>




</div>
  <?php echo $content_bottom; ?>

<script type="text/javascript"><!--

 $('[data-toggle="tooltip"]').tooltip();   


function display(view) {

	if (view == 'list') {

		$('.product-grid').attr('class', 'product-list');

		

		$('.product-list > div').each(function(index, element) {

			html  = '<div class="right">';

			html += '  <div class="cart">' + $(element).find('.cart').html() + '</div>';

			html += '  <div class="wishlist">' + $(element).find('.wishlist').html() + '</div>';

			html += '  <div class="compare">' + $(element).find('.compare').html() + '</div>';

			html += '</div>';			

			

			html += '<div class="left">';

			

			var image = $(element).find('.image').html();

			

			if (image != null) { 

				html += '<div class="image">' + image + '</div>';

			}

			

			var price = $(element).find('.price').html();

			

			if (price != null) {

				html += '<div class="price">' + price  + '</div>';

			}

					

			html += '  <div class="name">' + $(element).find('.name').html() + '</div>';

			html += '  <div class="description">' + $(element).find('.description').html() + '</div>';

			

			var rating = $(element).find('.rating').html();

			

			if (rating != null) {

				html += '<div class="rating">' + rating + '</div>';

			}

				

			html += '</div>';

						

			$(element).html(html);

		});		

		

		$('.display').html('<b><?php echo $text_display; ?></b> <?php echo $text_list; ?> <b>/</b> <a onclick="display(\'grid\');"><?php echo $text_grid; ?></a>');

		

		$.totalStorage('display', 'list'); 

	} else {

		$('.product-list').attr('class', 'product-grid');

		

		$('.product-grid > div').each(function(index, element) {

			html = '';

			

			var image = $(element).find('.image').html();

			

			if (image != null) {

				html += '<div class="image">' + image + '</div>';

			}

			

			html += '<div class="name">' + $(element).find('.name').html() + '</div>';

			html += '<div class="description">' + $(element).find('.description').html() + '</div>';

			

			var price = $(element).find('.price').html();

			

			if (price != null) {

				html += '<div class="price">' + price  + '</div>';

			}

			

			var rating = $(element).find('.rating').html();

			

			if (rating != null) {

				html += '<div class="rating">' + rating + '</div>';

			}

						

			html += '<div class="cart">' + $(element).find('.cart').html() + '</div>';

			html += '<div class="wishlist">' + $(element).find('.wishlist').html() + '</div>';

			html += '<div class="compare">' + $(element).find('.compare').html() + '</div>';

			

			$(element).html(html);

		});	

					

		$('.display').html('<b><?php echo $text_display; ?></b> <a onclick="display(\'list\');"><?php echo $text_list; ?></a> <b>/</b> <?php echo $text_grid; ?>');

		

		$.totalStorage('display', 'grid');

	}

}



view = $.totalStorage('display');



if (view) {

	display(view);

} else {

	display('list');

}

//--></script> 
 </div>
    </div>        
            
<?php echo $footer; ?>