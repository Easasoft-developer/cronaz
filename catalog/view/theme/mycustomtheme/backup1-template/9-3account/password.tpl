<?php echo $header; ?><?php echo $column_left; ?><?php echo $column_right; ?>

<div id="content" class="col-lg-9"><?php echo $content_top; ?>

  <ul class="crumb">

    <?php foreach ($breadcrumbs as $breadcrumb) { ?>

    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>

    <?php } ?>

  </ul>

  <h1 class="clrorg mt53"><?php echo $heading_title; ?></h1>

  <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">

    <h2><?php echo $text_password; ?></h2>

    <div class="content">

      <table class="form" width="70%">

        <tr>

          <td><span class="required">*</span> <big><?php echo $entry_password; ?></big></td>

          <td><input type="password" name="password" value="<?php echo $password; ?>" class="col-lg-12 login" />

            <?php if ($error_password) { ?>

            <span class="error"><?php echo $error_password; ?></span>

            <?php } ?></td>

        </tr>

        <tr>

          <td><span class="required">*</span> <big><?php echo $entry_confirm; ?></big></td>

          <td><input type="password" name="confirm" value="<?php echo $confirm; ?>" class="col-lg-12 login" />

            <?php if ($error_confirm) { ?>

            <span class="error"><?php echo $error_confirm; ?></span>

            <?php } ?></td>

        </tr>

      </table>

    </div>

    <div class="buttons mt25">

      <div class="pull-left mr138"><a href="<?php echo $back; ?>" class="button back-btn"><?php echo $button_back; ?></a></div>

      <div class="text-center">

        <input type="submit" value="<?php echo $button_continue; ?>" class="button continue-btn" />

      </div>

    </div>

  </form>
  </div>

  <?php echo $content_bottom; ?></div>

<?php echo $footer; ?>