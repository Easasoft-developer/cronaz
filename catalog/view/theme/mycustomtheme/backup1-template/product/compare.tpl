<?php echo $header; ?>

<?php if ($success) { ?>

<div class="success"><?php echo $success; ?><img src="catalog/view/theme/default/image/close.png" alt="" class="close" /></div>

<?php } ?>

<?php echo $column_left; ?><?php echo $column_right; ?>

<div id="content" class="container mb28"><?php echo $content_top; ?>

  <ul class="crumb">

    <?php foreach ($breadcrumbs as $breadcrumb) { ?>

    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>

    <?php } ?>

  </ul>

  <h1 style="margin-top: 30px;" class="tittle clrorg cps"><?php echo $heading_title; ?></h1>

  <?php if ($products) { ?>

  <table class="compare-info table-bordered">

    <thead>

      <tr>

        <td style="padding:10px" class="compare-product tittle" colspan="<?php echo count($products) + 1; ?>"><?php echo $text_product; ?></td>

      </tr>

    </thead>

    <tbody>

      <tr>

        <td style="padding:10px"><?php echo $text_name; ?></td>

        <?php foreach ($products as $product) { ?>

        <td style="padding:10px" class="name product-name"><a href="<?php echo $products[$product['product_id']]['href']; ?>" class="clrorg"><?php echo $products[$product['product_id']]['name']; ?></a></td>

        <?php } ?>

      </tr>

      <tr>

        <td style="padding:10px"><?php echo $text_image; ?></td>

        <?php foreach ($products as $product) { ?>

        <td style="padding:10px"><?php if ($products[$product['product_id']]['thumb']) { ?>

          <img src="<?php echo $products[$product['product_id']]['thumb']; ?>" alt="<?php echo $products[$product['product_id']]['name']; ?>" />

          <?php } ?></td>

        <?php } ?>

      </tr>

      <tr>

        <td style="padding:10px"><?php echo $text_price; ?></td>

        <?php foreach ($products as $product) { ?>

        <td style="padding:10px"><?php if ($products[$product['product_id']]['price']) { ?>

          <?php if (!$products[$product['product_id']]['special']) { ?>

          <?php echo $products[$product['product_id']]['price']; ?>

          <?php } else { ?>

          <span class="price-old"><?php echo $products[$product['product_id']]['price']; ?></span> <span class="price-new"><?php echo $products[$product['product_id']]['special']; ?></span>

          <?php } ?>

          <?php } ?></td>

        <?php } ?>

      </tr>

      <tr>

        <td style="padding:10px"><?php echo $text_model; ?></td>

        <?php foreach ($products as $product) { ?>

        <td style="padding:10px"><?php echo $products[$product['product_id']]['model']; ?></td>

        <?php } ?>

      </tr>

      <tr>

        <td style="padding:10px"><?php echo $text_manufacturer; ?></td>

        <?php foreach ($products as $product) { ?>

        <td style="padding:10px"><?php echo $products[$product['product_id']]['manufacturer']; ?></td>

        <?php } ?>

      </tr>

      <tr>

        <td style="padding:10px"><?php echo $text_availability; ?></td>

        <?php foreach ($products as $product) { ?>

        <td style="padding:10px"><?php echo $products[$product['product_id']]['availability']; ?></td>

        <?php } ?>

      </tr>

	  <?php if ($review_status) { ?>

      <tr>

        <td style="padding:10px"><?php echo $text_rating; ?></td>

        <?php foreach ($products as $product) { ?>

        <td style="padding:10px"><img src="catalog/view/theme/default/image/stars-<?php echo $products[$product['product_id']]['rating']; ?>.png" alt="<?php echo $products[$product['product_id']]['reviews']; ?>" /><br />

          <?php echo $products[$product['product_id']]['reviews']; ?></td>

        <?php } ?>

      </tr>

      <?php } ?>

	  <tr>

        <td style="padding:10px"><?php echo $text_summary; ?></td>

        <?php foreach ($products as $product) { ?>

        <td style="padding:10px" class="description"><?php echo $products[$product['product_id']]['description']; ?></td>

        <?php } ?>

      </tr>

      <tr>

        <td style="padding:10px"><?php echo $text_weight; ?></td>

        <?php foreach ($products as $product) { ?>

        <td style="padding:10px"><?php echo $products[$product['product_id']]['weight']; ?></td>

        <?php } ?>

      </tr>

      <tr>

        <td style="padding:10px"><?php echo $text_dimension; ?></td>

        <?php foreach ($products as $product) { ?>

        <td style="padding:10px"><?php echo $products[$product['product_id']]['length']; ?> x <?php echo $products[$product['product_id']]['width']; ?> x <?php echo $products[$product['product_id']]['height']; ?></td>

        <?php } ?>

      </tr>

    </tbody>

    <?php foreach ($attribute_groups as $attribute_group) { ?>

    <thead>

      <tr>

        <td style="padding:10px" class="compare-attribute" colspan="<?php echo count($products) + 1; ?>"><?php echo $attribute_group['name']; ?></td>

      </tr>

    </thead>

    <?php foreach ($attribute_group['attribute'] as $key => $attribute) { ?>

    <tbody>

      <tr>

        <td style="padding:10px"><?php echo $attribute['name']; ?></td>

        <?php foreach ($products as $product) { ?>

        <?php if (isset($products[$product['product_id']]['attribute'][$key])) { ?>

        <td style="padding:10px"><?php echo $products[$product['product_id']]['attribute'][$key]; ?></td>

        <?php } else { ?>

        <td style="padding:10px"></td>

        <?php } ?>

        <?php } ?>

      </tr>

    </tbody>

    <?php } ?>

    <?php } ?>

    <tr>

      <td style="padding:10px"></td>

      <?php foreach ($products as $product) { ?>

      <td style="padding:10px"><input type="button" value="<?php echo $button_cart; ?>" onclick="addToCart('<?php echo $product['product_id']; ?>');" class="button cart continue-btn" style=" margin-bottom:10px;"/>
      
      <a href="<?php echo $product['remove']; ?>" class="removec-btn" style="margin-bottom:10px"><?php echo $button_remove; ?></a>
      
      </td>

      <?php } ?>

    </tr>

   <!-- <tr>

      <td></td>

      <?php foreach ($products as $product) { ?>

      <td class="remove"><a href="<?php echo $product['remove']; ?>" class="button cart continue-btn"><?php echo $button_remove; ?></a></td>

      <?php } ?>

    </tr>-->

  </table>

  <div class="buttons mb28 text-center mt10">

    <div style="float:left; margin-top:20px"><a href="<?php echo $continue; ?>" class="button continue-btn"><?php echo $button_continue; ?></a></div>

  </div>

  <?php } else { ?>

  <div class="content"><?php echo $text_empty; ?></div>

  <div class="buttons mt25">

    <div class="right"><a href="<?php echo $continue; ?>" class="button continue-btn"><?php echo $button_continue; ?></a></div>

  </div>

  <?php } ?>

  <?php echo $content_bottom; ?></div>

<?php echo $footer; ?>