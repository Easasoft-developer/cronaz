<?php echo $header; ?>


<?php echo $column_left; ?>
<?php echo $column_right; ?>

<div id="content" class="col-lg-9 row"><?php echo $content_top; ?>

  <ul class="crumb hidden-xs">

    <?php foreach ($breadcrumbs as $breadcrumb) { ?>

    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>

    <?php } ?></li>

  </ul>
  
  <div class="col-lg-12 row">
  	<h1 class="clrorg col-lg-6 row mt53 tittle" style="text-transform: uppercase;"><?php echo $heading_title; ?></h1>
        <div class="col-lg-6 mt25 pull-right">
		<?php if ($success) { ?>

        <div class="success alert alert-success"><a href="#" class="close" data-dismiss="alert">&times;</a><?php echo $success; ?></div>
        
        <?php } ?>
        
        <?php if ($error_warning) { ?>
        
        <div class="warning alert alert-warning"><a href="#" class="close" data-dismiss="alert">&times;</a><?php echo $error_warning; ?></div>
        
        <?php } ?>
        
    	</div>
        
    </div>


  <!--<h1 class="clrorg"><?php echo $heading_title; ?></h1>-->

  <div class="login-content col-lg-12 row">

    <div class="left col-lg-6 row">

      <h4 class="mb20 mt20"><strong><?php echo $text_new_customer; ?></strong></h4>

      <div class="content">

        <p class=""><big><strong><?php echo $text_register; ?></strong></big></p>

        <p class="mb28 fs14"><?php echo $text_register_account; ?></p>

        <a href="<?php echo $register; ?>" class="button continue-btn"><?php echo $button_continue; ?></a></div>

    </div>
		
    <div class="right col-lg-6 pull-right login-bg">
    

      <h4 class="mb20 mt20"><strong><?php echo $text_returning_customer; ?></strong></h4>

      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">

        <div class="content">

          <p class="fs14"><?php echo $text_i_am_returning_customer; ?></p>

          <p class="fs14" style="margin: 0;"><strong><?php echo $entry_email; ?></strong></p>

          <input type="text" name="email" value="<?php echo $email; ?>" class="col-lg-12 login" style="margin-bottom: 5px;"/>

          
          <p class="fs14" style="margin: 0;"><strong><?php echo $entry_password; ?></strong></p>

          <input type="password" name="password" value="<?php echo $password; ?>" class="col-lg-12 login" style="margin-bottom: 5px;" />



          <p class="fs14"><a href="<?php echo $forgotten; ?>"><?php echo $text_forgotten; ?></a></p>

          <input type="submit" value="<?php echo $button_login; ?>" class="button continue-btn" />

          <?php if ($redirect) { ?>

          <input type="hidden" name="redirect" value="<?php echo $redirect; ?>" />

          <?php } ?>

        </div>

      </form>

    </div>

  </div>

  <?php echo $content_bottom; ?></div>
</div>
<script type="text/javascript"><!--

$('#login input').keydown(function(e) {

	if (e.keyCode == 13) {

		$('#login').submit();

	}

});

//--></script> 

<?php echo $footer; ?>