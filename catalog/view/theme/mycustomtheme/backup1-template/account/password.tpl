<?php echo $header; ?><?php echo $column_left; ?><?php echo $column_right; ?>

<div id="content" class="col-lg-9"><?php echo $content_top; ?>

  <ul class="crumb">

    <?php foreach ($breadcrumbs as $breadcrumb) { ?>

    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>

    <?php } ?>

  </ul>

  <h1 class="clrorg mt53 tittle cps"><?php echo $heading_title; ?></h1>

  <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">

    <h4 class="mt20"><strong><?php echo $text_password; ?></strong></h4>

    <div class="content">

      <table class="form" width="70%">

        <tr>

          <td><p style="margin-top: 10px;"><span class="required">*</span><?php echo $entry_password; ?></p></td>

          <td><input type="password" name="password" value="<?php echo $password; ?>" class="col-lg-12 login" />

            <?php if ($error_password) { ?>

            <span class="error"><?php echo $error_password; ?></span>

            <?php } ?></td>

        </tr>

        <tr>

          <td><p style="margin-top: 10px;"><span class="required">*</span><?php echo $entry_confirm; ?></p></td>

          <td><input type="password" name="confirm" value="<?php echo $confirm; ?>" class="col-lg-12 login" />

            <?php if ($error_confirm) { ?>

            <span class="error"><?php echo $error_confirm; ?></span>

            <?php } ?></td>

        </tr>

      </table>

    </div>

    <div class="buttons mt25">

      <!-- <div class="pull-left mr10"><a href="<?php echo $back; ?>" class="button back-btn"><?php echo $button_back; ?></a></div> -->

      <div class="pull-left">

        <input type="submit" value="<?php //echo $button_continue; ?>Update" class="button continue-btn" />

      </div>

    </div>

  </form>
  </div>

  <?php echo $content_bottom; ?></div>

<?php echo $footer; ?>