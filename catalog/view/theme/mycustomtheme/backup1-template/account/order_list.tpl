<?php echo $header; ?><?php echo $column_left; ?><?php echo $column_right; ?>

<div id="content" class="col-lg-9"><?php echo $content_top; ?>

  <ul class="crumb">

    <?php foreach ($breadcrumbs as $breadcrumb) { ?>

    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>

    <?php } ?>

  </ul>

  <h1 class="clrorg mt53 tittle mb20 cps"><?php echo $heading_title; ?></h1>
  
  <?php if ($orders) { ?>

  <?php foreach ($orders as $order) { ?>

  <div class="order-list">
  <table class="table-bordered" width="100%">
  <tbody>
  <tr style="background-color:#E4E4E4">

    <td style="padding:10px;" class="order-id"><b><?php echo $text_order_id; ?></b> #<?php echo $order['order_id']; ?></td>
	
    <td style="padding:10px;" class="order-status"><b><?php echo $text_status; ?></b> <?php echo $order['status']; ?></td>
     <td class="order-status"><b>Action</td>
    
</tr>
<tr>
    <div class="order-content">

      <td style="padding:10px;"><div><b style="display: inline-block;padding-bottom: 10px;"><?php echo $text_date_added; ?></b> <?php echo $order['date_added']; ?><br />

        <b><?php echo $text_products; ?></b> <?php echo $order['products']; ?></div></td>

      <td style="padding:10px;"><div><b style="display: inline-block;padding-bottom: 10px;"><?php echo $text_customer; ?></b> <?php echo $order['name']; ?><br />

        <b><?php echo $text_total; ?></b> <?php echo $order['total']; ?></div></td>
       

      <td style="padding:10px;"><div class="order-info"><a href="<?php echo $order['href']; ?>"><img src="catalog/view/theme/default/image/info.png" alt="<?php echo $button_view; ?>" title="<?php echo $button_view; ?>" /></a>&nbsp;&nbsp;<a href="<?php echo $order['reorder']; ?>"><img src="catalog/view/theme/default/image/reorder.png" alt="<?php echo $button_reorder; ?>" title="<?php echo $button_reorder; ?>" /></a></div></td>
</tr>
    </div>
    </tbody>
  </table>

  </div>
  

  <?php } ?>

  

  <?php } else { ?>

  <div class="content fs14"><p><?php echo $text_empty; ?></p></div>

  <?php } ?>
  

  <!-- <div class="buttons mt25">

    <div class="right"><a href="<?php echo $continue; ?>" class="button continue-btn"><?php echo $button_continue; ?></a></div>

  </div> -->
  </div>
<div class="pagination"><?php echo $pagination; ?></div>
  <?php echo $content_bottom; ?></div>

<?php echo $footer; ?>