<?php echo $header; ?>
<?php if ($success) { ?>
<div class="success"><?php echo $success; ?></div>
<?php } ?>
<?php echo $column_left; ?>
<?php echo $column_right; ?>
<div id="content" class="col-lg-9">
<?php echo $content_top; ?>

  <ul class="crumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>
  <h1 class="clrorg mt53 tittle cps"><?php echo $heading_title; ?></h1>
  <div class="row">
  <div class="col-lg-4">
  <h4 class="mt20"><strong><?php echo $text_my_account; ?></strong></h4>
  <div class="content">
    <ul class="list">
      <li><a href="<?php echo $edit; ?>"><?php echo $text_edit; ?></a></li>
      <li><a href="<?php echo $password; ?>"><?php echo $text_password; ?></a></li>
      <li><a href="<?php echo $address; ?>"><?php echo $text_address; ?></a></li>
      <li><a href="<?php echo $wishlist; ?>"><?php echo $text_wishlist; ?></a></li>
    </ul>
  </div>
  </div>
  <div class="col-lg-4">
  <h4 class="mt20"><strong><?php echo $text_my_orders; ?></strong></h4>
  <div class="content">
    <ul class="list">
      <li><a href="<?php echo $order; ?>"><?php echo $text_order; ?></a></li>
      <!--<li><a href="<?php echo $download; ?>"><?php echo $text_download; ?></a></li>-->
      <!--<?php if ($reward) { ?>
      <li><a href="<?php echo $reward; ?>"><?php echo $text_reward; ?></a></li>
      <?php } ?>-->
      <li><a href="<?php echo $return; ?>"><?php echo $text_return; ?></a></li>
     <!-- <li><a href="<?php echo $transaction; ?>"><?php echo $text_transaction; ?></a></li>
      <li><a href="<?php echo $recurring; ?>"><?php echo $text_recurring; ?></a></li>-->
    </ul>
  </div>
  </div>
  <div class="col-lg-4">
  <h4 class="mt20"><strong><?php echo $text_my_newsletter; ?></strong></h4>
  <div class="content">
    <ul class="list">
      <li><a href="<?php echo $newsletter; ?>"><?php echo $text_newsletter; ?></a></li>
    </ul>
  </div>
  </div>
  
  </div>
  
  </div>
  <?php echo $content_bottom; ?></div>

<?php echo $footer; ?> 