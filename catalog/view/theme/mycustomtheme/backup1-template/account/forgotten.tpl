<?php echo $header; ?>



<?php echo $column_left; ?><?php echo $column_right; ?>

<div id="content" class="col-lg-9"><?php echo $content_top; ?>

  <ul class="crumb">

    <?php foreach ($breadcrumbs as $breadcrumb) { ?>

    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>

    <?php } ?>

  </ul>

  <h1 class="clrorg mt53 tittle cps"><?php echo $heading_title; ?></h1>

  <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">

    <p class="f14"><?php echo $text_email; ?></p>
    
    <?php if ($error_warning) { ?>

<div class="warning cred"><?php echo $error_warning; ?></div>

<?php } ?>

    <h4><strong><?php echo $text_your_email; ?></strong></h4>

    <div class="content">

      <table class="form">

        <tr>

          <td><p class="mr10"><?php echo $entry_email; ?></p></td>

          <td><input type="text" name="email" value="" size="50" /></td>

        </tr>

      </table>

    </div>

    <div class="buttons mt25">

      <div class="pull-left mr10"><a href="<?php echo $back; ?>" class="button continue-btn"><?php echo $button_back; ?></a></div>

      <div class="pull-left">

        <input type="submit" value="Submit" class="button continue-btn" />

      </div>

    </div>

  </form>
  </div>

  <?php echo $content_bottom; ?></div>

<?php echo $footer; ?>