<?php echo $header; ?><?php echo $column_left; ?><?php echo $column_right; ?>
<style type="text/css">
#info > ul ,#info >ol
{
	padding:10px;
}
#info > ul ,#info >li,#info >ol,#info >ol >li
{
	list-style-type:disc;
	line-height:25px;
	font-family:"PT Sans", sans-serif;
	font-size:14px;
}

</style>
<div id="content" class="container inner-cont"><?php echo $content_top; ?>
<div id="info">
  <ul class="crumb">

    <?php foreach ($breadcrumbs as $breadcrumb) { ?>

    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>

    <?php } ?>

  </ul>

  <h1 class="clrorg tittle"><?php echo $heading_title; ?></h1>

  <?php echo $description; ?>

  <!--<div class="buttons mb28">

    <div class="right"><a href="<?php echo $continue; ?>" class="button continue-btn"><?php echo $button_continue; ?></a></div>

  </div>-->
</div>
  <?php echo $content_bottom; ?></div>

<?php echo $footer; ?>