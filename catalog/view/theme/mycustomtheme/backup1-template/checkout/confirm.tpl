<?php if (!isset($redirect)) { ?>
<div class="checkout-product mt25 mb28">
  <table class="table-bordered" width="100%">
    <thead>
      <tr>
        <td style="padding:10px;" class="name"><strong><?php echo $column_name; ?></strong></td>
        <td style="padding:10px;" class="model"><strong><?php echo $column_model; ?></strong></td>
        <td style="padding:10px;" class="quantity"><strong><?php echo $column_quantity; ?></strong></td>
        <td style="padding:10px;" class="price"><strong><?php echo $column_price; ?></strong></td>
        <td style="padding:10px;" class="total"><strong><?php echo $column_total; ?></strong></td>
      </tr>
    </thead>
    <tbody>
      <?php foreach ($products as $product) { ?>  
      <?php if($product['recurring']): ?>
        <tr>
            <td style="padding:10px;" colspan="6" style="border:none;"><image src="catalog/view/theme/default/image/reorder.png" alt="" title="" style="float:left;" /><span style="float:left;line-height:18px; margin-left:10px;"> 
                <strong><?php echo $text_recurring_item ?></strong>
                <?php echo $product['profile_description'] ?>
            </td>
        </tr>
      <?php endif; ?>
      <tr>
        <td style="padding:10px;" class="name"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
          <?php foreach ($product['option'] as $option) { ?>
          <br />
          &nbsp;<small> - <?php echo $option['name']; ?>: <?php echo $option['value']; ?></small>
          <?php } ?>
          <?php if($product['recurring']): ?>
          <br />
          &nbsp;<small><?php echo $text_payment_profile ?>: <?php echo $product['profile_name'] ?></small>
          <?php endif; ?>
        </td>
        <td style="padding:10px;" class="model"><?php echo $product['model']; ?></td>
        <td style="padding:10px;" class="quantity"><?php echo $product['quantity']; ?></td>
        <td style="padding:10px;" class="price"><?php echo $product['price']; ?></td>
        <td style="padding:10px;" class="total"><?php echo $product['total']; ?></td>
      </tr>
      <?php } ?>
      <?php foreach ($vouchers as $voucher) { ?>
      <tr>
        <td style="padding:10px;" class="name"><?php echo $voucher['description']; ?></td>
        <td style="padding:10px;" class="model"></td>
        <td style="padding:10px;" class="quantity">1</td>
        <td style="padding:10px;" class="price"><?php echo $voucher['amount']; ?></td>
        <td style="padding:10px;" class="total"><?php echo $voucher['amount']; ?></td>
      </tr>
      <?php } ?>
    </tbody>
    <tfoot>
      <?php foreach ($totals as $total) { ?>
      <tr>
        <td style="padding:10px; text-align:right" colspan="4" class="price"><p><strong><?php echo $total['title']; ?>:</strong></p></td>
        <td style="padding:10px; text-align:right" class="total"><?php echo $total['text']; ?></td>
      </tr>
      <?php } ?>
    </tfoot>
  </table>
</div>
<div class="payment"><?php echo $payment; ?></div>
<?php } else { ?>
<script type="text/javascript"><!--
location = '<?php echo $redirect; ?>';
//--></script> 
<?php } ?>