<div id="featured-bg">

<div class="container">

<h2 class="tittle clrwht text-uppercase mb20"><?php echo $heading_title; ?> Products</h2>

<ul class="arrivals">
<?php 
$compa='http://www.cronaz.com/index.php?route=product/compare';

foreach ($products as $product) { ?>
<li>

<div class="productinner">
<?php if ($product['thumb']) { ?>

<a href="<?php echo $product['href']; ?>">
<img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" class="img-responsive" />
</a>

<?php 
} 
?>

<span class="newlabel">New</span>

</div>

<dl>

<dt><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></dt>

<!--<dd><span class="glyphicon glyphicon-heart-empty"></span><a onclick="addToCompare(<?php echo $product['product_id']; ?>);">Compare</a></dd>-->

<dd class="price clrorg">
<span class="rupee">`</span>
<?php if ($product['price']) 
{ 
?>

<?php if (!$product['special']) { ?>
<?php echo str_replace("Rs.","",$product['price']); ?>
<?php } else { ?>

<?php echo str_replace("Rs.","",$product['price']); ?><?php echo str_replace("Rs.","",$product['special']); ?>
<?php } ?>

<?php 
} 
?>
</dd>
</dl>
    
<!--<?php if ($product['rating']) { ?>

<div class="rating"><img src="catalog/view/theme/default/image/stars-<?php echo $product['rating']; ?>.png" alt="<?php echo $product['reviews']; ?>" /></div>

<?php } ?>-->

<input type="button" value="<?php echo $button_cart; ?>" onclick="addToCart('<?php echo $product['product_id']; ?>');" class="continue-btn" />
<div class="text-center mt20">
      <div class="wishlist"><a onclick="addToWishList('<?php echo $product['product_id']; ?>');"  data-toggle="tooltip" data-placement="top" data-html="true" title="Add To Wish List"><span class="glyphicon glyphicon-heart"></span></a></div>

      <div class="compare"><a onclick="addToCompare('<?php echo $product['product_id']; ?>');" data-toggle="tooltip" data-placement="top" data-html="true" title="Add To Compare	"><span class="glyphicon glyphicon-refresh"></span></a></div>

    </div>

</li><!--product-->
<?php
}
?>


</ul><!--arrivals-->
</div><!--container-->
</div><!--featured-bg-->





<!--<div class="box">

  <div class="box-heading"><?php echo $heading_title; ?></div>

  <div class="box-content">

    <div class="box-product">

      <?php foreach ($products as $product) { ?>

      <div>

        <?php if ($product['thumb']) { ?>

        <div class="image"><a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" /></a></div>

        <?php } ?>

        <div class="name"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></div>

        <?php if ($product['price']) { ?>

        <div class="price">

          <?php if (!$product['special']) { ?>

          <?php echo $product['price']; ?>

          <?php } else { ?>

          <span class="price-old"><?php echo $product['price']; ?></span> <span class="price-new"><?php echo $product['special']; ?></span>

          <?php } ?>

        </div>

        <?php } ?>

        <?php if ($product['rating']) { ?>

        <div class="rating"><img src="catalog/view/theme/default/image/stars-<?php echo $product['rating']; ?>.png" alt="<?php echo $product['reviews']; ?>" /></div>

        <?php } ?>

        <div class="cart"><input type="button" value="<?php echo $button_cart; ?>" onclick="addToCart('<?php echo $product['product_id']; ?>');" class="button" /></div>

      </div>

      <?php } ?>

    </div>

  </div>

</div>

-->