<!DOCTYPE html>
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
<head>
<meta charset="UTF-8" />
<title><?php echo $title; ?></title>
<base href="<?php echo $base; ?>" />
<?php if ($description) { ?>
<meta name="description" content="<?php echo $description; ?>" />
<?php } ?>
<?php if ($keywords) { ?>
<meta name="keywords" content="<?php echo $keywords; ?>" />
<?php } ?>
<?php if ($icon) { ?>
<link href="<?php echo $icon; ?>" rel="icon" />
<?php } ?>

<?php foreach ($links as $link) { ?>
<link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
<?php } ?>

<link href="catalog/view/theme/mycustomtheme/stylesheet/bootstrap.css" rel="stylesheet" type="text/css" />

<link href='http://fonts.googleapis.com/css?family=PT+Sans' rel='stylesheet' type='text/css'>

<link href="catalog/view/theme/mycustomtheme/stylesheet/style.css" rel="stylesheet" type="text/css" />

<?php foreach ($styles as $style) { ?>
<link rel="<?php echo $style['rel']; ?>" type="text/css" href="<?php echo $style['href']; ?>" media="<?php echo $style['media']; ?>" />
<?php } ?>

<!--<link href="catalog/view/theme/mycustomtheme/stylesheet/stylesheet.css" rel="stylesheet" type="text/css" />-->

<!--<script src="catalog/view/javascript/jquery/jquery.min.js" type="text/javascript"></script>-->

<script type="text/javascript" src="catalog/view/javascript/jquery/jquery-1.7.1.min.js"></script>
<script src="catalog/view/theme/mycustomtheme/js/bootstrap.js" type="text/javascript"></script>
<script type="text/javascript" src="catalog/view/theme/mycustomtheme/js/jquery.nivo.slider.js"></script>


<script type="text/javascript" src="catalog/view/javascript/jquery/jqzoom.pack.1.0.1.js"></script>
<link rel="stylesheet" type="text/css" href="catalog/view/javascript/jquery/jqzoom.css" />


<script type='text/javascript' src='catalog/view/javascript/jquery/jquery.autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="catalog/view/javascript/jquery/jquery.autocomplete.css" />


<script type="text/javascript" src="catalog/view/javascript/jquery/ui/jquery-ui-1.8.16.custom.min.js"></script>
<link rel="stylesheet" type="text/css" href="catalog/view/javascript/jquery/ui/themes/ui-lightness/jquery-ui-1.8.16.custom.css" />
<script type="text/javascript" src="catalog/view/javascript/common.js"></script>
<?php foreach ($scripts as $script) { ?>
<script type="text/javascript" src="<?php echo $script; ?>"></script>
<script src="catalog/view/theme/mycustomtheme/js/bootstrap-number-input.js" type="text/javascript"></script>
<?php } ?>
<script type="text/javascript">
$(document).ready(function(e) {
    $(".carts").mouseover(function(){
		$(".minicart").css("display","block");
		
	});
	
	$(".carts").mouseleave(function(e) {
       $(".minicart").css("display","none"); 
    });



  $(".megamenu-left li ul li").mouseover(function(e) {
     $(this).find('a').css('color','#fff'); 
  });

  $(".megamenu-left li ul li").mouseout(function(e) {
     $(this).find('a').css('color','#000'); 
  });


  $("ul li.megamenu-header").mouseover(function(e) {
     $(this).find('a').css('color','#000');
     $(this).css('background-color','#fff');  
  });

  $("ul li.megamenu-header").mouseout(function(e) {
     $(this).find('a').css('color','#000');
     $(this).css('background-color','#fff');   
  });


	
});


/*$(document).ready(function() {
 
    var div = $('#secondaryNavigation');
    var start = $(div).offset().top;
 
    $.event.add(window, "scroll", function() {
        var p = $(window).scrollTop();
        $(div).css('position',((p)>start) ? 'fixed' : 'static');
        $(div).css('top',((p)>start) ? '0px' : '');
		$(div).css('box-shadow',((p)>start) ? 'rgba(0, 0, 0, 0.3) 0px 1px 10px 0px' : '');
		$(".bannerContainer").css('margin-top',((p)>start) ? '56px' : '');
		$('#secondaryNavigation').addClass('sec-nav')
    });
 
});*/

$(window).load(function() {
        $('#slider').nivoSlider();
    });


</script>


<script type="text/javascript">
$(document).ready(function() {
	var options3 =
            {
                zoomWidth: 405,
                zoomHeight: 200,
                showEffect:'fadeout',
                hideEffect:'fadeout',
                fadeoutSpeed: 'slow'

            }

	$(".jqzoom").jqzoom(options3);
});
</script>




<script> 
$(function() {
	$('.list td').mouseover(function() {
    	$(this).addClass('selectedRow');
    }).mouseout(function() {
        $(this).removeClass('selectedRow');
    })
});

</script> 





<script type="text/javascript">
$(document).ready(function() {
	$("#search").autocomplete("index.php?route=common/header/autosugges", {
		
		width: 260,
		matchContains: true,
		//mustMatch: true,
		//minChars: 0,
		//multiple: true,
		//highlight: false,
		//multipleSeparator: ",",
		selectFirst: false
	});
	
	
});
</script>


 <script>
         $(function() {
            var availableTutorials = [
               "ActionScript",
               "Boostrap",
               "C",
               "C++",
            ];
            $( "#search" ).autocomplete({
               source: availableTutorials
            });
         });
      </script>

<style>
#gro
{

height: auto;
position: absolute;
top: 132px;	
z-index:99999;
}


.bigopen
{
	display:block;
}
.bigclose
{
	display:none;
}
</style>

<script>


$(document).ready(function(){
    $( "ul.toprightnav li:first" ).css( "color","#E45339" );
});

</script>

 
</head>



<body>

<div class="topnav">

	<div class="container">

    	<ul class="topleftnav col-lg-3">

        	<li><a href="" class="active">Home</a></li>

           <!-- <li><a href="<?php echo HTTP_SERVER ; ?>index.php?route=information/information&information_id=4">Company</a></li>

            <li><a href="<?php echo HTTP_SERVER ; ?>index.php?route=information/contact">Contact Us</a></li>-->

        </ul>

        <ul class="toprightnav pull-right">
        
       

    <li><?php if (!$logged) 
    { ?><a href=""><span><?php echo $text_welcome; ?></span></a> <?php 
    } 
    else { 
    ?>
    <?php echo $text_logged; ?>
    <?php 
    } 
    ?></li>
    
    <li> <a href="<?php echo $account; ?>"><?php echo $text_account; ?></a></li>
    
    <li class="carts"><a href="<?php echo $shopping_cart; ?>"><span class="glyphicon glyphicon-shopping-cart cart-icon"><span class="count" id="cart-total"><?php echo count($this->cart->getProducts());?></span></span></a>
    
    <div style="width:400px;position:absolute;top: 34px;right: 166px;z-index: 99999; background-color:#FFFFFF; display:none" class="minicart">
<?php
if(count($this->cart->getProducts())==0)
{
?>
<p style="text-align:center; padding:20; border: solid 1px #f4f4f4">shopping cart empty</p>
<?php
}
else
{
$products=$this->cart->getProducts();
?>


    <div class="cart-info">
      <table class="table-responsive" width="100%">
        <thead bgcolor="#f4f4f4">
          <tr style="height: 30px !important;">
            <td class="name text-center"><b><small>Product Name</small></b></td>
            <td class="quantity text-center"><b><small>Quantity</small></b></td>
            <!--<td class="price text-center fs14"><b><strong>Unit Price</strong></b></td>-->
            <td class="total text-center"><b><small>Total</small></b></td>
            <td class="total text-center"><b><small>Remove</small></b></td>
          </tr>
        </thead>
        <tbody>
          <?php foreach ($products as $product) { ?>
            <?php if($product['recurring']): ?>
              <tr style="height: 30px !important;">
                  <td colspan="6" style="border:none;"><image src="catalog/view/theme/default/image/reorder.png" alt="" title="" style="float:left;"  /><span style="float:left;line-height:18px; margin-left:10px;"> 
                      <small><?php echo $text_recurring_item ?></small>
                      <?php echo $product['profile_description'] ?>
                  </td>
                </tr>
            <?php endif; ?>
          <tr style="border-bottom:1px solid #f4f4f4; height: 30px !important;">
           
            <td class="name text-center">
            <small><?php echo $product['name']; ?></small>
            
              
             </td>
           <!-- <td class="model text-center product-name"><?php echo $product['model']; ?></td>-->
            <td class="quantity text-center">
            
            <small><?php echo $product['quantity']; ?></small>
           <!-- <input type="text" readonly name="quantity[<?php echo $product['key']; ?>]" value="<?php echo $product['quantity']; ?>" size="1" />-->
              <!--&nbsp;
              <input type="image" src="catalog/view/theme/default/image/update.png" alt="<?php echo $button_update; ?>" title="<?php echo $button_update; ?>" />-->
             </td>
            <!--<td class="price text-center"><?php echo $product['price']; ?></td>-->
            <td class="total text-center"><small><?php echo $product['total']; ?></small></td>
            <td class="text-center"> <a href="<?php echo HTTP_SERVER .'index.php?route=checkout/cart&remove='.$product['key'] ?>">
            <img src="catalog/view/theme/default/image/remove.png" alt="" width="10" height="10" title="Remove" /></a>
            </td>
          </tr>
          <?php } ?>
          
        </tbody>
      </table>
      <?php
      }
      ?>
    </div>
    
    
    </li>
    <li><a href="<?php echo $wishlist; ?>"><span class="glyphicon glyphicon-heart clrwht cart-icon">
    <span class="count" id="wishlist-total"> <?php echo isset($this->session->data['wishlist']) ? count($this->session->data['wishlist']) : 0;?></span></span></a></li>
    
    <li><a href="<?php echo $cuscompare; ?>"><span class="glyphicon glyphicon-refresh clrwht cart-icon">
     <span class="count" id="compare-total">
     <?php echo isset($this->session->data['compare']) ? count($this->session->data['compare']) : 0;?></span>
    </span></a></li>
</ul>


        </div>

    </div><!--container-->
</div><!--topnav-->


<script>
$(document).ready(function(){


$(".bannerbg").live('mousemove', function( event ) {

    var pageCoords = "( " + event.pageX + ", " + event.pageY + " )";
      
    if(event.pageX < 108)
    {
        $(".mega-menu").removeClass("bigopen").addClass("bigclose");
    }
    else if(event.pageX > 108)
    {
        $(".mega-menu").removeClass("bigclose").addClass("bigopen");
    }
    
}); 


    $("#bs-example-navbar-collapse-1 ul li").mouseover(function(e) {
        
    
	
    //$("#bs-example-navbar-collapse-1 ul li").bind('mouseover',function(){
		//alert();
		 $(".refe").removeClass("bigopen").addClass("bigclose");
        var Bigmenu = $(this).attr('id');
		//$(this).addClass("bigopen");
		
		
		$("."+Bigmenu).removeClass("bigclose").addClass("bigopen");
		
		$("."+Bigmenu).mouseleave(function(e) {
		
		     //$("."+Bigmenu).hide();
			 $(".refe").addClass("bigclose");
			$("."+Bigmenu).removeClass("bigopen").addClass("bigclose");
 		});
 		
    });
  	
});


$(window).load(function() {
    
	$("#banner").css("display","block");
	$("#loaders").css("display","none");
	
});


</script>




<?php if ($error) { ?>
    
    <div class="warning"><?php echo $error ?><img src="catalog/view/theme/default/image/close.png" alt="" class="close" /></div>
    
<?php } ?>
<div id="notification"></div>

<div id="loaders" style="position:relative">
<div style="text-align:center"><img src="catalog/view/theme/mycustomtheme/image/loader.gif"></div>
</div>
<div id="banner" class="mb48" style="display:none;">



<div class="bannerbg" >
<?php
$url = "$_SERVER[REQUEST_URI]";
if($url=='/index.php?route=common/home' || $url=='/')
{
?>



<div id="wrapper">
<div class="slider-wrapper theme-default">
<div id="slider" class="nivoSlider"> 
<img src="catalog/view/theme/mycustomtheme/images/banner1.jpg" data-thumb="catalog/view/theme/mycustomtheme/images/thumb1.jpg" alt="" /> 
<img src="catalog/view/theme/mycustomtheme/images/banner2.jpg" data-thumb="catalog/view/theme/mycustomtheme/images/thumb2.jpg" alt="" title="" />
<img src="catalog/view/theme/mycustomtheme/images/banner3.jpg" data-thumb="catalog/view/theme/mycustomtheme/images/thumb1.jpg" alt="" data-transition="slideInLeft" /> 
</div>
</div>
</div>
<?
}else{
?>
<img src="catalog/view/theme/mycustomtheme/images/inner-banner.jpg" class="img-responsive" alt="" />

<!--<div id="htmlcaption" class="nivo-html-caption"> <strong>This</strong> is an example of a <em>HTML</em> caption with <a href="#">a link</a>. </div>-->

<?php
}
?>
 


    <?php 
    
    $m=0;
    foreach ($categories as $category)
    { 
     
    if( count($category['children']) > 0)
    {
    
    ?>
  
<div class="container <?php echo $category['name']; ?> bigclose refe" >
<div id="gro">
  <ul class="nav navbar-nav row mt10" style="width:1140px;">
  
  
  
  
  
   <ul class="mega-menu col-lg-12 col-sm-12">
           <div class="col-lg-6 col-sm-6 row megamenu-left">
           <div class="col-lg-12 col-sm-12 row">
           

    
    <?php 
    $mega=1;
            foreach($category['children'] as $child)
            {
            ?>
               <li class="col-lg-6 col-sm-6">
    <ul> 
            <li class="megamenu-header">
            <a href="<?php echo $child['href']; ?>" style="font-weight:bold !important; font-size:16px !important"><?php echo $child['name']; ?></a></li>
          
            <?php
            
            $spt=explode('_',$child['href']);
             $chid=end($spt);
            
                if(count($category['subchildren'])>0)
                {
                foreach($category['subchildren'] as $subchild)
                {
                $subid=explode('_',$subchild['href']);
                
                if(in_array($chid,$subid))
                {
                
                ?>
                
               <li><a href="<?php echo $subchild['href']; ?>"><?php echo $subchild['name']; ?></a></li>
           
            <?php 
            }
                }
                }
?>
</ul>
</li>
<?php   
if(count($category['children'])==$mega)
{
?>
</div>
</div>

<?php
}
if($category['name']=='Men')
{
?>
<div class="col-lg-6 col-sm-6 row p0 pull-right"><img src="catalog/view/theme/mycustomtheme/images/men-watch.jpg" class="img-responsive" alt=""></div>
<?php
}elseif($category['name']=='Women')
{
?>
<div class="col-lg-6 col-sm-6 row p0 pull-right"><img src="catalog/view/theme/mycustomtheme/images/wemen-menu.jpg" class="img-responsive" alt=""></div>
<?php
}else
{
?>
<div class="col-lg-6 col-sm-6 row p0 pull-right"><img src="catalog/view/theme/mycustomtheme/images/men-watch.jpg" class="img-responsive" alt=""></div>
<?php
}

$mega++;             
}
?>
    
                                               
     



<!--</div>

</div>

<div class="col-lg-6 col-sm-6 row p0 pull-right"><img src="catalog/view/theme/mycustomtheme/images/men-watch.jpg" class="img-responsive" alt=""></div>-->


</ul>
   
  </li>
  </ul> 
  
 </div> 
 </div>
 
 <?php
 
       }
     }  
     $m++;  
?>
 

<div class="navigation" id="secondaryNavigation">

<nav class="navbar navbar-default nav-tranparent">

  <div class="container">

    <!-- Brand and toggle get grouped for better mobile display -->

    <div class="navbar-header">

  
        
        <a class="navbar-brand hidden-lg hidden-md hidden-sm" href="<?php echo $home; ?>">
        <img src="<?php echo $logo; ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" class="img-responsive" style="margin-top:-15px !important">
        </a>

      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">

        <span class="sr-only">Toggle navigation</span>

        <span class="icon-bar"></span>

        <span class="icon-bar"></span>

        <span class="icon-bar"></span>

      </button>

    </div>



    <!-- Collect the nav links, forms, and other content for toggling -->

    <div class="collapse navbar-collapse p0" id="bs-example-navbar-collapse-1">

      <ul class="nav navbar-nav col-lg-3">
      
      
      <?php 
      $m=0;
      foreach ($categories as $category) { 
      if(2 >= $m)
      {
      ?>
    <li id="<?php echo $category['name']; ?>">
    
    <a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a>   
          
    </li>
    <?php 
    }
    $m++;
    }
     ?>
      

  </ul>

    <?php if ($logo) { ?>
<a href="<?php echo $home; ?>"><img src="<?php echo $logo; ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" class="navbar-brand m0 hidden-xs p0 col-lg-3"  style="margin:0 auto; margin-top:-15px !important" /></a>
  <?php } ?>   

 

      

      <ul class="nav navbar-nav navbar-right col-lg-6">
<?php
     $l=0;
      foreach ($categories as $category) { 
      if($l > 2)
      {
      ?>
    <li id="<?php echo $category['name']; ?>">
    <a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a>
       
    </li>
    <?php 
    }
    $l++;
    }
?>
        

        <!--	<form class="navbar-form navbar-right" role="search">

              <div class="form-group nav-search">

                <input type="text" class="form-control" placeholder="Search" name="search" id="search">

                <label for="email" class="glyphicon glyphicon-search search-icon" rel="search" title="search"></label>

              </div>

            </form>-->
  <form class="navbar-form navbar-right" role="search">          
   <div class="form-group nav-search">          
           
              <div id="search" style="width:250px">
    <div class="button-search">
    <label for="email" class="glyphicon glyphicon-search search-icon" rel="search" title="search"></label>
</div>
    <input type="text" class="form-control" list="browsers"  name="search" placeholder="<?php echo $text_search; ?>" value="<?php echo $search; ?>"  id="search" style="width:250px; margin-bottom:10px;" autocomplete="off" />
    
    
    <datalist id="browsers">
    <?php
    foreach($pnames as $pname)
    {
    ?>
     <option value="<?php echo $pname['name'];?>">
     <?php
    }
    
    ?>
  <!--<option value="Internet Explorer">
  <option value="Firefox">
  <option value="Chrome">
  <option value="Opera">
  <option value="Safari">-->
</datalist>
    
  </div> 
</div>
</form>

        </li>

      </ul>

      

    </div><!-- /.navbar-collapse -->
    
   
 


    <hr class="hidden-xs hidden-md" />
    
   <!--<div class="row" style="margin-left:0px; margin-right:0px;border: solid 1px #f4f4f4;
border-radius: 2px; margin-top:20px">
   <div class="col-md-12" style="background-color:#FFF">
   <div class="col-md-6">
   <ul style="line-height:25px; font-size:14px">
   <?php
   for($i=0;$i<=2;$i++)
   {
   ?>
   <li style="font-weight:bold">Watches</li>
   <li>Watches</li>
   <li>Watches</li>
   <li>Watches</li>
   <li>Watches</li>
   <?php
   }
   ?>
   </ul>
   
   </div>
   
    <div class="col-md-6">dsgsdgsg</div>
   </div>
   </div> -->
    

  </div><!-- /.container -->

</nav>
</div>
</div><!--navigation-->



</div><!--banner-->




<div class="clearfix"></div>

</div>
  