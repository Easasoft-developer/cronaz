<?php echo $header; ?><?php echo $column_left; ?><?php echo $column_right; ?>

<div id="content" class="col-lg-9"><?php echo $content_top; ?>

  <ul class="crumb">

    <?php foreach ($breadcrumbs as $breadcrumb) { ?>

    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>

    <?php } ?>

  </ul>

  <h1 class="clrorg"><?php echo $heading_title; ?></h1>

  <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">

    <div class="content">

      <table class="form">

        <tr>

          <td><?php echo $entry_newsletter; ?></td>

          <td><?php if ($newsletter) { ?>

            <input type="radio" name="newsletter" value="1" checked="checked" />

            <?php echo $text_yes; ?>&nbsp;

            <input type="radio" name="newsletter" value="0" />

            <?php echo $text_no; ?>

            <?php } else { ?>

            <input type="radio" name="newsletter" value="1" />

            <?php echo $text_yes; ?>&nbsp;

            <input type="radio" name="newsletter" value="0" checked="checked" />

            <?php echo $text_no; ?>

            <?php } ?></td>

        </tr>

      </table>

    </div>

    <div class="buttons mt25">

      <div class="pull-left"><a href="<?php echo $back; ?>" class="button continue-btn"><?php echo $button_back; ?></a></div>

      <div class="pull-right"><input type="submit" value="<?php echo $button_continue; ?>" class="button continue-btn" /></div>

    </div>

  </form>
  </div>

  <?php echo $content_bottom; ?></div>

<?php echo $footer; ?>