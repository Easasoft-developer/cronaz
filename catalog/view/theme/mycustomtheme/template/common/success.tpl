<?php echo $header;  ?>
<div class="container">
      <div class="row">
        <ul class="crumb col-xs-12">
            <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
            <?php } ?>
          </ul>
          </div>
    </div>

<?php echo $column_left; ?>

<?php echo $column_right; ?>


<div id="content" class="container">
<?php echo $content_top; ?>

  
<?php if($txn_msg1=='success') { ?>
  <h1 class="clrorg tittle cps"><?php echo $heading_title; ?></h1>

    <p class="fs14"><?php echo $text_message; ?></p>
<?php } else { ?>

 <h1 class="clrorg tittle cps">Oops! YOUR TRANSACTION HAS BEEN declined</h1>
<p class="fs14">Try again.. we regret the inconvenience.</p>
<?php } ?>
<hr>

<div class="col-sm-6">
	<div class="table-responsive">
<table class="table table-bordered table-hover">
<tbody>
<tr>
  <td>TRANSACTION ID: </td>
  <td><?php echo $tpsl_txn_id1; ?></td>
</tr>
<tr>
  <td>ITEM PRICE: </td>
  <td><?php echo $txn_amt1;?></td>
</tr>
<tr>
  <td>ORDER STATUS ID: </td>
  <td><?php echo $txn_status1;?></td>
</tr>
<tr>
  <td>ORDER STATUS:</td>
  <td><?php echo $txn_msg1;?></td>
</tr>
<tr>
  <td>DATE & TIME:</td>
  <td><?php echo $tpsl_txn_time1; ?></td>
</tr>
<tr>
  <td>REFERENCE #</td>
  <td><?php echo $clnt_txn_ref1;?></td>
</tr>
<tr>
  <td>BANK CODE:</td>
  <td><?php echo $tpsl_bank_cd1;?></td>
</tr>

</tbody>

 </table>
</div>
</div>

<div class="col-sm-6">
<?php foreach($addresses as $add) { ?>
<strong>NAME:</strong><p><?php echo $add['firstname'].$add['lastname']; ?></p>
<strong>DELIVERY ADDRESS:</strong></br>
<?php echo $add['address_1']; ?></br>
<?php echo $add['city'];?></br>
<?php echo $add['country']; ?></br>
<?php echo $add['postcode']; ?></br>
<?php } 
unset($add);
?></br>
<strong>Thanks for shopping with us online!</strong>
<hr>
</div>

  </div>
  </div>
  <?php echo $content_bottom; ?></div>
<?php echo $footer; ?>
