<!DOCTYPE html>
<html dir="<?php echo $direction;?>" lang="<?php echo $lang; ?>">
	<head>
		<meta charset="UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title><?php echo $title; ?></title>
		<base href="<?php echo $base; ?>" />
		<?php if ($description) { ?>
		<meta name="description" content="<?php echo $description; ?>" />
		<?php } ?>
		<?php if ($keywords) { ?>
		<meta name="keywords" content="<?php echo $keywords; ?>" />
		<?php } ?>
		<?php if ($icon) { ?>
		<link href="<?php echo $icon; ?>" rel="icon" />
		<?php } ?>
		<?php foreach ($links as $link) { ?>
		<link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
		<?php } ?>
		<link href="catalog/view/theme/mycustomtheme/stylesheet/bootstrap.css" rel="stylesheet" type="text/css" />
		<link href='http://fonts.googleapis.com/css?family=PT+Sans' rel='stylesheet' type='text/css'>
		<link href="catalog/view/theme/mycustomtheme/stylesheet/style.css" rel="stylesheet" type="text/css" />
		<?php foreach ($styles as $style) { ?>
		<link rel="<?php echo $style['rel']; ?>" type="text/css" href="<?php echo $style['href']; ?>" media="<?php echo $style['media']; ?>" />
		<?php } ?>
		<link rel="stylesheet" type="text/css" href="catalog/view/javascript/jquery/jqzoom.css" />
		<link rel="stylesheet" type="text/css" href="catalog/view/javascript/jquery/jquery.autocomplete.css" />
		<link rel="stylesheet" type="text/css" href="catalog/view/javascript/jquery/ui/themes/ui-lightness/jquery-ui-1.8.16.custom.css" />
		<!--<link href="catalog/view/theme/mycustomtheme/stylesheet/stylesheet.css" rel="stylesheet" type="text/css" />-->
		<!--<script src="catalog/view/javascript/jquery/jquery.min.js" type="text/javascript"></script>-->
		<script src="http://code.jquery.com/jquery-1.7.1.js"></script>
		<script src="catalog/view/theme/mycustomtheme/js/bootstrap.js" type="text/javascript"></script>
		<script type="text/javascript" src="catalog/view/theme/mycustomtheme/js/jquery.nivo.slider.js"></script>
		<script type="text/javascript" src="catalog/view/javascript/jquery/jqzoom.pack.1.0.1.js"></script>
		<script type='text/javascript' src='catalog/view/javascript/jquery/jquery.autocomplete.js'></script>
		<script type="text/javascript" src="catalog/view/javascript/jquery/ui/jquery-ui-1.8.16.custom.min.js"></script>
		<script type="text/javascript" src="catalog/view/javascript/common.js"></script>
		<script type="text/javascript" src="catalog/view/javascript/mediajs.js"></script>
		<?php foreach ($scripts as $script) { ?>
		<script type="text/javascript" src="<?php echo $script; ?>"></script>
		<!--<script src="catalog/view/theme/mycustomtheme/js/bootstrap-number-input.js" type="text/javascript"></script>-->
		<?php } ?>

		<script type="text/javascript">
			if( window.location == "http://cronaz.com/" )
				{window.location="http://www.cronaz.com/";}
			
			// cart items count
			$(document).ready(function(e) {			
			
			$.ajax({
			  url: 'index.php?route=common/header/getProducts',
			  dataType: 'json',
			  success: function(json) {					
				var d = json.items;
				var countquantity=0;
				 if(Object.keys(d).length==0) 			 
				 {			
				 $('span.newton').html(countquantity);			
				 } 			
				 else 
				 {			
				$.each(d,function(i,le){
				  countquantity=parseInt(countquantity)+parseInt(le.quantity);
				});
				
				$('span.newton').html(countquantity);				
				
				}
				}				
				});	
			
			
			// mouseover shopping_cart
			
			
			window['$cartItems'] = [];
			 $(".carts").on('mouseover',function(){

			   if(window['$cartItems'].length===0){

					$.ajax({
					url: 'index.php?route=common/header/getProducts',
					dataType: 'json',
					success: function(json) {
					var data=json.items;
					window['$cartItems'] = data;
					var html=''; 
			
				  if(Object.keys(data).length==0){
			 		// if (window.innerWidth > 768) {
						 
			     $('p#empt').html('Your shopping cart is empty');
			     $('.cart-info').hide();
			     $('p#empt').show();
			
			       // }			
			     } 
			     // if part ended

				   else 
				   {
			
			      $('p#empt').hide();
				   	$('.cart-info').show();
				 	
						$.each(data,function(i,ele){			
				
					  html+='<tr style="height: 30px !important;width:400px;position:absolute;top: 34px;right: 166px;z-index: 99999; background-color:#FFFFFF; display:none"">'
			               +'<td colspan="6" style="border:none;">'
			               +'<image src="catalog/view/theme/default/image/reorder.png" alt="" title="" style="float:left;"  />'
			               +'<span style="float:left;line-height:18px; margin-left:10px;">'
			                   +'<small></small>'
			               +'</td>'
			             +'</tr>'
			      	+'<tr>'
			        
			         +'<td class="name">'
			         +'<small>'+ele.name+'</small>'
			           
			          +'</td>'
			         +'<td class="quantity">'
			         
			         +'<small>'+ele.quantity+'</small>'
			        
			          +'</td>'
			         +'<td class="total"><small>'+ele.total+'</small></td>'
			         +'<td>'
			         + '<a href="http://www.cronaz.com/index.php?route=checkout/cart&remove='+ele.key+'">'
			         +'<img src="catalog/view/theme/default/image/remove.png" alt="" width="15" height="15" title="Remove" /></a>'
			         +'</td>'
			       +'</tr>';
			       });

			      $('#cartitems').html(html); 			
			}

			// else part ended
			
			}
			// success json
			});
			
			}
			// if cartitems ended


			$(".minicart").addClass('show');
			
			});		
			
			
			
			$(".carts").on('mouseleave',function(e) {
			    $(".minicart").removeClass('show');
			 });		
		
			
			});

$('.navbar-collapse .navbar-nav > li > a').click(function() {
          var location = $(this).attr('href');
          window.location.href = location;
          return false;
          });
			


			$(window).load(function() {
			   $('#slider').nivoSlider();
			});			

		</script>


		<script type="text/javascript">
			$(document).ready(function() {
			  var options3 =
          {
              zoomWidth: 405,
              zoomHeight: 200,
              showEffect:'fadeout',
              hideEffect:'fadeout',
              fadeoutSpeed: 'slow'

          }
			
			  $(".jqzoom").jqzoom(options3);
			});
		</script>



		<script> 
			$(function() {
			  $('.list td').mouseover(function() {
			      $(this).addClass('selectedRow');
			    }).mouseout(function() {
			        $(this).removeClass('selectedRow');
			    })
			});
			
		</script> 
		<script type="text/javascript">
			$(document).ready(function() {
			  $("#search").autocomplete("index.php?route=common/header/autosugges", {
			    
			    width: 260,
			    matchContains: true,
			    //mustMatch: true,
			    //minChars: 0,
			    //multiple: true,
			    //highlight: false,
			    //multipleSeparator: ",",
			    selectFirst: false
			  });
			  
			  
			});
		</script>
		<script>
			$(function() {
			   var availableTutorials = [
			      "ActionScript",
			      "Boostrap",
			      "C",
			      "C++",
			   ];
			   $( "#search" ).autocomplete({
			      source: availableTutorials
			   });
			});
		</script>
		
		<script>
			$(document).ready(function(){
			    $( "ul.toprightnav li:first" ).css( "color","#E45339" );				
			});		
					
// 					if (window.matchMedia('(max-width: 992px)').matches)
// {			$(document).removeClass(".collapsed");

// }
			
			     $(function() {
			     $('.btn-navbar').click(function(){          
			  if ($(".navbar-collapse").is(":visible")) {
			         $('.navbar-collapse').slideUp();
			         $('.navbar-collapse').hide();
			         $('.navbar-collapse').removeClass('in');
			         $('.navbar-collapse').css({
        WebkitTransition : 'visibility 0.2s ease',
        MozTransition    : 'visibility 0.2s ease',
        MsTransition     : 'visibility 0.2s ease',
        OTransition      : 'visibility 0.2s ease',
        transition       : 'visibility 0.2s ease'
    });
			
			     }else{
			     	$('.navbar-collapse').addClass('in');
			     	$('.navbar-collapse').css({
        WebkitTransition : 'visibility 0.5s ease',
        MozTransition    : 'visibility 0.5s ease',
        MsTransition     : 'visibility 0.5s ease',
        OTransition      : 'visibility 0.5s ease',
        transition       : 'visibility 0.5s ease'
    });
			     	$('.navbar-collapse').slideDown();
			     	$(".navbar-collapse").css("visibility","visible");
			     }
			     });
			   });
			
			 //      $(function() {
			 //      $(".btn-navbar").click(function(){
			 //     $(".navbar-collapse").animate({
			 //         height: 'toggle',
			 //         visibility:'visible'
			 //     });
			 // });
			 //      });
			
			// $( ".btn-navbar" ).click(function() {
			// 		$( ".navbar-collapse" ).fadeToggle( "slow", "linear" );
			// });      
			// $(function() {
			// $('.btn-navbar').click(function(){
			//   $(document).find('.navbar-collapse').toggle();
			// });
			
			// $(document).click(function(e) {
			//   var target = e.target;
			//   if (!$(target).is('.btn-navbar') && !$(target).parents().is('.btn-navbar')) {
			//     $('.navbar-collapse').hide();
			//   }
			// });
			
			// });
			
		</script>

		<script type="text/javascript">
// if (window.matchMedia('(max-width: 767px)').matches) {
		      	
// 		     	} 

		     	 $(document).ready(function() {
						

						 $(window).resize(function(){

						 if(window.innerWidth < 992) {


								 } else if(window.innerWidth>992) {


								}

						 });

						 });


		</script>
		<script>
			$(document).ready(function(){		

				
			
					$(".bannerbg").mousemove(function( event ) {
					
					    var pageCoords = "( " + event.pageX + ", " + event.pageY + " )";
					      
					    if(event.pageX < 108)
					    {
					        $(".mega-menu").removeClass("bigopen").addClass("bigclose");
					    }
					    else if(event.pageX > 108)
					    {
					        $(".mega-menu").removeClass("bigclose").addClass("bigopen");
					    }
					    
					}); 

					
					// if($(window).width() < 992)
					// 	$(".bannerbg").mousemove(function( event ) {
					
					//     var pageCoords = "( " + event.pageX + ", " + event.pageY + " )";
					      
					//     if(event.pageX < 50)
					//     {
					//         $(".mega-menu").removeClass("bigopen").addClass("bigclose");
					//     }
					//     else if(event.pageX > 50)
					//     {
					//         $(".mega-menu").removeClass("bigclose").addClass("bigopen");
					//     }
					    
					// }); 
					// }
			
			
			    $("#bs-example-navbar-collapse-1 ul li").mouseover(function(e) {
			        
			     $(".refe").removeClass("bigopen").addClass("bigclose");
			        var Bigmenu = $(this).attr('id');	    			    
			    
				    $("."+Bigmenu).removeClass("bigclose").addClass("bigopen");
				    
				    $("."+Bigmenu).mouseleave(function(e) {	        
				         
				       $(".refe").addClass("bigclose");
				      $("."+Bigmenu).removeClass("bigopen").addClass("bigclose");
				    });
			    
			    });



			    
			});
			
			
			
			
			
			
			
			$(window).load(function() {
			    
			  $("#banner").css("display","block");
			$("#loaders").css("display","none");
			  
			});			
			
		</script>
	</head>
	<body>
		<div class="topnav">
			<div class="container">
			<div class="row">
				<div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
					<ul class="topleftnav">
						<li><a href="http://www.cronaz.com" class="active">Home</a></li>
					</ul>
				</div>
				<div class="col-lg-9 col-md-9 col-sm-8 col-xs-12">
					<ul class="toprightnav toprightnavRes">
						<li><?php if (!$logged) 
							{ ?><a href=""><span><?php echo $text_welcome; ?></span></a> <?php 
							} 
							else { 
							?>
							<?php echo $text_logged; ?>
							<?php 
								} 
								?>
						</li>
						<li>
						<a title="<?php echo $text_account; ?>" href="<?php echo $account; ?>"><?php echo $text_account; ?></a>
						</li>
						<li class="carts">
							<a title="<?php echo $text_shopping_cart; ?>" href="<?php echo $shopping_cart; ?>"><span class="glyphicon glyphicon-shopping-cart cart-icon"><span class="count newton" id="cart-total"><?php // echo count($this->cart->getProducts());?></span></span></a>
							<div class="minicart">
								<p id="empt"></p>
								<?php $products=$this->cart->getProducts(); ?>
								<div class="cart-info" style="display:none;">
								<div class="table-responsive" >
									<table class="table">
										<thead>
											<tr>
												<td class="name"><b><small>Product Name</small></b></td>
												<td class="quantity"><b><small>Quantity</small></b></td>
												<!--<td class="price fs14"><b><strong>Unit Price</strong></b></td>-->
												<td class="total"><b><small>Total</small></b></td>
												<td class="total"><b><small>Remove</small></b></td>
											</tr>
										</thead>
										<tbody id="cartitems">
											<?php foreach ($products as $product) { ?>
											<?php if($product['recurring']): ?>
											<tr>
												<td colspan="6" style="border:none;">
													<img src="catalog/view/theme/default/image/reorder.png" alt="" title="" style="float:left;"  />
													<span style="float:left;line-height:18px; margin-left:10px;"> 
													<small><?php echo $text_recurring_item ?></small>
													<?php echo $product['profile_description'] ?>
												</td>
											</tr>
											<?php endif; ?>
											<tr>
												<td class="name"><small><?php echo $product['name']; ?></small></td>
												<td class="quantity"><small><?php echo $product['quantity']; ?></small></td>
												<td class="total"><small><?php echo $product['total']; ?></small></td>
												<td><a href="<?php echo HTTP_SERVER .'index.php?route=checkout/cart&remove='.$product['key'] ?>">
													<img src="catalog/view/theme/default/image/remove.png" alt="" width="10" height="10" title="Remove" /></a>
												</td>
											</tr>
											<?php } ?>
										</tbody>
									</table>
								</div>
								</div>
								</div>
						</li>
						<li><a title="<?php echo $text_wishlist; ?>" href="<?php echo $wishlist; ?>"><span class="glyphicon glyphicon-heart clrwht cart-icon">
						<span class="count" id="wishlist-total"> <?php echo isset($this->session->data['wishlist']) ? count($this->session->data['wishlist']) : 0;?></span></span></a>
						</li>
						<li><a title="<?php echo $text_compare; ?>" href="<?php echo $cuscompare; ?>"><span class="glyphicon glyphicon-refresh clrwht cart-icon">
						<span class="count" id="compare-total">
						<?php echo isset($this->session->data['compare']) ? count($this->session->data['compare']) : 0;?></span>
						</span></a>
						</li>
					</ul>
					</div>
					</div>
				</div>
			</div>
			<!--container
				</div>-->
			<!--topnav-->
			<?php if ($error) { ?>
			<div class="warning"><?php echo $error ?>
				<img src="catalog/view/theme/default/image/close.png" alt="" class="close close-n" />
			</div>
			<?php } ?>
			<div class="container"><div id="notification"></div></div>
			<div id="loaders" class="loaders">
				<div><img src="catalog/view/theme/mycustomtheme/image/ajax-loader.gif"></div>
			</div>
			<div id="banner" class="mb20" style="display:none;">
				<div class="bannerbg">
					<div class="navigation" id="secondaryNavigation">
						<nav class="navbar navbar-default nav-tranparent">
							<div class="container">
								<!-- Brand and toggle get grouped for better mobile display -->
								<div class="navbar-header">
									<button type="button" class="navbar-toggle collapsed btn-navbar" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
									<span class="sr-only">Toggle navigation</span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
									</button>
									<a class="navbar-brand navbar-xs" href="<?php echo $home; ?>">
									<img src="<?php echo $logo; ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" class="img-responsive">	 								
									</a>
								</div>
								<!-- Collect the nav links, forms, and other content for toggling -->
								<div class="collapse navbar-collapse p0 row" id="bs-example-navbar-collapse-1">
									<ul class="nav navbar-nav col-lg-3 col-md-3 col-sm-4">
										<?php 
											$m=0;
											foreach ($categories as $category) { 
											if(2 >= $m)
											{
											?>
										<li id="<?php echo $category['name']; ?>">
											<a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a>   
										</li>
										<?php 
											}
											$m++;
											}
											 ?>
									</ul>
									<a class="navbar-brand navbar-logo hidden-xs hidden-sm col-lg-3 col-md-3 col-sm-8" href="<?php echo $home; ?>">
									<img src="<?php echo $logo; ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" class="img-responsive">									
									</a>
									<ul class="nav navbar-nav navbar-right col-lg-6 col-md-6 col-sm-8">
										<?php
											$l=0;
											 foreach ($categories as $category) { 
											 if($l > 2)
											 {
											 ?>
										<li id="<?php echo $category['name']; ?>">
											<a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a>
										</li>
										<?php 
											}
											$l++;
											}
											?>

											<!-- <form class="navbar-form" role="search">
								        <div class="form-group">
								          <input type="text" class="form-control" placeholder="Search">
								        </div>
								        <button type="submit" class="btn btn-default">Submit</button>
								      </form> -->

										<li>
										
											 <form class="navbar-form navbar-right" role="search">
												<div class="form-group nav-search">
													<div id="search">
														
														<span class="glyphicon glyphicon-search search-icon" rel="search" title="search"></span>
													
														<input type="text" class="form-control" list="browsers"  name="search" placeholder="<?php echo $text_search; ?>" value="<?php echo $search; ?>"  id="search" autocomplete="off" />
														<datalist id="browsers">
															<?php
																foreach($pnames as $pname)
																{
																?>
															<option value="<?php echo $pname['name'];?>">
																<?php
																	}
																	
																	?>												
														</datalist>
													</div>
												</div>
											</form> 
										</li>
									</ul>
								</div>
								<!-- /.navbar-collapse -->
								<hr class="hidden-xs hidden-sm hidden-md"/>
							</div>
							<!-- /.container -->
						</nav>
					</div>
					<?php
						$url = "$_SERVER[REQUEST_URI]";
						
						if(!isset($this->request->get['route']) || $this->request->get['route'] == 'common/home')
						{
						?>
					<div id="wrapper">
					<div class="slider-wrapper theme-default">
					<div id="slider" class="nivoSlider">
					<img src="catalog/view/theme/mycustomtheme/images/Banner-1.jpg" data-thumb="catalog/view/theme/mycustomtheme/images/thumb1.jpg" alt="" /> 
					<img src="catalog/view/theme/mycustomtheme/images/Banner-2.jpg" data-thumb="catalog/view/theme/mycustomtheme/images/thumb2.jpg" alt="" title="" />
					<img src="catalog/view/theme/mycustomtheme/images/Banner-3.jpg" data-thumb="catalog/view/theme/mycustomtheme/images/thumb1.jpg" alt="" data-transition="slideInLeft" /> 
					</div>
					</div>
					</div>
					<?php
						}else{
						?>
					<img src="catalog/view/theme/mycustomtheme/images/inner-banner.jpg" class="img-responsive" alt="" />
					<?php
						}
						?>
					<?php 
						$m=0;
						foreach ($categories as $category)
						{ 
						 
						if( count($category['children']) > 0)
						{
						
						?>
					<div class="container <?php echo $category['name']; ?> bigclose refe" >
					<div id="gro" class="hidden-xs hidden-sm">
					<ul class="nav navbar-nav row">
					<ul class="mega-menu col-lg-12 col-sm-12">
					<div class="col-lg-6 col-sm-6 row megamenu-left">
					<div class="col-lg-12 col-sm-12 row">
					<?php 
						$mega=1;
						        foreach($category['children'] as $child)
						        {
						        ?>
					<li class="col-lg-6 col-sm-6">
					<ul>
					<li class="megamenu-header">
					<a href="<?php echo $child['href']; ?>"><?php echo $child['name']; ?></a>
					</li>
					<?php
						$spt=explode('_',$child['href']);
						 $chid=end($spt);
						
						    if(count($category['subchildren'])>0)
						    {
						    foreach($category['subchildren'] as $subchild)
						    {
						    $subid=explode('_',$subchild['href']);
						    
						    if(in_array($chid,$subid))
						    {
						    
						    ?>
					<li><a href="<?php echo $subchild['href']; ?>"><?php echo $subchild['name']; ?></a>
					</li>
					<?php 
						}
						    }
						    }
						?>
					</ul>
					</li>
					<?php   
						if(count($category['children'])==$mega)
						{
						?>
					</div>
					</div>
					<?php
						}
						if($category['name']=='Men')
						{
						?>
					<div class="col-lg-6 col-sm-6 hidden-xs  row p0 pull-right"><img src="catalog/view/theme/mycustomtheme/images/men-watch.jpg" class="img-responsive" alt=""></div>
					<?php
						}elseif($category['name']=='Women')
						{
						?>
					<div class="col-lg-6 col-sm-6 hidden-xs  row p0 pull-right"><img src="catalog/view/theme/mycustomtheme/images/wemen-menu.jpg" class="img-responsive" alt=""></div>
					<?php
						}else
						{
						?>
					<div class="col-lg-6 col-sm-6 hidden-xs  row p0 pull-right"><img src="catalog/view/theme/mycustomtheme/images/men-watch.jpg" class="img-responsive" alt=""></div>
					<?php
						}
						
						$mega++;             
						}
						?>					
					</ul>
					</ul>
					</div>
					</div>
					<?php
						}
						}  
						$m++;  
						?>
				</div>
				<!--navigation-->
			</div>
			<!--banner-->
			<div class="clearfix"></div>
	