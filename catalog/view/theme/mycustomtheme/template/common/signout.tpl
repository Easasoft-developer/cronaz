<?php echo $header; ?>
<div class="container">
      <div class="row">
        	<ul class="crumb col-xs-12">
            <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
            <?php } ?>
          </ul>
      </div>
</div>

<?php echo $column_left; ?>

<?php echo $column_right; ?>


<div id="content">

<?php echo $content_top; ?>
<div  class="container">
	<div class="row">
    <div class="col-sm-9">
    	<h1 class="clrorg tittle cps" style="margin-top:0;">LOGOUT</h1>
      <p>You have been logout successfully.</p>
    </div>
  </div>
  </div>
</div></div></div>
<?php echo $footer; ?>