<!DOCTYPE html>
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
<head>
<meta charset="UTF-8" />
<title><?php echo $title; ?></title>
<base href="<?php echo $base; ?>" />
<?php if ($description) { ?>
<meta name="description" content="<?php echo $description; ?>" />
<?php } ?>
<?php if ($keywords) { ?>
<meta name="keywords" content="<?php echo $keywords; ?>" />
<?php } ?>
<?php if ($icon) { ?>
<link href="<?php echo $icon; ?>" rel="icon" />
<?php } ?>

<link href="catalog/view/theme/mycustomtheme/stylesheet/bootstrap.css" rel="stylesheet" type="text/css" />

<link href='http://fonts.googleapis.com/css?family=PT+Sans' rel='stylesheet' type='text/css'>

<link href="catalog/view/theme/mycustomtheme/stylesheet/style.css" rel="stylesheet" type="text/css" />



<script src="catalog/view/theme/mycustomtheme/js/jquery.min.js" type="text/javascript"></script>

<script src="catalog/view/theme/mycustomtheme/js/bootstrap.js" type="text/javascript"></script>

<script type="text/javascript" src="catalog/view/javascript/common.js"></script>
<?php foreach ($scripts as $script) { ?>
<script type="text/javascript" src="<?php echo $script; ?>"></script>
<?php } ?>

</head>



<body>

<div class="topnav">

	<div class="container">

    	<ul class="topleftnav col-lg-3">

        	<li><a href="" class="active">Home</a></li>

            <li><a href="">Company</a></li>

            <li><a href="">Contact Us</a></li>

        </ul>

        <ul class="toprightnav pull-right">
        
       

    <li><a href=""><?php if (!$logged) 
    { ?><span class="clrorg"><?php echo $text_welcome; ?></span></a> <?php 
    } 
    else { 
    ?>
    <?php echo $text_logged; ?>
    <?php 
    } 
    ?></li>
    
    <li> <a href="<?php echo $account; ?>"><?php echo $text_account; ?></a></li>
    
    <li><a href="<?php echo $shopping_cart; ?>"><span class="glyphicon glyphicon-shopping-cart"></span></a></li>
</ul>
        </div>

    </div><!--container-->

</div><!--topnav-->

<div id="banner" class="mb48">

<div class="bannerbg">
<?php
$url = "$_SERVER[REQUEST_URI]";
if($url=='/cronaz/index.php?route=common/home')
{
?>
<img src="catalog/view/theme/mycustomtheme/images/slide1.jpg" class="img-responsive" alt="" />
<?
}else{
?>
<img src="catalog/view/theme/mycustomtheme/images/inner-banner.jpg" class="img-responsive" alt="" />

<?php
}
?>
 </div>



<div class="navigation">

<nav class="navbar navbar-default nav-tranparent">

  <div class="container">

    <!-- Brand and toggle get grouped for better mobile display -->

    <div class="navbar-header">

  
        
        <a class="navbar-brand hidden-lg hidden-md hidden-sm" href="<?php echo $home; ?>">
        <img src="<?php echo $logo; ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" class="img-responsive" />
        </a>

      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">

        <span class="sr-only">Toggle navigation</span>

        <span class="icon-bar"></span>

        <span class="icon-bar"></span>

        <span class="icon-bar"></span>

      </button>

    </div>



    <!-- Collect the nav links, forms, and other content for toggling -->

    <div class="collapse navbar-collapse p0" id="bs-example-navbar-collapse-1">

      <ul class="nav navbar-nav col-lg-4">
      
      
      <?php 
      $m=0;
      foreach ($categories as $category) { 
      if(3 >= $m)
      {
      ?>
    <li><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a>
      
    </li>
    <?php 
    }
    $m++;
    }
     ?>
      

  </ul>

    <?php if ($logo) { ?>
<a href="<?php echo $home; ?>"><img src="<?php echo $logo; ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" class="navbar-brand m0 hidden-xs p0 col-lg-3"  style="margin:0 auto" /></a>
  <?php } ?>   

 

      

      <ul class="nav navbar-nav navbar-right col-lg-5">
<?php
     $l=0;
      foreach ($categories as $category) { 
      if($l > 3)
      {
      ?>
    <li><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a><li>
      
    </li>
    <?php 
    }
    $l++;
    }
?>
        

        	<form class="navbar-form navbar-right" role="search">

              <div class="form-group nav-search">

                <input type="text" class="form-control" placeholder="Search" name="search" id="search">

                <label for="email" class="glyphicon glyphicon-search search-icon" rel="search" title="search"></label>

              </div>

            </form>

           

        </li>

      </ul>

      

    </div><!-- /.navbar-collapse -->

    <hr class="hidden-xs" />
    
    
    

  </div><!-- /.container -->

</nav>

</div><!--navigation-->



</div><!--banner-->

<div class="clearfix"></div>