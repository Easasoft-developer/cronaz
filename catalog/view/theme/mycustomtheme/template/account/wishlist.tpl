<?php echo $header; ?>

<div class="container">
  <div class="row">
    <ul class="crumb col-xs-12">
      <?php foreach ($breadcrumbs as $breadcrumb) { ?>
      <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
      <?php } ?>
    </ul>
  </div>
  <div class="row">
    <div class="col-xs-12">
      <?php if ($success) { ?>
      <div class="success alert-success text-center"><?php echo $success; ?><img src="catalog/view/theme/default/image/close.png" alt="" class="close" /></div>
      <?php } ?>
    </div>
  </div>
</div>

<div class="container">
  <div class="row">
    <h1 class="clrorg col-xs-12 tittle cps" style="margin-top:10px;"><?php echo $heading_title; ?></h1>
  </div>
</div>
  <?php echo $column_left; ?><?php echo $column_right; ?>
  <div id="content" class="col-lg-9 col-sm-9 col-lg-9 col-xs-12">
    <?php echo $content_top; ?>
    <?php if ($products) { ?>
    <div class="wishlist-info table-responsive">
      <table class="table table-bordered">
        <thead>
          <tr style="background-color:#E4E4E4;">
            <td class="image"><?php echo $column_image; ?></td>
            <td class="name"><?php echo $column_name; ?></td>
            <td class="model"><?php echo $column_model; ?></td>
            <td class="stock"><?php echo $column_stock; ?></td>
            <td class="price"><?php echo $column_price; ?></td>
            <td class="action"><?php echo $column_action; ?></td>
          </tr>
        </thead>
        <?php foreach ($products as $product) { ?>
        <tbody id="wishlist-row<?php echo $product['product_id']; ?>">
          <tr>
            <td class="image"><?php if ($product['thumb']) { ?>
              <a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" /></a>
              <?php } ?>
            </td>
            <td class="name"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></td>
            <td class="model"><?php echo $product['model']; ?></td>
            <td class="stock"><?php echo $product['stock']; ?></td>
            <td class="price">
              <?php if ($product['price']) { ?>
              <div class="price">
                <?php if (!$product['special']) { ?>
                <?php echo $product['price']; ?>
                <?php } else { ?>
                <s><?php echo $product['price']; ?></s> <b><?php echo $product['special']; ?></b>
                <?php } ?>
              </div>
              <?php } ?>
            </td>
            <td class="action"><img src="catalog/view/theme/default/image/cart-add.png" alt="<?php echo $button_cart; ?>" title="<?php echo $button_cart; ?>" onclick="addToCart('<?php echo $product['product_id']; ?>');" />&nbsp;&nbsp;<a href="<?php echo $product['remove']; ?>"><img src="catalog/view/theme/default/image/remove.png" alt="<?php echo $button_remove; ?>" title="<?php echo $button_remove; ?>" /></a></td>
          </tr>
        </tbody>
        <?php } ?>
      </table>
    </div>
    <!-- <div class="buttons mt25">
      <div class="right"><a href="<?php echo $continue; ?>" class="button continue-btn"><?php echo $button_continue; ?></a></div>
      
      </div> -->
    <?php } else { ?>
    <div class="content fs14">
      <h4><?php echo $text_empty; ?></h4>
    </div>
    <div class="buttons">
      <div class="right"><a href="<?php echo $continue; ?>" class="button continue-btn"><?php echo $button_continue; ?></a></div>
    </div>
    <?php } ?>
  </div>
  </div>

<?php echo $content_bottom; ?></div>
<?php echo $footer; ?>

