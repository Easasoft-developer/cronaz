<?php echo $header; ?>



<div class="container">
  <div class="row">
  <ul class="crumb col-xs-12">
  
      <?php foreach ($breadcrumbs as $breadcrumb) { ?>
  
      <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
  
      <?php } ?>
  
    </ul>

    </div>
</div>

<div class="container">
  <div class="row"><h1 class="clrorg tittle col-xs-12 cps" style="margin-top:10px;"><?php echo $heading_title; ?></h1></div>
</div>

<?php echo $column_left; ?><?php echo $column_right; ?>

<div id="content" class="col-lg-9 col-sm-9 col-xs-12 col-lg-9"><?php echo $content_top; ?>

  

  <!-- <h1 class="clrorg mt53 tittle cps"><?php echo $heading_title; ?></h1> -->

  <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">

    <h4><strong><?php echo $text_password; ?></strong></h4>

    <div class="content">

      <div class="row col-md-12 col-sm-12 col-lg-12 col-xs-12">

        <div class="row">

          <div class="col-md-3 col-lg-3 col-sm-4 col-xs-12"><p style="margin-top: 10px;"><span class="required">*</span><?php echo $entry_password; ?></p></div>

          <div class="col-md-5 col-lg-5 col-sm-7 col-xs-12"><input type="password" name="password" value="<?php echo $password; ?>" class="col-lg-12 col-sm-12 col-xs-12 col-md-12 login" />

            <?php if ($error_password) { ?>

            <span class="error"><?php echo $error_password; ?></span>

            <?php } ?></div>

        </div>

        <div class="row">

          <div class="col-md-3 col-lg-3 col-sm-4 col-xs-12"><p style="margin-top: 10px;"><span class="required">*</span><?php echo $entry_confirm; ?></p></div>

          <div class="col-md-5 col-lg-5 col-sm-7 col-xs-12"><input type="password" name="confirm" value="<?php echo $confirm; ?>" class="col-lg-12 col-sm-12 col-xs-12 col-md-12 login" />

            <?php if ($error_confirm) { ?>

            <span class="error"><?php echo $error_confirm; ?></span>

            <?php } ?></div>

        </div>

      </div>

    </div>

    <div class="buttons row col-md-12 col-sm-12 col-lg-12 col-xs-12">

      <!-- <div class="pull-left mr10"><a href="<?php echo $back; ?>" class="button back-btn"><?php echo $button_back; ?></a></div> -->

      <div>

        <input type="submit" value="<?php //echo $button_continue; ?>Update" class="button continue-btn" />

      </div>

    </div>

  </form>
  </div>
  </div>

  <?php echo $content_bottom; ?></div>

<?php echo $footer; ?>