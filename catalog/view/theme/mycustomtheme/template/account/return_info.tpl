<?php echo $header; ?>

<div class="container">
  <div class="row">
  <ul class="crumb col-xs-12">
  
      <?php foreach ($breadcrumbs as $breadcrumb) { ?>
  
      <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
  
      <?php } ?>
  
    </ul>
    </div>
</div>

<div class="container">
<div class="row">
<h1 class="clrorg col-xs-12 cps tittle" style="margin-top:10px;"><?php echo $heading_title; ?></h1>
</div>
</div>

<?php echo $column_left; ?><?php echo $column_right; ?>

<div id="content" class="col-lg-9 col-md-9 col-sm-9 col-xs-12"><?php echo $content_top; ?>

  <div class="row">

  <div class="col-xs-12">
  <h4><strong><?php echo $text_return_detail; ?></strong></h4>
  <div class="table-responsive">
  <table class="list table table-bordered">
  
      <tbody>
  
        <tr>
  
          <td class="left" style="width: 50%;"><b><?php echo $text_return_id; ?></b> #<?php echo $return_id; ?><br />
  
            <b><?php echo $text_date_added; ?></b> <?php echo $date_added; ?></td>
  
          <td class="left" style="width: 50%;"><b><?php echo $text_order_id; ?></b> #<?php echo $order_id; ?><br />
  
            <b><?php echo $text_date_ordered; ?></b> <?php echo $date_ordered; ?></td>
  
        </tr>
  
      </tbody>
  
    </table>

    </div>
    </div>

  </div>

  <h2 class="tittle"><?php echo $text_product; ?></h2>

  <div class="table-responsive">
  <table class="list table table-bordered">

    <thead>

      <tr>

        <td class="left"><strong><?php echo $column_product; ?></strong></td>

        <td class="left"><strong><?php echo $column_model; ?></strong></td>

        <td class="right"><strong><?php echo $column_quantity; ?></strong></td>

      </tr>

    </thead>

    <tbody>

      <tr>

        <td class="left" style="width: 33.3%;"><?php echo $product; ?></td>

        <td class="left" style="width: 33.3%;"><?php echo $model; ?></td>

        <td class="right" style="width: 33.3%;"><?php echo $quantity; ?></td>

      </tr>

    </tbody>

  </table>
  </div>

  <div class="table-responsive">
  <table class="list table table-bordered">

    <thead>

      <tr>

        <td class="left" style="width: 33.3%;"><strong><?php echo $column_reason; ?></strong></td>

        <td class="left" style="width: 33.3%;"><strong><?php echo $column_opened; ?></strong></td>

        <td class="left" style="width: 33.3%;"><strong><?php echo $column_action; ?></strong></td>

      </tr>

    </thead>

    <tbody>

      <tr>

        <td class="left"><?php echo $reason; ?></td>

        <td class="left"><?php echo $opened; ?></td>

        <td class="left"><?php echo $action; ?></td>

      </tr>

    </tbody>

  </table>
  </div>

  <?php if ($comment) { ?>

<div class="table-responsive">
  <table class="list table table-bordered">

    <thead>

      <tr>

        <td class="left"><?php echo $text_comment; ?></td>

      </tr>

    </thead>

    <tbody>

      <tr>

        <td class="left"><?php echo $comment; ?></td>

      </tr>

    </tbody>

  </table>
  </div>

  <?php } ?>

  <?php if ($histories) { ?>

  <h2><?php echo $text_history; ?></h2>

<div class="table-responsive">
  <table class="list table table-bordered">

    <thead>

      <tr>

        <td class="left" style="width: 33.3%;"><?php echo $column_date_added; ?></td>

        <td class="left" style="width: 33.3%;"><?php echo $column_status; ?></td>

        <td class="left" style="width: 33.3%;"><?php echo $column_comment; ?></td>

      </tr>

    </thead>

    <tbody>

      <?php foreach ($histories as $history) { ?>

      <tr>

        <td class="left"><?php echo $history['date_added']; ?></td>

        <td class="left"><?php echo $history['status']; ?></td>

        <td class="left"><?php echo $history['comment']; ?></td>

      </tr>

      <?php } ?>

    </tbody>

  </table>
  </div>

  <?php } ?>

  <div class="buttons">

    <div class="right"><a href="<?php echo $continue; ?>" class="button continue-btn"><?php echo $button_continue; ?></a></div>

  </div>
  </div>
  </div>

  <?php echo $content_bottom; ?></div>

<?php echo $footer; ?>