<?php echo $header; ?>

<script type="text/javascript">

$(document).ready(function(){

var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
var num = /^[0-9]+$/;

 if ( $("input[name=firstname]").val().length === 0 || $("input[name=firstname]").val().length > 32 )
{
    $("input[name=firstname]").focus();
 }

else if ( $("input[name=lastname]").val().length === 0 || $("input[name=lastname]").val().length > 32  )
{
    $("input[name=lastname]").focus();
 }
 
 
else if ( $("input[name=email]").val().length === 0 || $("input[name=email]").val().length > 32 || (!regex.test($("input[name=email]").val())))
{
    $("input[name=email]").focus();
 }
 
 else if ( $("input[name=telephone]").val().length === 0 || (!num.test($("input[name=telephone]").val())))
{
    $("input[name=telephone]").focus();
 }
 
  else if ( $("input[name=address_1]").val().length === 0 || $("input[name=address_1]").val().length > 128 )
{
    $("input[name=address_1]").focus();
 }
  else if ( $("input[name=city]").val().length === 0 || $("input[name=city]").val().length > 128 )
{
    $("input[name=city]").focus();
 }
 else if ( $("input[name=postcode]").val().length === 0 || $("input[name=postcode]").val().length > 10 )
{
    $("input[name=postcode]").focus();
      
 }
 else if ($('select[name=\'zone_id\']').val() === "")
 {
    alert("hello");
 }  
  else if ( $("input[name=password]").val().length == 0 || $("input[name=password]").val().length > 20  )
{
    $("input[name=password]").focus();
 }
  else if ( $("input[name=confirm]").val().length === 0 ||  $("input[name=password]").val() != $("input[name=confirm]").val() )
{
    $("input[name=confirm]").focus();
 }
 else { }


});

</script>

<div class="container"> <!--Custom Code-->
   <div class="row">
   <ul class="crumb col-xs-12" style="">
   
         <?php foreach ($breadcrumbs as $breadcrumb) { ?>
   
         <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
   
         <?php } ?>
   
       </ul>
  </div>

    <div class="row">
          <div class="col-xs-12">
             <?php if ($error_warning) { ?>

          <div class="warning alert alert-warning text-center" role="alert"><?php echo $error_warning; ?><img src="catalog/view/theme/default/image/close.png" alt="" class="close" /></div>
    
    <?php } ?>
          </div>
        </div>

        
</div>

<?php echo $column_left; ?>

<div class="container">
 <div class="row"> 
 
 <div class="col-xs-12"><h1 style="text-transform: uppercase;margin-top:10px;" class="clrorg tittle titleRegistration"><?php echo $heading_title; ?>
  </h1> 
  </div>
 
 </div>
</div>

<?php echo $column_right; ?>

<div id="content" class="col-lg-9 col-xs-12 col-md-9 col-sm-9"><?php echo $content_top; ?>

  <div class="row">
  <div class="col-xs-12">
     <p class="fs14"><?php echo $text_account_already; ?></p>
  </div>
  </div>


<div class="row">
<!--Custom code-->
  <form name="form" action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">   

    <div class="content">

      <div class="col-md-12 col-sm-12 col-lg-12 col-xs-12 dvRegContent" style="">

        

        <div class="row">
            <div class="col-xs-12"><h4><strong><?php echo $text_your_details; ?></strong></h4></div>
        </div>
        <div class="row">          

          <div class="col-md-4 col-sm-6 col-lg-4 col-xs-12"> <p><span class="required">*</span><?php echo $entry_firstname; ?></p></div>

          <div class="col-md-5 col-sm-6 col-lg-5 col-xs-12">

          <input type="text" name="firstname" value="<?php echo $firstname; ?>" class="col-lg-12 col-md-12 col-xs-12 col-sm-12 login" />

            <?php if ($error_firstname) { ?>

            <span class="error"><?php echo $error_firstname; ?></span>

            <?php } ?>
            </div>

        </div>

        <div class="row">

          <div class="col-md-4 col-sm-6 col-lg-4 col-xs-12"><p><span class="required">*</span><?php echo $entry_lastname; ?></p></div>

          <div class="col-md-5 col-sm-6 col-lg-5 col-xs-12"><input type="text" name="lastname" value="<?php echo $lastname; ?>" class="col-lg-12 col-md-12 col-xs-12 col-sm-12 login" />

            <?php if ($error_lastname) { ?>

            <span class="error"><?php echo $error_lastname; ?></span>

            <?php } ?></div>

        </div>

        <div class="row">

          <div class="col-md-4 col-sm-6 col-lg-4 col-xs-12"><p><span class="required">*</span><?php echo $entry_email; ?></p>
          </div>

          <div class="col-md-5 col-sm-6 col-lg-5 col-xs-12"><input type="email" name="email" value="<?php echo $email; ?>" class="col-lg-12 col-md-12 col-xs-12 col-sm-12 login" />

           <?php if ($error_email) { ?>

            <span class="error"><?php echo $error_email; ?></span>

            <?php } ?></div>

        </div>

        <div class="row">

          <div class="col-md-4 col-sm-6 col-lg-4 col-xs-12"><p><span class="required">*</span><?php echo $entry_telephone; ?></p></div>

          <div class="col-md-5 col-sm-6 col-lg-5 col-xs-12"><input type="tel" name="telephone" value="<?php echo $telephone; ?>" class="col-lg-12 col-md-12 col-xs-12 col-sm-12 login" />

            <?php if ($error_telephone) { ?>

            <span class="error"><?php echo $error_telephone; ?></span>

            <?php } ?></div>

        </div>

       <!-- <tr>

          <td><big><?php echo $entry_fax; ?></big></td>

          <td><input type="text" name="fax" value="<?php echo $fax; ?>" class="col-lg-12 login" /></td>

        </tr>-->

      </div>

    </div>

   

    <div class="content">

      <div class="col-md-12 col-sm-12 col-lg-12 col-xs-12 dvRegContent" style="">

        <div class="row">
           <div class="col-xs-12"><h4><strong><?php echo $text_your_address; ?></strong></h4></div>

          <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12"><p><span class="required">*</span><?php echo $entry_address_1; ?></p></div>

          <div class="col-md-5 col-lg-5 col-sm-6 col-xs-12"><input type="text" name="address_1" value="<?php echo $address_1; ?>" class="col-lg-12 col-md-12 col-xs-12 col-sm-12 login" />

            <?php if ($error_address_1) { ?>

            <span class="error"><?php echo $error_address_1; ?></span>

            <?php } ?></div>

        </div>

        <div class="row">

          <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12"><p>&nbsp;<?php echo $entry_address_2; ?></p></div>

          <div class="col-md-5 col-lg-5 col-sm-6 col-xs-12"><input type="text" name="address_2" value="<?php echo $address_2; ?>" class="col-lg-12 col-md-12 col-xs-12 col-sm-12 login" /></div>

        </div>

        <div class="row">

          <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12"><p><span class="required">*</span><?php echo $entry_city; ?></p></div>

          <div class="col-md-5 col-lg-5 col-sm-6 col-xs-12"><input type="text" name="city" value="<?php echo $city; ?>" class="col-lg-12 col-md-12 col-xs-12 col-sm-12 login" />

            <?php if ($error_city) { ?>

            <span class="error"><?php echo $error_city; ?></span>

            <?php } ?></div>

        </div>

        <div class="row">

          <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12"><!--<span id="postcode-required" class="required">*</span>--> <p><span  class="required">*</span><?php echo $entry_postcode; ?></p></div>

          <div class="col-md-5 col-lg-5 col-sm-6 col-xs-12"><input type="text" name="postcode" value="<?php echo $postcode; ?>" class="col-lg-12 col-md-12 col-xs-12 col-sm-12 login" />

            <?php if ($error_postcode) { ?>

            <span class="error"><?php echo $error_postcode; ?></span>

            <?php } ?></div>

        </div>

        <div class="row">

          <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12"><p><span class="required"></span><?php echo $entry_country; ?></p></div>

          <div class="col-md-5 col-lg-5 col-sm-6 col-xs-12"><select name="country_id" class="col-lg-12 col-md-12 col-xs-12 col-sm-12 login">

              <option value=""><?php echo $text_select; ?></option>

              <?php foreach ($countries as $country) { ?>

              <?php if ($country['country_id'] == $country_id) { ?>

              <option value="<?php echo $country['country_id']; ?>" selected="selected"><?php echo $country['name']; ?></option>

              <?php } else { ?>

              <option value="<?php echo $country['country_id']; ?>"><?php echo $country['name']; ?></option>

              <?php } ?>

              <?php } ?>

            </select>

            <?php if ($error_country) { ?>

            <span class="error"><?php echo $error_country; ?></span>

            <?php } ?></div>

        </div>

        <div class="row">

          <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12"><p><span class="required">*</span><?php echo $entry_zone; ?></p></div>

          <div class="col-md-5 col-lg-5 col-sm-6 col-xs-12"><select id="checkbox" name="zone_id" class="col-lg-12 col-md-12 col-xs-12 col-sm-12 login">

            </select>

            <?php if ($error_zone) { ?>

            <span class="error"><?php echo $error_zone; ?></span>

            <?php } ?></div>

        </div>

      </div>

    </div>

    <div class="content">

      <div class="col-md-12 col-sm-12 col-lg-12 col-xs-12 dvRegContent">

        <div class="row">

          <div class="col-xs-12"><h4><strong><?php echo $text_your_password; ?></strong></h4></div>

          <div class="col-md-4 col-sm-6 col-lg-4 col-xs-12"><p><span class="required">*</span><?php echo $entry_password; ?></p></div>

          <div class="col-md-5 col-sm-6 col-lg-5 col-xs-12"><input type="password" name="password" value="<?php echo $password; ?>" class="col-lg-12 col-sm-12 col-lg-12 col-xs-12 login" />

            <?php if ($error_password) { ?>

            <span class="error"><?php echo $error_password; ?></span>

            <?php } ?></div>

        </div>

        <div class="row">

          <div class="col-md-4 col-sm-6 col-lg-4 col-xs-12"><p><span class="required">*</span><?php echo $entry_confirm; ?><p></div>

          <div class="col-md-5 col-sm-6 col-lg-5 col-xs-12"><input type="password" name="confirm" value="<?php echo $confirm; ?>" class="col-lg-12 col-sm-12 col-lg-12 col-xs-12 login" />

            <?php if ($error_confirm) { ?>

            <span class="error"><?php echo $error_confirm; ?></span>

            <?php } ?></div>

        </div>

      </div>

    </div>

    <div class="content">

      <div class="col-md-12 col-lg-12 colsm-12 col-xs-12 dvRegContent">

        <div class="row">

           <div class="col-xs-12"><h4><strong><?php echo $text_newsletter; ?></strong></h4></div>

          <div class="col-md-4 col-lg-4 col-sm-6 col-xs-6"><p><?php echo $entry_newsletter; ?></p></div>

         <div class="col-md-5 col-lg-5 col-sm-6 col-xs-12"><?php if ($newsletter) { ?>

            <input type="radio" name="newsletter" value="1" checked="checked" />

            <?php echo $text_yes; ?>

            <input type="radio" name="newsletter" value="0" />

            <?php echo $text_no; ?>

            <?php } else { ?>

            <input type="radio" name="newsletter" value="1" />

            <?php echo $text_yes; ?>

            <input type="radio" name="newsletter" value="0" checked="checked" />

            <?php echo $text_no; ?>

            <?php } ?></div>

        </div>

        <div class="row">
            <?php if ($text_agree) { ?>

          <div class="col-xs-12"><div class="buttons mt25">
          
                      <div class="right">

                      
          
                        <?php if ($agree) { ?>
        
                         <label for="agree1" class="fs14 tc">
                            <input id="agree1" type="checkbox" name="agree" value="1" checked="checked"/> <?php echo $text_agree; ?>
                        </label>
          
                        <?php } else { ?>
          
                        <label for="agree1" class="fs14 tc">
                          <input id="agree1" type="checkbox" name="agree" value="1"/> <?php echo $text_agree; ?>
                        </label>
          
                        <?php } ?>
                        <div class="regBtn">
                                <input type="submit" value="<?php //echo $button_continue; ?>Register" onload="Submit()" class="button continue-btn" />
                        </div>
                      </div>
          
                    </div></div>

    <?php } else { ?>

          <div class="buttons">

            <div class="right">

              <input type="submit" value="<?php echo $button_continue; ?>" class="button" />

            </div>

          </div>

    <?php } ?>

        </div>



      </div>

    </div>

    

  </form>
  </div>

  <?php echo $content_bottom; ?>
</div>
</div>

</div>

<script type="text/javascript"><!--

$('input[name=\'customer_group_id\']:checked').live('change', function() {

  var customer_group = [];

  

<?php foreach ($customer_groups as $customer_group) { ?>

  customer_group[<?php echo $customer_group['customer_group_id']; ?>] = [];

  customer_group[<?php echo $customer_group['customer_group_id']; ?>]['company_id_display'] = '<?php echo $customer_group['company_id_display']; ?>';

  customer_group[<?php echo $customer_group['customer_group_id']; ?>]['company_id_required'] = '<?php echo $customer_group['company_id_required']; ?>';

  customer_group[<?php echo $customer_group['customer_group_id']; ?>]['tax_id_display'] = '<?php echo $customer_group['tax_id_display']; ?>';

  customer_group[<?php echo $customer_group['customer_group_id']; ?>]['tax_id_required'] = '<?php echo $customer_group['tax_id_required']; ?>';

<?php } ?>  



/*  if (customer_group[this.value]) {

    if (customer_group[this.value]['company_id_display'] == '1') {

      $('#company-id-display').show();

    } else {

      $('#company-id-display').hide();

    }

    

    if (customer_group[this.value]['company_id_required'] == '1') {

      $('#company-id-required').show();

    } else {

      $('#company-id-required').hide();

    }

    

    if (customer_group[this.value]['tax_id_display'] == '1') {

      $('#tax-id-display').show();

    } else {

      $('#tax-id-display').hide();

    }

    

    if (customer_group[this.value]['tax_id_required'] == '1') {

      $('#tax-id-required').show();

    } else {

      $('#tax-id-required').hide();

    } 

  }

});*/



$('input[name=\'customer_group_id\']:checked').trigger('change');

//--></script> 

<script type="text/javascript"><!--

$('select[name=\'country_id\']').bind('change', function() {

  $.ajax({

    url: 'index.php?route=account/register/country&country_id=' + this.value,

    dataType: 'json',

    beforeSend: function() {

      /* $('select[name=\'country_id\']').after('<span class="wait">&nbsp;<img src="catalog/view/theme/default/image/loading.gif" alt="" /></span>'); */

    },

    complete: function() {

      $('.wait').remove();

    },      

    success: function(json) {

      if (json['postcode_required'] == '1') {

        $('#postcode-required').show();

      } else {

        $('#postcode-required').hide();

      }

      

      html = '<option value=""><?php echo $text_select; ?></option>';

      

      if (json['zone'] != '') {

        for (i = 0; i < json['zone'].length; i++) {

              html += '<option value="' + json['zone'][i]['zone_id'] + '"';

            

          if (json['zone'][i]['zone_id'] == '<?php echo $zone_id; ?>') {

                html += ' selected="selected"';

            }

  

            html += '>' + json['zone'][i]['name'] + '</option>';

        }

      } else {

        html += '<option value="0" selected="selected"><?php echo $text_none; ?></option>';

      }

      

      $('select[name=\'zone_id\']').html(html);

    },

    error: function(xhr, ajaxOptions, thrownError) {

      alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);

    }

  });

});



$('select[name=\'country_id\']').trigger('change');

//--></script> 

<script type="text/javascript"><!--

$(document).ready(function() {

  $('.colorbox').colorbox({

    width: 640,

    height: 480

  });

});

//--></script> 

<?php echo $footer; ?>