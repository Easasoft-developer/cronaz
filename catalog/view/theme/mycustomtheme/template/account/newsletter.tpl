<?php echo $header; ?>


<div class="container">
   <div class="row"><ul class="crumb col-xs-12">
   
       <?php foreach ($breadcrumbs as $breadcrumb) { ?>
   
       <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
   
       <?php } ?>
   
     </ul></div>
</div>

<div class="container">  
  <div class="row"><h1 class="clrorg col-xs-12 tittle cps" style="margin-top:10px;"><?php echo $heading_title; ?></h1></div>
</div>



<?php echo $column_left; ?><?php echo $column_right; ?>

<div id="content" class="col-lg-9 col-md-9 col-sm-9 col-xs-12"><?php echo $content_top; ?>

  <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">

    <div class="content">

      <table class="form">

        <tr>

          <td><?php echo $entry_newsletter; ?></td>

          <td><?php if ($newsletter) { ?>

            <input type="radio" name="newsletter" value="1" checked="checked" />

            <span><?php echo $text_yes; ?></span>&nbsp;

            <input type="radio" name="newsletter" value="0" />

            <span><?php echo $text_no; ?></span>

            <?php } else { ?>

            <input type="radio" name="newsletter" value="1" />

            <span><?php echo $text_yes; ?></span>&nbsp;

            <input type="radio" name="newsletter" value="0" checked="checked" />

            <span><?php echo $text_no; ?></span>

            <?php } ?></td>

        </tr>

      </table>

      <div class="buttons">

      <div class=""><!-- <a href="<?php echo $back; ?>" class="button continue-btn"><?php echo $button_back; ?></a> -->
      
      <input type="submit" value="<?php //echo $button_continue; ?>Update" class="button continue-btn" />
      </div>

      <div class="pull-right"></div>

    </div>

    </div>

    

  </form>
  </div>
  </div>

  <?php echo $content_bottom; ?></div>

<?php echo $footer; ?>