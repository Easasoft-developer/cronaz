<?php echo $header; ?>

<div class="container">
  
   <div class="row">
   <ul class="crumb col-xs-12">
   
       <?php foreach ($breadcrumbs as $breadcrumb) { ?>
   
       <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
   
       <?php } ?>
   
     </ul>
     </div>
</div>

<div class="container">
<div class="row">
<h1 class="clrorg col-xs-12 tittle cps" style="margin-top:10px;"><?php echo $heading_title; ?></h1>
</div>
</div>

<?php echo $column_left; ?><?php echo $column_right; ?>

<div id="content" class="col-lg-9 col-xs-12 col-md-9 col-sm-9"><?php echo $content_top; ?>

  <?php if ($returns) { ?>

  <?php foreach ($returns as $return) { ?>

  <div class="return-list table-responsive">
  <table class="table table-bordered" style="margin-bottom:0px;">

    <tr>
    <td class="return-id"><b><?php echo $text_return_id; ?></b> #<?php echo $return['return_id']; ?></td>

    <td class="return-status"><b><?php echo $text_status; ?></b> <?php echo $return['status']; ?></td>

    <div class="return-content">

      <td><b><?php echo $text_date_added; ?></b> <?php echo $return['date_added']; ?></td>

        <td><b><?php echo $text_order_id; ?></b> <?php echo $return['order_id']; ?></td>

      <td><b><?php echo $text_customer; ?></b> <?php echo $return['name']; ?></td>

      <td class="return-info">
      <a href="<?php echo $return['href']; ?>"><img src="catalog/view/theme/default/image/info.png" alt="<?php echo $button_view; ?>" title="<?php echo $button_view; ?>" /></a>
      </td>
       </div>
      </tr>
      

   
    </table>

  </div>

  <?php } ?>

 

  <?php } else { ?>

  <div class="content fs14"><p><?php echo $text_empty; ?></p></div>

  <?php } ?>

  <!-- <div class="buttons mt25">

    <div class="right"><a href="<?php echo $continue; ?>" class="button"><?php echo $button_continue; ?></a></div>

  </div> -->
   <div class="pagination"><?php echo $pagination; ?></div>
  </div>
  </div>

  <?php echo $content_bottom; ?>

  </div>

<?php echo $footer; ?>