<?php echo $header; ?>

<div class="container"><!--Custom Code-->
 <div class="row">
 <ul class="crumb col-xs-12">
 
     <?php foreach ($breadcrumbs as $breadcrumb) { ?>
 
     <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
 
     <?php } ?>
 
   </ul>
   </div>
</div>

<div class="container"><h1 style="margin-top:10px;" class="clrorg tittle cps"><?php echo $heading_title; ?></h1></div>

<?php echo $column_left; ?>

<?php echo $column_right; ?>

<div id="content" class="col-lg-9 col-md-9 col-sm-9 col-xs-12"><?php echo $content_top; ?>

 

  <!-- <h1 class="clrorg mt53 tittle cps"><?php echo $heading_title; ?></h1> -->

  <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">

    <p class="f14"><?php echo $text_email; ?></p>
    
    <?php if ($error_warning) { ?>

<div class="alert-warning warning cred text-center"><?php echo $error_warning; ?><img src="catalog/view/theme/default/image/close.png" alt="" class="close" /></div>

<?php } ?>

    <h4><strong><?php echo $text_your_email; ?></strong></h4>

    <div class="content">

      <div class="form">

        <div class="row">

          <div class="col-md-3 col-lg-2 col-sm-3 col-xs-12"><p class="mr10"><?php echo $entry_email; ?></p></div>

          <div class="col-md-6 col-lg-6 col-sm-6 col-xs-12"><input type="email" name="email" value="" class="col-md-6 col-lg-6 col-sm-12 col-xs-12 login" /></div>

        </div>

      </div>

    </div>

    <div class="buttons mt25">

      <div class="pull-left mr10"><a href="<?php echo $back; ?>" class="button continue-btn"><?php echo $button_back; ?></a></div>

      <div class="pull-left">

        <input type="submit" value="Submit" class="button continue-btn" />

      </div>

    </div>

  </form>
  </div>
  </div>

  <?php echo $content_bottom; ?></div>

<?php echo $footer; ?>