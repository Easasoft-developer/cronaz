<?php echo $header; ?>

<div class="container">
   <div class="row">
   <ul class="crumb col-xs-12">
   
       <?php foreach ($breadcrumbs as $breadcrumb) { ?>
   
       <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
   
       <?php } ?>
   
     </ul>
     </div>

       <div class="row">
         <div class="col-xs-12">
           <?php if ($success) { ?>

<div class="alert-success success"><?php echo $success; ?><img src="catalog/view/theme/default/image/close.png" alt="" class="close"></div>

<?php } ?>

<?php if ($error_warning) { ?>

<div class="alert-warning warning text-center"><?php echo $error_warning; ?><img src="catalog/view/theme/default/image/close.png" alt="" class="close"></div>

<?php } ?>
         </div>
       </div>
</div>

<div class="container">
   <div class="row">
        <h1 class="clrorg col-xs-12 tittle cps" style="margin-top:10px;"><?php echo $heading_title; ?></h1>
   </div>
</div>



<?php echo $column_left; ?><?php echo $column_right; ?>

<div id="content" class="col-lg-9 col-sm-9 col-xs-12 col-lg-12"><?php echo $content_top; ?>


  <h4><strong><?php echo $text_address_book; ?></strong></h4>

  <?php foreach ($addresses as $result) { ?>

  <div class="content">
    <div class="table-responsive">
    <table class="table">
	  <tbody class="login-bg">
      <tr>

        <td><p><?php echo $result['address']; ?></p></td>

        <td style="text-align: right;vertical-align: middle;"><a href="<?php echo $result['update']; ?>" class="button back-btn"><?php echo $button_edit; ?></a> &nbsp; <a href="<?php echo $result['delete']; ?>" class="button back-btn"><?php echo $button_delete; ?></a></td>

      </tr>
	  </tbody>
    </table>
  </div>
  </div>

  <?php } ?>

  <div class="buttons">

    <!-- <div class="pull-left mr10"><a href="<?php echo $back; ?>" class="button back-btn"><?php echo $button_back; ?></a></div> -->

    <div><a href="<?php echo $insert; ?>" class="button back-btn"><?php echo $button_new_address; ?></a></div>

  </div>

</div>
</div>

  <?php echo $content_bottom; ?></div>

<?php echo $footer; ?>