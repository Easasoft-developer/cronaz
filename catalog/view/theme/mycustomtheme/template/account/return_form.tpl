<?php echo $header; ?>

<div class="container">
   <div class="row">
   <ul class="crumb col-xs-12">
   
       <?php foreach ($breadcrumbs as $breadcrumb) { ?>
   
       <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
   
       <?php } ?>
   
     </ul>
     </div>
</div>

<div class="container">

<div class="row">
<h1 class="clrorg tittle cps col-xs-12" style="margin-top:10px;"><?php echo $heading_title; ?></h1></div>

<div class="row">
  <div class="col-xs-12">
    <?php if ($error_warning) { ?>

<div class="alert-warning warning"><?php echo $error_warning; ?></div>

<?php } ?>
  </div>
</div>
</div>



<?php echo $column_left; ?><?php echo $column_right; ?>

<div id="content" class="col-lg-9 col-md-9 col-sm-9 col-xs-12"><?php echo $content_top; ?>


  <?php echo $text_description; ?>

  <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">

    <h4><strong><?php echo $text_order; ?></strong></h4>

    <div class="content">
    <table width="100%">
    <tbody>

      <tr>
      <td><span class="required">*</span> <?php echo $entry_firstname; ?></td>

       <td> <input type="text" name="firstname" value="<?php echo $firstname; ?>" class="large-field login col-lg-9 col-md-9 col-xs-12" /></td>

        

        <?php if ($error_firstname) { ?>

        <span class="error"><?php echo $error_firstname; ?></span>

        <?php } ?>

        </tr>

        <tr>

        <td><span class="required">*</span> <?php echo $entry_lastname; ?></td>

        <td><input type="text" name="lastname" value="<?php echo $lastname; ?>" class="large-field login col-lg-9 col-md-9 col-xs-12" /></td>       

        <?php if ($error_lastname) { ?>

        <span class="error"><?php echo $error_lastname; ?></span>

        <?php } ?>

      </tr>

      <tr>

        <td><span class="required">*</span> <?php echo $entry_email; ?></td>

        <td><input type="email" name="email" value="<?php echo $email; ?>" class="large-field login col-lg-9 col-md-9 col-xs-12" /></td>

        

        <?php if ($error_email) { ?>

        <span class="error"><?php echo $error_email; ?></span>

        <?php } ?>

        </tr>


        <tr>

        <td><span class="required">*</span> <?php echo $entry_telephone; ?></td>

        <td><input type="tel" name="telephone" value="<?php echo $telephone; ?>" class="large-field login col-lg-9 col-md-9 col-xs-12" /></td>

       

        <?php if ($error_telephone) { ?>

        <span class="error"><?php echo $error_telephone; ?></span>

        <?php } ?>

       </tr>
             
      <tr>

      <td><span class="required">*</span> <?php echo $entry_order_id; ?></td>

        <td><input type="text" name="order_id" value="<?php echo $order_id; ?>" class="large-field login col-lg-9 col-md-9 col-xs-12" /></td>

        

        <?php if ($error_order_id) { ?>

        <span class="error"><?php echo $error_order_id; ?></span>

        <?php } ?>

        </tr>
        <tr>

        <td><?php echo $entry_date_ordered; ?></td>

        <td><input type="text" name="date_ordered" value="<?php echo $date_ordered; ?>" class="large-field date login col-lg-9 col-md-9 col-xs-12" /></td>

        </tr>

     
      </tbody>
  
      </table>

    </div>

    <h4><strong><?php echo $text_product;?></strong></h4>

    <div id="return-product" class="row">

      <div class="content col-xs-12">

        <div class="return-product">
        <table width="100%">
        <tbody>
        <tr>

          <td style="width: 34%;">
          <span class="required">*</span> 
          <span><?php echo $entry_product; ?></span>
          </td>

          <td>
          <input type="text" name="product" value="<?php echo $product; ?>" class="col-lg-9 col-md-9 col-xs-12 login" /></td>           

            <?php if ($error_product) { ?>

            <span class="error"><?php echo $error_product; ?></span>

            <?php } ?>

          
          </tr>
          <tr>

          <td style="width: 34%;"><span class="required">*</span> <span><?php echo $entry_model; ?></span></td>

            <td><input type="text" name="model" value="<?php echo $model; ?>" class="col-lg-9 col-md-9 col-xs-12 login" /></td>

           

            <?php if ($error_model) { ?>

            <span class="error"><?php echo $error_model; ?></span>

            <?php } ?>

          
          </tr>

          <td style="width: 34%;"><span><?php echo $entry_quantity; ?></span></td>

            <td><input type="text" name="quantity" value="<?php echo $quantity; ?>" class="col-lg-9 col-md-9 col-xs-12 login" /></td>

          
          </tr>
          </tbody>
          </table>

        </div>
        

        <div class="return-detail">

          <div class="return-reason"><span class="required">*</span> <span><?php echo $entry_reason; ?></span><br>

            <table>

              <?php foreach ($return_reasons as $return_reason) { ?>

              <?php if ($return_reason['return_reason_id'] == $return_reason_id) { ?>

              <tr>

                <td width="1"><input type="radio" name="return_reason_id" value="<?php echo $return_reason['return_reason_id']; ?>" id="return-reason-id<?php echo $return_reason['return_reason_id']; ?>" checked="checked" /></td>

                <td><label for="return-reason-id<?php echo $return_reason['return_reason_id']; ?>"><?php echo $return_reason['name']; ?></label></td>

              </tr>

              <?php } else { ?>

              <tr>

                <td width="1"><input type="radio" name="return_reason_id" value="<?php echo $return_reason['return_reason_id']; ?>" id="return-reason-id<?php echo $return_reason['return_reason_id']; ?>" /></td>

                <td><label for="return-reason-id<?php echo $return_reason['return_reason_id']; ?>"><?php echo $return_reason['name']; ?></label></td>

              </tr>

              <?php  } ?>

              <?php  } ?>

            </table>

            <?php if ($error_reason) { ?>

            <span class="error"><?php echo $error_reason; ?></span>

            <?php } ?>

          </div>

          <div class="return-opened mtb20"><span><?php echo $entry_opened; ?></span>

         <br>

            <div class="mb10" style="padding-left: 10px;"><?php if ($opened) { ?>
            
                        &nbsp;<input type="radio" name="opened" value="1" id="opened" checked="checked" />
            
                        <?php } else { ?>
            
                        <input type="radio" name="opened" value="1" id="opened" />
            
                        <?php } ?>
            
                        <label for="opened"><?php echo $text_yes; ?></label>
            
                        <?php if (!$opened) { ?>
            
                        <input type="radio" name="opened" value="0" id="unopened" checked="checked" />
            
                        <?php } else { ?>
            
                        <input type="radio" name="opened" value="0" id="unopened" />
            
                        <?php } ?>
            
                        <label for="unopened"><?php echo $text_no; ?></label></div>

            
            <?php echo $entry_fault_detail; ?><br>

            <textarea name="comment" style="width: 100%;" cols="150" rows="6"><?php echo $comment; ?></textarea>

          </div>

          <div class="return-captcha"><b><?php echo $entry_captcha; ?></b><br>

          <img src="index.php?route=account/return/captcha" alt="" />

            <?php if ($error_captcha) { ?>

            <span class="error"><?php echo $error_captcha; ?></span>

            <?php } ?>

             <br><br>
            <input type="text" name="captcha" value="<?php echo $captcha; ?>" class="col-md-3 col-xs-12" />

            <br>

           

          </div>

        </div>

      </div>

    </div>

    <?php if ($text_agree) { ?>

    <div class="buttons">

      <div class="left"><a href="<?php echo $back; ?>" class="button continue-btn"><?php echo $button_back; ?></a></div>

      <div class="right"><?php echo $text_agree; ?>

        <?php if ($agree) { ?>

        <input type="checkbox" name="agree" value="1" checked="checked" />

        <?php } else { ?>

        <input type="checkbox" name="agree" value="1" />

        <?php } ?>

        <input type="submit" value="<?php echo $button_continue; ?>" class="button" />

      </div>

    </div>

    <?php } else { ?>

    <div class="buttons mt25">

      <div class="pull-left"><a href="<?php echo $back; ?>" class="button continue-btn"><?php echo $button_back; ?></a></div>

      <div class="pull-right">

        <input type="submit" value="<?php echo $button_continue; ?>" class="button continue-btn" />

      </div>

    </div>
    </div>

    <?php } ?>

  </form>
  </div>

  <?php echo $content_bottom; ?></div>

<script type="text/javascript"><!--

$(document).ready(function() {

	$('.date').datepicker({dateFormat: 'yy-mm-dd'});

});

//--></script> 

<script type="text/javascript"><!--

$(document).ready(function() {

	$('.colorbox').colorbox({

		width: 640,

		height: 480

	});

});

//--></script> 

<?php echo $footer; ?>