<?php echo $header; ?>


<div class="container">
  <div class="row">
  <ul class="crumb col-xs-12">
  
      <?php foreach ($breadcrumbs as $breadcrumb) { ?>
  
      <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
  
      <?php } ?></li>
  
    </ul>
  </div>
</div>

<div class="container">
  <div class="row">

  <h1 class="clrorg col-lg-6 tittle" style="text-transform: uppercase;margin-top:10px;"><?php echo $heading_title; ?></h1>

  </div>
  
</div> 


<?php echo $column_left; ?>

<?php echo $column_right; ?>

<div id="content" class="col-lg-9 col-md-9 col-sm-9 col-xs-12"><?php echo $content_top; ?>
    
  <div class="row">    
        
   <div class="col-xs-12"> 
   <?php if ($success) { ?>
    
            <div class="success alert alert-success" style=""><span class="close" data-dismiss="alert">&times;</span><?php echo $success; ?></div>
            
            <?php } ?>
            
            <?php if ($error_warning) { ?>
            
            <div class="warning alert alert-warning" style=""><span class="close" data-dismiss="alert">&times;</span><?php echo $error_warning; ?></div>
            
            <?php } ?>  
    </div> 
        
    </div>




  <div class="row">
  <div class="login-content col-lg-12 col-md-12 col-sm-12 col-xs-12">
  
      <div class="row">
      <div class="left col-lg-6 col-md-6 col-sm-6 col-xs-12">
        
              <h4 class="mb20 mt20"><strong><?php echo $text_new_customer; ?></strong></h4>
        
              <div class="content" style="margin:20px 0;">
        
                <p class=""><big><strong><?php echo $text_register; ?></strong></big></p>
        
                <p class="fs14"><?php echo $text_register_account; ?></p>
        
                <a href="<?php echo $register; ?>" class="button continue-btn"><?php echo $button_continue; ?></a></div>
        
      </div>


      <div class="right col-lg-6 col-md-6 col-sm-6 col-xs-12 pull-right login-bg" style="">
      
  
        <h4 class="mb20 mt20"><strong><?php echo $text_returning_customer; ?></strong></h4>
  
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
  
          <div class="content">
  
            <p class="fs14"><?php echo $text_i_am_returning_customer; ?></p>
  
            <p class="fs14" style="margin: 0;"><strong><?php echo $entry_email; ?></strong></p>
  
            <input type="text" name="email" value="<?php echo $email; ?>" class="login" style="margin-bottom: 5px;width:100%;"/>
  
            
            <p class="fs14" style="margin: 0;"><strong><?php echo $entry_password; ?></strong></p>
  
            <input type="password" name="password" value="<?php echo $password; ?>" class="login" style="margin-bottom: 5px;width:100%;"/>  
  
  
            <p class="fs14"><a href="<?php echo $forgotten; ?>"><?php echo $text_forgotten; ?></a></p>
  
            <p><input type="submit" value="<?php echo $button_login; ?>" class="button continue-btn" /></p>
  
            <?php if ($redirect) { ?>
  
            <input type="hidden" name="redirect" value="<?php echo $redirect; ?>" />
  
            <?php } ?>
  
          </div>
  
        </form>
  
      </div>
      

      </div>
      
      
  
    </div></div>

  <?php echo $content_bottom; ?></div>
  </div>
</div>
<script type="text/javascript">

$('#login input').keydown(function(e) {

  if (e.keyCode == 13) {

    $('#login').submit();

  }

});


$('.close').click( function () {

$('.success.alert.alert-success').css('display','none');

$('.warning.alert.alert-warning').css('display','none');

});

</script> 

<?php echo $footer; ?>
