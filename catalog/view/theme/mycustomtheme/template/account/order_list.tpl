<?php echo $header; ?>

<div class="container">
  <div class="row">
  <ul class="crumb col-xs-12">
   
       <?php foreach ($breadcrumbs as $breadcrumb) { ?>
   
       <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
   
       <?php } ?>
   
     </ul> 
  </div> 
</div>

<div class="container">
<div class="row"><h1 class="clrorg col-xs-12 tittle cps" style="margin-top:10px;"><?php echo $heading_title; ?></h1></div>
</div>

<?php echo $column_left; ?><?php echo $column_right; ?>

<div id="content" class="col-lg-9 col-md-9 col-sm-9 col-xs-12"><?php echo $content_top; ?>


  <?php if ($orders) { ?>

  <?php foreach ($orders as $order) { ?>

  <div class="order-list table-responsive">
  <table class="table table-bordered">
  <tbody>
  <tr style="background-color:#E4E4E4">

    <td  class="order-id"><b><?php echo $text_order_id; ?></b>&nbsp;&nbsp;#<?php echo $order['order_id']; ?></td>
	
    <td  class="order-status"><b><?php echo $text_status; ?></b>&nbsp;&nbsp;<?php echo $order['status']; ?></td>
     <td class="order-status" ><b> Action</td>
    
</tr>
<tr>
    <div class="order-content">

      <td><div><b style="display: inline-block;padding-bottom: 10px;"><?php echo $text_date_added; ?></b>&nbsp;&nbsp;<?php echo $order['date_added']; ?><br />

        <b><?php echo $text_products; ?></b>&nbsp;&nbsp;<?php echo $order['products']; ?></div></td>

      <td><div><b style="display: inline-block;padding-bottom: 10px;"><?php echo $text_customer; ?></b>&nbsp;&nbsp;<?php echo $order['name']; ?><br />

        <b><?php echo $text_total; ?></b>&nbsp;&nbsp;<?php echo $order['total']; ?></div></td>
       

      <td><div class="order-info"><a href="<?php echo $order['href']; ?>"><img src="catalog/view/theme/default/image/info.png" alt="<?php echo $button_view; ?>" title="<?php echo $button_view; ?>" /></a>&nbsp;&nbsp;<a href="<?php echo $order['reorder']; ?>"><img src="catalog/view/theme/default/image/reorder.png" alt="<?php echo $button_reorder; ?>" title="<?php echo $button_reorder; ?>" /></a></div></td>

    </div>
    </tr>
    </tbody>
  </table>


  </div>
  

  <?php } ?>

  

  <?php } else { ?>

  <div class="content fs14"><p><?php echo $text_empty; ?></p></div>

  <?php } ?>

  

  <!-- <div class="buttons mt25">

    <div class="right"><a href="<?php echo $continue; ?>" class="button continue-btn"><?php echo $button_continue; ?></a></div>

  </div> -->
  </div>
  </div>

<div class="pagination"><?php echo $pagination; ?></div>
  <?php echo $content_bottom; ?></div>

<?php echo $footer; ?>
