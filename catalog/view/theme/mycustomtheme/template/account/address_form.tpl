<?php echo $header; ?>

<div class="container">
  <div class="row">
  <ul class="crumb col-xs-12">
  
      <?php foreach ($breadcrumbs as $breadcrumb) { ?>
  
      <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
  
      <?php } ?>
  
    </ul>
    </div>
</div>

<div class="container">
   <div class="row">
  <div class="col-xs-12"> <h1 class="clrorg tittle cps" style="margin-top:10px;"><?php echo $heading_title; ?></h1></div>
   </div>
</div>

<?php echo $column_left; ?><?php echo $column_right; ?>


<div id="content" class="col-lg-9 col-sm-9 col-lg-9 col-xs-12"><?php echo $content_top; ?>

  <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">

    <h4 class="mt20"><strong><?php echo $text_edit_address; ?></strong></h4>

    <div class="content">

      <div class="form">

        <div class="row">

          <div class="col-md-3 col-sm-4 col-lg-3 col-xs-12"><p style="margin-top:10px;"><span class="required">*</span><?php echo $entry_firstname; ?></p></div>

          <div class="col-md-5 col-sm-7 col-lg-5 col-xs-12"><input type="text" name="firstname" value="<?php echo $firstname; ?>" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 login" />

            <?php if ($error_firstname) { ?>

            <span class="error"><?php echo $error_firstname; ?></span>

            <?php } ?></div>

        </div>

        <div class="row">

          <div class="col-md-3 col-sm-4 col-lg-3 col-xs-12"><p style="margin-top:10px;"><span class="required">*</span><?php echo $entry_lastname; ?></p></div>

          <div class="col-md-5 col-sm-7 col-lg-5 col-xs-12"><input type="text" name="lastname" value="<?php echo $lastname; ?>" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 login" />

            <?php if ($error_lastname) { ?>

            <span class="error"><?php echo $error_lastname; ?></span>

            <?php } ?></div>

        </div>

        <div class="row">

          <div class="col-md-3 col-sm-4 col-lg-3 col-xs-12"><p style="margin-top:10px;"><?php echo $entry_company; ?></p></div>

          <div class="col-md-5 col-sm-7 col-lg-5 col-xs-12"><input type="text" name="company" value="<?php echo $company; ?>" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 login" /></div>

        </div>

        <?php if ($company_id_display) { ?>

        <div class="row">

          <div class="col-md-3 col-sm-4 col-lg-3 col-xs-12"><p style="margin-top:10px;"><?php echo $entry_company_id; ?></p></div>

          <div class="col-md-5 col-sm-7 col-lg-5 col-xs-12"><input type="text" name="company_id" value="<?php echo $company_id; ?>" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 login" />

            <?php if ($error_company_id) { ?>

            <span class="error"><?php echo $error_company_id; ?></span>

            <?php } ?></div>

        </div>

        <?php } ?>

        <?php if ($tax_id_display) { ?>

        <div class="row">

          <div class="col-md-3 col-sm-4 col-lg-3 col-xs-12"><p style="margin-top:10px;"><?php echo $entry_tax_id; ?></p></div>

          <div class="col-md-5 col-sm-7 col-lg-5 col-xs-12"><input type="text" name="tax_id" value="<?php echo $tax_id; ?>" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 login" />

            <?php if ($error_tax_id) { ?>

            <span class="error"><?php echo $error_tax_id; ?></span>

            <?php } ?></div>

        </div>

        <?php } ?>

        <div class="row">

          <div class="col-md-3 col-sm-4 col-lg-3 col-xs-12"><p style="margin-top:10px;"><span class="required">*</span><?php echo $entry_address_1; ?></p></div>

          <div class="col-md-5 col-sm-7 col-lg-5 col-xs-12"><input type="text" name="address_1" value="<?php echo $address_1; ?>" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 login" />

            <?php if ($error_address_1) { ?>

            <span class="error"><?php echo $error_address_1; ?></span>

            <?php } ?></div>

        </div>

        <div class="row">

          <div class="col-md-3 col-sm-4 col-lg-3 col-xs-12"><p style="margin-top:10px;"><?php echo $entry_address_2; ?></p></div>

           <div class="col-md-5 col-sm-7 col-lg-5 col-xs-12"><input type="text" name="address_2" value="<?php echo $address_2; ?>" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 login" /></div>

        </div>

        <div class="row">

          <div class="col-md-3 col-sm-4 col-lg-3 col-xs-12"><p style="margin-top:10px;"><span class="required">*</span><?php echo $entry_city; ?></p></div>

          <div class="col-md-5 col-sm-7 col-lg-5 col-xs-12"><input type="text" name="city" value="<?php echo $city; ?>" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 login" />

            <?php if ($error_city) { ?>

            <span class="error"><?php echo $error_city; ?></span>

            <?php } ?></div>

        </div>

        <div class="row">

          <div class="col-md-3 col-sm-4 col-lg-3 col-xs-12"><p style="margin-top:10px;"><span id="postcode-required" class="required">*</span><?php echo $entry_postcode; ?></p></div>

          <div class="col-md-5 col-sm-7 col-lg-5 col-xs-12"><input type="text" name="postcode" value="<?php echo $postcode; ?>" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 login" />

            <?php if ($error_postcode) { ?>

            <span class="error"><?php echo $error_postcode; ?></span>

            <?php } ?></div>

        </div>

        <div class="row">

          <div class="col-md-3 col-sm-4 col-lg-3 col-xs-12"><p style="margin-top:10px;"><span class="required">*</span><?php echo $entry_country; ?></p></div>

          <div class="col-md-5 col-sm-7 col-lg-5 col-xs-12"><select name="country_id" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 login">

              <option value=""><?php echo $text_select; ?></option>

              <?php foreach ($countries as $country) { ?>

              <?php if ($country['country_id'] == $country_id) { ?>

              <option value="<?php echo $country['country_id']; ?>" selected="selected"><?php echo $country['name']; ?></option>

              <?php } else { ?>

              <option value="<?php echo $country['country_id']; ?>"><?php echo $country['name']; ?></option>

              <?php } ?>

              <?php } ?>

            </select>

            <?php if ($error_country) { ?>

            <span class="error"><?php echo $error_country; ?></span>

            <?php } ?></div>

        </div>

        <div class="row">

          <div class="col-md-3 col-sm-4 col-lg-3 col-xs-12"><p style="margin-top:10px;"><span class="required">*</span><?php echo $entry_zone; ?></p></div>

          <div class="col-md-5 col-sm-7 col-lg-5 col-xs-12"><select name="zone_id" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 login">

            </select>

            <?php if ($error_zone) { ?>

            <span class="error"><?php echo $error_zone; ?></span>

            <?php } ?></div>

        </div>

        <div class="row">

          <div class="col-md-3 col-sm-4 col-lg-3 col-xs-12"><p style="margin-top:10px;"><?php echo $entry_default; ?></p></div>

          <div class="col-md-5 col-sm-7 col-lg-5 col-xs-12" style="margin-top:10px;"><?php if ($default) { ?>

            <input type="radio" name="default" value="1" checked="checked" />

            <?php echo $text_yes; ?>

            <input type="radio" name="default" value="0" />

            <?php echo $text_no; ?>

            <?php } else { ?>

            <input type="radio" name="default" value="1" />

            <?php echo $text_yes; ?>

            <input type="radio" name="default" value="0" checked="checked" />

            <?php echo $text_no; ?>

            <?php } ?></div>

        </div>


        <div class="buttons">

          <div class="pull-left mr10"><a href="<?php echo $back; ?>" class="button back-btn"><?php echo $button_back; ?></a></div>

          <div class="">

            <input type="submit" value="<?php //echo $button_continue; ?>Create" class="button continue-btn" />

          </div>

        </div>

      </div>

    </div>

    

  </form>
  </div>
  </div>

  <?php echo $content_bottom; ?></div>

<script type="text/javascript"><!--

$('select[name=\'country_id\']').bind('change', function() {

	$.ajax({

		url: 'index.php?route=account/address/country&country_id=' + this.value,

		dataType: 'json',

		beforeSend: function() {

			$('select[name=\'country_id\']').after('<span class="wait">&nbsp;<img src="catalog/view/theme/default/image/loading.gif" alt="" /></span>');

		},		

		complete: function() {

			$('.wait').remove();

		},			

		success: function(json) {

			if (json['postcode_required'] == '1') {

				$('#postcode-required').show();

			} else {

				$('#postcode-required').hide();

			}

			

			html = '<option value=""><?php echo $text_select; ?></option>';

			

			if (json['zone'] != '') {

				for (i = 0; i < json['zone'].length; i++) {

        			html += '<option value="' + json['zone'][i]['zone_id'] + '"';

	    			

					if (json['zone'][i]['zone_id'] == '<?php echo $zone_id; ?>') {

	      				html += ' selected="selected"';

	    			}

	

	    			html += '>' + json['zone'][i]['name'] + '</option>';

				}

			} else {

				html += '<option value="0" selected="selected"><?php echo $text_none; ?></option>';

			}

			

			$('select[name=\'zone_id\']').html(html);

		},

		error: function(xhr, ajaxOptions, thrownError) {

			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);

		}

	});

});



$('select[name=\'country_id\']').trigger('change');

//--></script> 

<?php echo $footer; ?>