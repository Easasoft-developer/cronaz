<?php echo $header; ?>

<style>
@media(max-width:991px)
{
  .dvLoginReg 
  {
    
  }
  .titleRegistration
  {
    margin-bottom: 0 !important;
  }
}
</style>

 <div class="container">
   <div class="row">
   <ul class="crumb col-xs-12">
   
       <?php foreach ($breadcrumbs as $breadcrumb) { ?>
   
       <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
   
       <?php } ?>
   
     </ul></div>

 </div>

 <div class="container">

<div class="row"> <h1 class="clrorg tittle col-xs-12 cps" style="margin-top:10px;"><?php echo $heading_title; ?></h1></div>

</div>

<?php if ($error_warning) { ?>

<div class="warning"><?php echo $error_warning; ?></div>

<?php } ?>

<?php echo $column_left; ?><?php echo $column_right; ?>

<div id="content" class="col-lg-9 col-sm-9 col-md-9 col-xs-12"><?php echo $content_top; ?>
 
<div class="row">
  <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">    

    <div class="content">

      <div class="col-md-12 col-sm-12 col-lg-12 col-xs-12">



        <div class="row">
            <div class="col-xs-12"><h4 class="mt20"><strong><?php echo $text_your_details; ?></strong></h4></div>
        </div>
        <div class="row">

          <div class="col-md-3 col-sm-4 col-lg-3 col-xs-12"><p style="margin-top: 10px;"><span class="required">*</span><?php echo $entry_firstname; ?></p></div>

          <div class="col-md-5 col-sm-7 col-lg-5 col-xs-12"><input type="text" name="firstname" value="<?php echo $firstname; ?>" class="col-lg-12 col-sm-12 col-lg-12 col-xs-12 login" />

            <?php if ($error_firstname) { ?>

            <span class="error"><?php echo $error_firstname; ?></span>

            <?php } ?></div>

        </div>

        <div class="row">

          <div class="col-md-3 col-sm-4 col-lg-3 col-xs-12"><p style="margin-top: 10px;"><span class="required">*</span><?php echo $entry_lastname; ?></p></div>

          <div class="col-md-5 col-sm-7 col-lg-5 col-xs-12"><input type="text" name="lastname" value="<?php echo $lastname; ?>" class="col-lg-12 col-sm-12 col-lg-12 col-xs-12 login" />

            <?php if ($error_lastname) { ?>

            <span class="error"><?php echo $error_lastname; ?></span>

            <?php } ?></div>

        </div>

        <div class="row">

          <div class="col-md-3 col-sm-4 col-lg-3 col-xs-12"><p style="margin-top: 10px;"><span class="required">*</span><?php echo $entry_email; ?></p></div>

          <div class="col-md-5 col-sm-7 col-lg-5 col-xs-12"><input type="email" name="email" value="<?php echo $email; ?>" class="col-lg-12 col-sm-12 col-lg-12 col-xs-12 login" />

           <?php if ($error_email) { ?>

            <span class="error"><?php echo $error_email; ?></span>

            <?php } ?></div>

        </div>

        <div class="row">

          <div class="col-md-3 col-sm-4 col-lg-3 col-xs-12"><p style="margin-top: 10px;"><span class="required">*</span><?php echo $entry_telephone; ?></p></div>

          <div class="col-md-5 col-sm-7 col-lg-5 col-xs-12"><input type="tel" name="telephone" value="<?php echo $telephone; ?>" class="col-lg-12 col-sm-12 col-lg-12 col-xs-12 login" />

            <?php if ($error_telephone) { ?>

            <span class="error"><?php echo $error_telephone; ?></span>

            <?php } ?></div>

        </div>

        <div class="row">

          <div class="col-md-3 col-sm-4 col-lg-3 col-xs-12"><p style="margin-top: 10px;"><span class="required" style="color:#FFF">*</span><?php echo $entry_fax; ?></p></div>

          <div class="col-md-5 col-sm-7 col-lg-5 col-xs-12"><input type="tel" name="fax" value="<?php echo $fax; ?>" class="col-lg-12 col-sm-12 col-lg-12 col-xs-12 login" />
                  
          <?php if ($error_fax) { ?>

            <span class="error"><?php echo $error_fax; ?></span>

            <?php } ?>
          
          </div>

        </div>

      </div>
      
      
       <div class="buttons mt25 col-md-12 col-sm-12 col-lg-12 col-xs-12">

      <!-- <div class="pull-left mr10"><a href="<?php echo $back; ?>" class="button back-btn"><?php echo $button_back; ?></a></div> -->

      <div class="pull-left">

        <input type="submit" value="<?php //echo $button_continue; ?>Update" class="button continue-btn" />

      </div>

    </div>
      
      

    </div>

   

  </form></div>
  </div>
  </div>


  <?php echo $content_bottom; ?></div>

<?php echo $footer; ?>