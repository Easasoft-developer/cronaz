<?php echo $header; ?>

<style type="text/css">
  .dvLoginReg 
  {
    margin-top: 0 !important;
  }

  @media(max-width:991px)
{
  .dvLoginReg 
  {
    float:left !important;
  }
}
</style>

<div class="container">
  <ul class="crumb">

    <?php foreach ($breadcrumbs as $breadcrumb) { ?>

    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>

    <?php } ?>

  </ul>  
</div>

<div class="container"><h1 class="clrorg mt53 tittle mb20 cps"><?php echo $heading_title; ?></h1></div>

<?php echo $column_left; ?><?php echo $column_right; ?>

<div id="content" class="col-lg-9 col-md-9 col-sm-9 col-xs-12"><?php echo $content_top; ?>

  <!-- <ul class="crumb">

    <?php foreach ($breadcrumbs as $breadcrumb) { ?>

    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>

    <?php } ?>

  </ul> -->

  <!-- <h1 class="clrorg mt53 tittle mb20 cps"><?php echo $heading_title; ?></h1> -->
  
  <?php if ($orders) { ?>

  <?php foreach ($orders as $order) { ?>

  <div class="order-list row dvTblScroll" style="width:100%;">
  <table class="table-bordered" width="100%">
  <tbody>
  <tr style="background-color:#E4E4E4">

    <td style="padding:10px;" class="order-id"><b><?php echo $text_order_id; ?></b>&nbsp;&nbsp;#<?php echo $order['order_id']; ?></td>
	
    <td style="padding:10px;" class="order-status"><b><?php echo $text_status; ?></b>&nbsp;&nbsp;<?php echo $order['status']; ?></td>
     <td class="order-status" style="padding:10px;"><b> Action</td>
    
</tr>
<tr>
    <div class="order-content">

      <td style="padding:10px; white-space: nowrap;"><div><b style="display: inline-block;padding-bottom: 10px;"><?php echo $text_date_added; ?></b>&nbsp;&nbsp;<?php echo $order['date_added']; ?><br />

        <b><?php echo $text_products; ?></b>&nbsp;&nbsp;<?php echo $order['products']; ?></div></td>

      <td style="padding:10px; white-space: nowrap;"><div><b style="display: inline-block;padding-bottom: 10px;"><?php echo $text_customer; ?></b>&nbsp;&nbsp;<?php echo $order['name']; ?><br />

        <b><?php echo $text_total; ?></b>&nbsp;&nbsp;<?php echo $order['total']; ?></div></td>
       

      <td style="padding:10px; white-space: nowrap;"><div class="order-info"><a href="<?php echo $order['href']; ?>"><img src="catalog/view/theme/default/image/info.png" alt="<?php echo $button_view; ?>" title="<?php echo $button_view; ?>" /></a>&nbsp;&nbsp;<a href="<?php echo $order['reorder']; ?>"><img src="catalog/view/theme/default/image/reorder.png" alt="<?php echo $button_reorder; ?>" title="<?php echo $button_reorder; ?>" /></a></div></td>

    </div>
    </tr>
    </tbody>
  </table>


  </div>
  

  <?php } ?>

  

  <?php } else { ?>

  <div class="content fs14"><p><?php echo $text_empty; ?></p></div>

  <?php } ?>
  

  <!-- <div class="buttons mt25">

    <div class="right"><a href="<?php echo $continue; ?>" class="button continue-btn"><?php echo $button_continue; ?></a></div>

  </div> -->
  </div>
<div class="pagination"><?php echo $pagination; ?></div>
  <?php echo $content_bottom; ?></div>

<?php echo $footer; ?>