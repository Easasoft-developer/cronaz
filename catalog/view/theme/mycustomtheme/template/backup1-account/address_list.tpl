<?php echo $header; ?>

<style>
@media(max-width:991px)
{
  .dvLoginReg 
  {
    float:left !important;
  }
}
</style>

<div classs="container">
   <ul class="crumb">

    <?php foreach ($breadcrumbs as $breadcrumb) { ?>

    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>

    <?php } ?>

  </ul>
</div>

<div class="container">
   <h1 class="clrorg mt53 tittle cps"><?php echo $heading_title; ?></h1>
</div>

<?php if ($success) { ?>

<div class="success"><?php echo $success; ?></div>

<?php } ?>

<?php if ($error_warning) { ?>

<div class="warning" style="color: rgb(255, 36, 36);text-align: center;"><?php echo $error_warning; ?></div>

<?php } ?>

<?php echo $column_left; ?><?php echo $column_right; ?>

<div id="content" class="col-lg-9 col-sm-9 col-xs-12 col-lg-12"><?php echo $content_top; ?>

 <!--  <ul class="crumb">

    <?php foreach ($breadcrumbs as $breadcrumb) { ?>

    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>

    <?php } ?>

  </ul> -->

 <!--  <h1 class="clrorg mt53 tittle cps"><?php echo $heading_title; ?></h1> -->

  <h4 class="mt20 row"><strong><?php echo $text_address_book; ?></strong></h4>

  <?php foreach ($addresses as $result) { ?>

  <div class="content row" style="border-bottom:5px solid #fff">

    <table style="width: 100%;">
	  <tbody class="login-bg">
      <tr>

        <td style="padding: 10px;"><p><?php echo $result['address']; ?></p></td>

        <td style="text-align: right;padding: 18px;vertical-align: bottom;"><a href="<?php echo $result['update']; ?>" class="button back-btn"><?php echo $button_edit; ?></a> &nbsp; <a href="<?php echo $result['delete']; ?>" class="button back-btn"><?php echo $button_delete; ?></a></td>

      </tr>
	  </tbody>
    </table>

  </div>

  <?php } ?>

  <div class="buttons mt25 row">

    <!-- <div class="pull-left mr10"><a href="<?php echo $back; ?>" class="button back-btn"><?php echo $button_back; ?></a></div> -->

    <div class="pull-left"><a href="<?php echo $insert; ?>" class="button back-btn"><?php echo $button_new_address; ?></a></div>

  </div>

</div>

  <?php echo $content_bottom; ?></div>

<?php echo $footer; ?>