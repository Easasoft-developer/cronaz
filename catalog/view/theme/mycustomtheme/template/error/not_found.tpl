<?php echo $header; ?><?php echo $column_left; ?><?php echo $column_right; ?>

<div id="content" class="container"><?php echo $content_top; ?>

  <ul class="crumb">

    <?php foreach ($breadcrumbs as $breadcrumb) { ?>

    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>

    <?php } ?>

  </ul>

  <h1 class="clrorg tittle cps" style="margin-top:10px;"><?php echo $heading_title; ?></h1>

  <div class="content"><?php echo $text_error; ?></div>

  <div class="buttons mb28">

    <div class="right mt25"><a href="<?php echo $continue; ?>" class="button continue-btn"><?php echo $button_continue; ?></a></div>

  </div>

  <?php echo $content_bottom; ?></div>

<?php echo $footer; ?>