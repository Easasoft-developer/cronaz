<?php echo $header; ?><?php echo $column_left; ?><?php echo $column_right; ?>
<div id="content" class="container mb28"><?php echo $content_top; ?>
  <ul class="crumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>
  <h1 class="clrorg mt53 tittle cps"><?php echo $heading_title; ?></h1>
  <p class="fs14"><?php echo $text_message; ?></p>
  <!--<div class="buttons mt25">
    <div class="right"><a href="<?php echo $continue; ?>" class="button continue-btn"><?php echo $button_continue; ?></a></div>
  </div>-->
  </div>
  <?php echo $content_bottom; ?></div>
<?php echo $footer; ?>