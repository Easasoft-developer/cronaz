<?php if ($error_warning) { ?>
<div class="warning"><?php echo $error_warning; ?></div>
<?php } ?>
<?php if ($payment_methods) { ?>
<div class="col-xs-12">
<p class="mtb10"><?php echo $text_payment_method; ?></p>
<table class="radio">
  <?php foreach ($payment_methods as $payment_method) { ?>
  <tr class="highlight">
    <td><?php if ($payment_method['code'] == $code || !$code) { ?>
      <?php $code = $payment_method['code']; ?>
      <input type="radio" name="payment_method" value="<?php echo $payment_method['code']; ?>" id="<?php echo $payment_method['code']; ?>" checked="checked" />
      <?php } else { ?>
      <input type="radio" name="payment_method" value="<?php echo $payment_method['code']; ?>" id="<?php echo $payment_method['code']; ?>" />
      <?php } ?><label for="<?php echo $payment_method['code']; ?>"><?php echo $payment_method['title']; ?></label></td>
  </tr>
  <?php } ?>
</table>
</div>
<?php } ?>

<b class="pl8"><?php echo $text_comments; ?></b>
<textarea name="comment" rows="8" style="width: 98%;margin-left:8px;"><?php echo $comment; ?></textarea>
<br />
<br />
<?php if ($text_agree) { ?>
<div class="buttons mb20">

  <div class="right">

    <?php if ($agree) { ?>

    <div class="checkbox">
    <label for="payagree">
    <input id="payagree" type="checkbox" name="agree" value="1" checked="checked" /> <?php echo $text_agree; ?>
    </label>
    </div>

    

    <?php } else { ?>
    <div class="checkbox">
    <label for="payagree">
    <input id="payagree" type="checkbox" name="agree" value="1" /> <?php echo $text_agree; ?>
    </label>
    </div>
    

    <?php } ?>

    <input type="button" value="<?php echo $button_continue; ?>" id="button-payment-method" class="button continue-btn mt25" />

  </div>

</div>
<?php } else { ?>
<div class="buttons">
  <div class="right">
    <input type="button" value="<?php echo $button_continue; ?>" id="button-payment-method" class="button" />
  </div>
</div>
<?php } ?>
<script type="text/javascript"><!--
$('.colorbox').colorbox({
	width: 640,
	height: 480
});
//--></script> 