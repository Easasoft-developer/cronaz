<div class="left col-sm-6">
  <h4><strong><?php echo $text_new_customer; ?></strong></h4>
  <p class="fs14"><?php echo $text_checkout; ?></p>
  <label for="register">
    <?php if ($account == 'register') { ?>
    <input type="radio" name="account" value="register" id="register" checked="checked" />
    <?php } else { ?>
    <input type="radio" name="account" value="register" id="register" />
    <?php } ?>
    <b><?php echo $text_register; ?></b></label>
  
  <?php if ($guest_checkout) { ?>
  &nbsp;<label for="guest">
    <?php if ($account == 'guest') { ?>
    <input type="radio" name="account" value="guest" id="guest" checked="checked" />
    <?php } else { ?>
    <input type="radio" name="account" value="guest" id="guest" />
    <?php } ?>
    <b><?php echo $text_guest; ?></b></label>
  
  <?php } ?>
  
  <p><?php echo $text_register_account; ?></p>
  <input type="button" value="<?php echo $button_continue; ?>" id="button-account" class="button continue-btn" />
  <br />
  <br />
</div>

<div id="login" class="right col-md-4 col-sm-6">
  <h4><strong><?php echo $text_returning_customer; ?></strong></h4>
  <p class="fs14"><?php echo $text_i_am_returning_customer; ?></p>
  <b><?php echo $entry_email; ?></b><br />
  <input class="login mb10" type="text" name="email" value="" />
  <br />
    
  <b><?php echo $entry_password; ?></b><br />
  <input class="login" type="password" name="password" value="" />
  <br />
  <a href="<?php echo $forgotten; ?>"><?php echo $text_forgotten; ?></a><br />
 
  <input type="button" value="<?php echo $button_login; ?>" id="button-login" class="button continue-btn mt10" /><br />
  <br />
</div>