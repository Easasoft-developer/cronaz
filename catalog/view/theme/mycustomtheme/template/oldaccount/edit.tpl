<?php echo $header; ?>

<?php if ($error_warning) { ?>

<div class="warning"><?php echo $error_warning; ?></div>

<?php } ?>

<?php echo $column_left; ?><?php echo $column_right; ?>

<div id="content" class="col-lg-9"><?php echo $content_top; ?>

  <ul class="crumb">

    <?php foreach ($breadcrumbs as $breadcrumb) { ?>

    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>

    <?php } ?>

  </ul>

  <h1 class="clrorg"><?php echo $heading_title; ?></h1>

  <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">

    <h2><?php echo $text_your_details; ?></h2>

    <div class="content">

      <table class="form table-responsive" style="width:70%">

        <tr>

          <td><span class="required">*</span> <big><?php echo $entry_firstname; ?></big></td>

          <td><input type="text" name="firstname" value="<?php echo $firstname; ?>" class="col-lg-12 login" />

            <?php if ($error_firstname) { ?>

            <span class="error"><?php echo $error_firstname; ?></span>

            <?php } ?></td>

        </tr>

        <tr>

          <td><span class="required">*</span> <big><?php echo $entry_lastname; ?></big></td>

          <td><input type="text" name="lastname" value="<?php echo $lastname; ?>" class="col-lg-12 login" />

            <?php if ($error_lastname) { ?>

            <span class="error"><?php echo $error_lastname; ?></span>

            <?php } ?></td>

        </tr>

        <tr>

          <td><span class="required">*</span> <big><?php echo $entry_email; ?></big></td>

          <td><input type="text" name="email" value="<?php echo $email; ?>" class="col-lg-12 login" />

           <?php if ($error_email) { ?>

            <span class="error"><?php echo $error_email; ?></span>

            <?php } ?></td>

        </tr>

        <tr>

          <td><span class="required">*</span> <big><?php echo $entry_telephone; ?></big></td>

          <td><input type="text" name="telephone" value="<?php echo $telephone; ?>" class="col-lg-12 login" />

            <?php if ($error_telephone) { ?>

            <span class="error"><?php echo $error_telephone; ?></span>

            <?php } ?></td>

        </tr>

        <tr>

          <td><big><?php echo $entry_fax; ?></big></td>

          <td><input type="text" name="fax" value="<?php echo $fax; ?>" class="col-lg-12 login" /></td>

        </tr>

      </table>

    </div>

    <div class="buttons mt25">

      <div class="pull-left mr138"><a href="<?php echo $back; ?>" class="button back-btn"><?php echo $button_back; ?></a></div>

      <div class="text-center">

        <input type="submit" value="<?php echo $button_continue; ?>" class="button continue-btn" />

      </div>

    </div>

  </form>
  </div>

  <?php echo $content_bottom; ?></div>

<?php echo $footer; ?>