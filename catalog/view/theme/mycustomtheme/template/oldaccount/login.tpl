<?php echo $header; ?>


<?php echo $column_left; ?>
<?php echo $column_right; ?>

<div id="content" class="col-lg-9 row"><?php echo $content_top; ?>

  <ul class="crumb hidden-xs">

    <?php foreach ($breadcrumbs as $breadcrumb) { ?>

    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>

    <?php } ?></li>

  </ul>
  
  <div class="col-lg-12 row">
  	<h1 class="clrorg col-lg-6 row"><?php echo $heading_title; ?></h1>
        <div class="col-lg-6 mt25 pull-right">
		<?php if ($success) { ?>

        <div class="success alert alert-success"><a href="#" class="close" data-dismiss="alert">&times;</a><?php echo $success; ?></div>
        
        <?php } ?>
        
        <?php if ($error_warning) { ?>
        
        <div class="warning alert alert-warning"><a href="#" class="close" data-dismiss="alert">&times;</a><?php echo $error_warning; ?></div>
        
        <?php } ?>
        
    	</div>
        
    </div>


  <!--<h1 class="clrorg"><?php echo $heading_title; ?></h1>-->

  <div class="login-content col-lg-12 row">

    <div class="left col-lg-6 row">

      <h2 class="mb20"><?php echo $text_new_customer; ?></h2>

      <div class="content">

        <p class="clrorg"><big><strong><?php echo $text_register; ?></strong></big></p>

        <p class="mb28 fs14"><?php echo $text_register_account; ?></p>

        <a href="<?php echo $register; ?>" class="button continue-btn"><?php echo $button_continue; ?></a></div>

    </div>
		
    <div class="right col-lg-6 pull-right login-bg">
    

      <h2 class="mb20"><?php echo $text_returning_customer; ?></h2>

      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">

        <div class="content">

          <p class="fs14"><?php echo $text_i_am_returning_customer; ?></p>

          <big><strong><?php echo $entry_email; ?></strong></big><br />

          <input type="text" name="email" value="<?php echo $email; ?>" class="col-lg-9 login" />

          <br />

          <br />
          <br />

          <big><strong><?php echo $entry_password; ?></strong></big><br />

          <input type="password" name="password" value="<?php echo $password; ?>" class="col-lg-9 login" />

          <br /><br /><br />

          <a href="<?php echo $forgotten; ?>"><?php echo $text_forgotten; ?></a><br />

          <br />

          <input type="submit" value="<?php echo $button_login; ?>" class="button continue-btn" />

          <?php if ($redirect) { ?>

          <input type="hidden" name="redirect" value="<?php echo $redirect; ?>" />

          <?php } ?>

        </div>

      </form>

    </div>

  </div>

  <?php echo $content_bottom; ?></div>
</div>
<script type="text/javascript"><!--

$('#login input').keydown(function(e) {

	if (e.keyCode == 13) {

		$('#login').submit();

	}

});

//--></script> 

<?php echo $footer; ?>