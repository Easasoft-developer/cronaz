<?php echo $header; ?>

<style type="text/css">
    
    .content p + input{margin-bottom: 10px;}   
    .content p + textarea{margin-bottom: 10px;}  

    .infocontact span {margin-bottom: 15px;}

</style>

<?php echo $column_left; ?><?php echo $column_right; ?>

<div id="content" class="container inner-cont">

<?php echo $content_top; ?>

  <div class="row">

  <ul class="crumb col-xs-12">
  
      <?php foreach ($breadcrumbs as $breadcrumb) { ?>
  
      <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
  
      <?php } ?>
  
    </ul>

</div>

  
<div class="row">

<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
  
    <h1 class="clrorg tittle"><?php echo $heading_title; ?></h1>
    <h2 class="tittle"><?php //echo $text_location; ?></h2>

    <div class="contact-info">

          <div class="content">
          <div class="contact-addr"><b>
          <?php //echo $text_address; ?>Corporate Office</b><br />      
             <div>      <br />    
                CRONAZ INDUSTRIES,<br />

                63/13, Arcot Road, <br />

                Kodambakkam,Chennai – 600024,<br />

                Tamilnadu, India <br />

                Email: support@cronaz.com
            </div>
          
          </div>

        </div>

    </div>
</div>

<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 infocontact">
    <h2 class="tittle" style="margin-top: 15px;"><?php echo $text_contact; ?></h2>

    <div class="content">

    <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">

    <p><?php echo $entry_name; ?></p>

    <input type="text" name="name" value="<?php echo $name; ?>" class="login" />

    <?php if ($error_name) { ?>

    <span class="error"><?php echo $error_name; ?></span>

    <?php } ?>

   

    <p><?php echo $entry_email; ?></p>

    <input type="text" name="email" value="<?php echo $email; ?>" class="login" />

    

    <?php if ($error_email) { ?>

    <span class="error"><?php echo $error_email; ?></span>

    <?php } ?>

   

    <p><?php echo $entry_enquiry; ?></p>

    <textarea name="enquiry" cols="40" rows="10" class=""><?php echo $enquiry; ?></textarea>
    

    <?php if ($error_enquiry) { ?>

    <span class="error"><?php echo $error_enquiry; ?></span>

    <?php } ?>

    

    <p><?php echo $entry_captcha; ?></p>

    <input type="text" name="captcha" value="<?php echo $captcha; ?>" class="login" />

    

    <img src="index.php?route=information/contact/captcha" alt="" />

    <?php if ($error_captcha) { ?>

    <span class="error"><?php echo $error_captcha; ?></span>

    <?php } ?>

 
 
    

    <div class="buttons mt10">

      <div class="right"><input type="submit" value="Submit" class="button" /></div>

    </div>
    </form>
    </div>   

</div>
</div>
  <?php echo $content_bottom; ?>
  </div>
  </div>

<?php echo $footer; ?>