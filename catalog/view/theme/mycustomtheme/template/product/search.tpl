<?php echo $header; ?>

<div class="container">

<div class="row">
	<div class="col-sm-12">
		<ul class="crumb">

    <?php foreach ($breadcrumbs as $breadcrumb) { ?>

    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>

    <?php } ?>

  </ul>
	</div>
</div>




</div>

<div class="container">


<div class="row">
	<div class="col-sm-12">
		<h1 class="clrorg tittle mt53 cps" style="margin-top:10px;"><?php echo $heading_title; ?></h1>
	</div>
</div>




</div>

<?php echo $column_left; ?><?php echo $column_right; ?>

<div id="content" class="container mb28"><?php echo $content_top; ?>


  <h4><strong><?php echo $text_critea; ?></strong></h4>

  <div class="content">

  		<form class="form-inline" role="form">      
      	<div class="form-group">
      		<label class="sr-only" for=""><?php echo $entry_search; ?></label><?php echo $entry_search; ?>&nbsp;&nbsp;
      		<?php if ($search) { ?>

	      	<input type="text" name="search" value="<?php echo $search; ?>" class="login form-control" />

	      	<?php } else { ?>
	      	<input type="text" name="search" value="<?php echo $search; ?>" onclick="this.value = '';" onkeydown="this.style.color = '000000'" class="login search-text" />

	      <?php } ?>
      	</div>

      	<div class="form-group">
      		<select name="category_id" class="login" style="">

	        <option value="0"><?php echo $text_category; ?></option>

	        <?php foreach ($categories as $category_1) { ?>

	        <?php if ($category_1['category_id'] == $category_id) { ?>

	        <option value="<?php echo $category_1['category_id']; ?>" selected="selected"><?php echo $category_1['name']; ?></option>

	        <?php } else { ?>

	        <option value="<?php echo $category_1['category_id']; ?>"><?php echo $category_1['name']; ?></option>

	        <?php } ?>

	        <?php foreach ($category_1['children'] as $category_2) { ?>

	        <?php if ($category_2['category_id'] == $category_id) { ?>

	        <option value="<?php echo $category_2['category_id']; ?>" selected="selected">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $category_2['name']; ?></option>

	        <?php } else { ?>

	        <option value="<?php echo $category_2['category_id']; ?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $category_2['name']; ?></option>

	        <?php } ?>

	        <?php foreach ($category_2['children'] as $category_3) { ?>

	        <?php if ($category_3['category_id'] == $category_id) { ?>

	        <option value="<?php echo $category_3['category_id']; ?>" selected="selected">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $category_3['name']; ?></option>

	        <?php } else { ?>

	        <option value="<?php echo $category_3['category_id']; ?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $category_3['name']; ?></option>

	        <?php } ?>

	        <?php } ?>

	        <?php } ?>

	        <?php } ?>

	      </select>
      	</div>

      	<div style="margin-top:15px;">
	      <?php if ($sub_category) { ?>

	      <input type="checkbox" name="sub_category" value="1" id="sub_category" checked="checked" />

	      <?php } else { ?>

	      <input type="checkbox" name="sub_category" value="1" id="sub_category" />

	      <?php } ?>

	      <label for="sub_category"><?php echo $text_sub_category; ?></label>
			</div>

    <div style="margin-top:15px;">
	    <?php if ($description) { ?>

	    <input type="checkbox" name="description" value="1" id="description" checked="checked" />

	    <?php } else { ?>

	    <input type="checkbox" name="description" value="1" id="description" />

	    <?php } ?>

	    <label for="description"> <?php echo $entry_description; ?></label>
	</div>


      </form>    
</div>
  <div class="buttons mt25" style="margin-bottom:15px;">

    <div class="right"><input type="button" value="<?php echo $button_search; ?>" id="button-search" class="button" /></div>

  </div>

  <h4><strong><?php echo $text_search; ?></strong></h4>

  <?php if ($products) { ?>

  <div class="product-filter mb28 row" style="margin:20px 0;" >

    <div class="col-md-3"><a href="<?php echo $compare; ?>" id="compare-total"><?php echo $text_compare; ?></a></div>

    <div class="limit dvLimitSort col-md-3 col-sm-6 col-xs-12 text-right"><?php echo $text_limit; ?>

      <select onchange="location = this.value;">

        <?php foreach ($limits as $limits) { ?>

        <?php if ($limits['value'] == $limit) { ?>

        <option value="<?php echo $limits['href']; ?>" selected="selected"><?php echo $limits['text']; ?></option>

        <?php } else { ?>

        <option value="<?php echo $limits['href']; ?>"><?php echo $limits['text']; ?></option>

        <?php } ?>

        <?php } ?>

      </select>

    </div>

    <div class="sort dvLimitSort col-md-4 col-md-offset-2 col-sm-6 col-xs-12 text-right"><?php echo $text_sort; ?>

      <select onchange="location = this.value;">

        <?php foreach ($sorts as $sorts) { ?>

        <?php if ($sorts['value'] == $sort . '-' . $order) { ?>

        <option value="<?php echo $sorts['href']; ?>" selected="selected"><?php echo $sorts['text']; ?></option>

        <?php } else { ?>

        <option value="<?php echo $sorts['href']; ?>"><?php echo $sorts['text']; ?></option>

        <?php } ?>

        <?php } ?>

      </select>

    </div>

  </div>


  

  <div class="product-list row">

    <?php foreach ($products as $product) { ?>

    <div class="list-product1 dvProductThumbnail col-md-4 col-sm-6 col-lg-3 col-xs-6">
    <div class="thumbnail text-center">

      <?php if ($product['thumb']) { ?>

      <div class="image"><a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" title="<?php echo $product['name']; ?>" alt="<?php echo $product['name']; ?>" /></a></div>

      <?php } ?>

      <div class="name" style="padding-top:10px;"><a style="font-weight: bold;" href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></div>

     <!-- <div class="description"><?php echo $product['description']; ?></div>-->

      <?php if ($product['price']) { ?>

      <div class="price" style="margin-top:6px;color: #e8543a;margin-bottom: 6px;">

        <?php if (!$product['special']) { ?>

        <?php echo $product['price']; ?>

        <?php } else { ?>

        <span class="price-old"><?php echo $product['price']; ?></span> <span class="price-new"><?php echo $product['special']; ?></span>

        <?php } ?>

        <?php if ($product['tax']) { ?>

        <br />

        <span class="price-tax"><?php echo $text_tax; ?> <?php echo $product['tax']; ?></span>

        <?php } ?>

      </div>

      <?php } ?>

     <!-- <?php if ($product['rating']) { ?>

      <div class="rating"><img src="catalog/view/theme/default/image/stars-<?php echo $product['rating']; ?>.png" alt="<?php echo $product['reviews']; ?>" /></div>

      <?php } ?>-->

      <div class="cart"><input type="button" value="<?php echo $button_cart; ?>" onclick="addToCart('<?php echo $product['product_id']; ?>');" class="button continue-btn mb20" /></div>
<div style="text-align:center" class="mb20">
<div class="wishlist"><a onclick="addToWishList('56');" data-toggle="tooltip" data-placement="top" data-html="true" title="Add To Wish List"><span class="glyphicon glyphicon-heart"></span></a></div>
<div class="compare"><a onclick="addToCompare('56');" data-toggle="tooltip" data-placement="top" data-html="true" title="Add To Compare	"><span class="glyphicon glyphicon-refresh"></span></a></div>
</div>

</div>
    </div>

    <?php } ?>

  </div>

  <div class="pagination"><?php echo $pagination; ?></div>

  <?php } else { ?>

  <div class="content"><?php echo $text_empty; ?></div>

  <?php }?>

  <?php echo $content_bottom; ?>
</div>

<script type="text/javascript"><!--

$('#content input[name=\'search\']').keydown(function(e) {

	if (e.keyCode == 13) {

		$('#button-search').trigger('click');

	}

});



$('select[name=\'category_id\']').bind('change', function() {

	if (this.value == '0') {

		$('input[name=\'sub_category\']').attr('disabled', 'disabled');

		$('input[name=\'sub_category\']').removeAttr('checked');

	} else {

		$('input[name=\'sub_category\']').removeAttr('disabled');

	}

});



$('select[name=\'category_id\']').trigger('change');



$('#button-search').bind('click', function() {

	url = 'index.php?route=product/search';

	

	var search = $('#content input[name=\'search\']').attr('value');

	

	if (search) {

		url += '&search=' + encodeURIComponent(search);

	}



	var category_id = $('#content select[name=\'category_id\']').attr('value');

	

	if (category_id > 0) {

		url += '&category_id=' + encodeURIComponent(category_id);

	}

	

	var sub_category = $('#content input[name=\'sub_category\']:checked').attr('value');

	

	if (sub_category) {

		url += '&sub_category=true';

	}

		

	var filter_description = $('#content input[name=\'description\']:checked').attr('value');

	

	if (filter_description) {

		url += '&description=true';

	}



	location = url;

});



function display(view) {

	if (view == 'list') {

		/*$('.product-grid').attr('class', 'product-list');

		

		$('.product-list > div').each(function(index, element) {

			html  = '<div>';

			html += '  <div class="cart">' + $(element).find('.cart').html() + '</div>';

			html += '  <div class="wishlist">' + $(element).find('.wishlist').html() + '</div>';

			html += '  <div class="compare">' + $(element).find('.compare').html() + '</div>';

			html += '</div>';			

			

			html += '<div class="left">';

			

			var image = $(element).find('.image').html();

			

			if (image != null) { 

				html += '<div class="image">' + image + '</div>';

			}

			

			var price = $(element).find('.price').html();

			

			if (price != null) {

				html += '<div class="price">' + price  + '</div>';

			}

						

			html += '  <div class="name">' + $(element).find('.name').html() + '</div>';

			html += '  <div class="description">' + $(element).find('.description').html() + '</div>';

			

			var rating = $(element).find('.rating').html();

			

			if (rating != null) {

				html += '<div class="rating">' + rating + '</div>';

			}

				

			html += '</div>';

						

			$(element).html(html);

		});		

		

		$('.display').html('<b><?php echo $text_display; ?></b> <?php echo $text_list; ?> <b>/</b> <a onclick="display(\'grid\');"><?php echo $text_grid; ?></a>');

		

		$.totalStorage('display', 'list');*/ 

	} else {

		$('.product-list').attr('class', 'product-grid');

		

		$('.product-grid > div').each(function(index, element) {

			html = '';

			

			var image = $(element).find('.image').html();

			

			if (image != null) {

				html += '<div class="image">' + image + '</div>';

			}

			

			html += '<div class="name">' + $(element).find('.name').html() + '</div>';

			/*html += '<div class="description">' + $(element).find('.description').html() + '</div>';*/

			

			var price = $(element).find('.price').html();

			

			if (price != null) {

				html += '<div class="price">' + price  + '</div>';

			}	

					

			var rating = $(element).find('.rating').html();

			

			if (rating != null) {

				html += '<div class="rating">' + rating + '</div>';

			}

						

			html += '<div class="cart">' + $(element).find('.cart').html() + '</div>';

			html += '<div class="wishlist">' + $(element).find('.wishlist').html() + '</div>';

			html += '<div class="compare">' + $(element).find('.compare').html() + '</div>';

			

			$(element).html(html);

		});	

					

		$('.display').html('<b><?php echo $text_display; ?></b> <a onclick="display(\'list\');"><?php echo $text_list; ?></a> <b>/</b> <?php echo $text_grid; ?>');

		

		$.totalStorage('display', 'grid');

	}

}



view = $.totalStorage('display');



if (view) {

	display(view);

} else {

	display('list');

}

//--></script> 

<?php echo $footer; ?>