<?php echo $header; ?>
<style type="text/css">
 .success{position: relative;text-align: center;}
 .success img,.warning img{position: absolute;margin: 3px;}
</style>
<div class="container">
  
<ul class="crumb">
 
     <?php foreach ($breadcrumbs as $breadcrumb) { ?>
 
     <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
 
     <?php } ?>
 
   </ul>

     <div class="row">
       <div class="col-xs-12">
         <?php if ($success) { ?>

<div class="success alert-success"><?php echo $success; ?><img src="catalog/view/theme/default/image/close.png" alt="" class="close" /></div>

<?php } ?>
</div>
       </div>
     </div>


<div class="container">
<div class="row"><h1 style="margin-top: 10px;" class="col-xs-12 tittle clrorg cps"><?php echo $heading_title; ?></h1>
</div>
</div>



<?php echo $column_left; ?><?php echo $column_right; ?>

<div id="content" class="container mb28"><?php echo $content_top; ?>

  <?php if ($products) { ?>
<div class="row">
<div class="col-xs-12">
<div class="table-responsive">
  <table class="compare-info table table-bordered"  >

    <thead>

      <tr>

        <td  class="compare-attribute" colspan="<?php echo count($products) + 1; ?>"><strong class="text-uppercase"><?php echo $text_product; ?></strong></td>

      </tr>

    </thead>

    <tbody>

      <tr>

        <td ><?php echo $text_name; ?></td>

        <?php foreach ($products as $product) { ?>

        <td  class="name product-name"><a href="<?php echo $products[$product['product_id']]['href']; ?>" class="clrorg"><?php echo $products[$product['product_id']]['name']; ?></a></td>

        <?php } ?>

      </tr>

      <tr>

        <td ><?php echo $text_image; ?></td>

        <?php foreach ($products as $product) { ?>

        <td ><?php if ($products[$product['product_id']]['thumb']) { ?>

          <img src="<?php echo $products[$product['product_id']]['thumb']; ?>" alt="<?php echo $products[$product['product_id']]['name']; ?>" />

          <?php } ?></td>

        <?php } ?>

      </tr>

      <tr>

        <td ><?php echo $text_price; ?></td>

        <?php foreach ($products as $product) { ?>

        <td ><?php if ($products[$product['product_id']]['price']) { ?>

          <?php if (!$products[$product['product_id']]['special']) { ?>

          <?php echo $products[$product['product_id']]['price']; ?>

          <?php } else { ?>

          <span class="price-old"><?php echo $products[$product['product_id']]['price']; ?></span> <span class="price-new"><?php echo $products[$product['product_id']]['special']; ?></span>

          <?php } ?>

          <?php } ?></td>

        <?php } ?>

      </tr>

      <tr>

        <td ><?php echo $text_model; ?></td>

        <?php foreach ($products as $product) { ?>

        <td ><?php echo $products[$product['product_id']]['model']; ?></td>

        <?php } ?>

      </tr>

      <tr>

        <td ><?php echo $text_manufacturer; ?></td>

        <?php foreach ($products as $product) { ?>

        <td ><?php echo $products[$product['product_id']]['manufacturer']; ?></td>

        <?php } ?>

      </tr>

      <tr>

        <td ><?php echo $text_availability; ?></td>

        <?php foreach ($products as $product) { ?>

        <td ><?php echo $products[$product['product_id']]['availability']; ?></td>

        <?php } ?>

      </tr>

	  <?php if ($review_status) { ?>

      <tr>

        <td ><?php echo $text_rating; ?></td>

        <?php foreach ($products as $product) { ?>

        <td ><img src="catalog/view/theme/default/image/stars-<?php echo $products[$product['product_id']]['rating']; ?>.png" alt="<?php echo $products[$product['product_id']]['reviews']; ?>" /><br />

          <?php echo $products[$product['product_id']]['reviews']; ?></td>

        <?php } ?>

      </tr>

      <?php } ?>

	  <tr>

        <td ><?php echo $text_summary; ?></td>

        <?php foreach ($products as $product) { ?>

        <td  class="description"><?php echo $products[$product['product_id']]['description']; ?></td>

        <?php } ?>

      </tr>

      <tr>

        <td ><?php echo $text_weight; ?></td>

        <?php foreach ($products as $product) { ?>

        <td ><?php echo $products[$product['product_id']]['weight']; ?></td>

        <?php } ?>

      </tr>

      <tr>

        <td ><?php echo $text_dimension; ?></td>

        <?php foreach ($products as $product) { ?>

        <td ><?php echo $products[$product['product_id']]['length']; ?> x <?php echo $products[$product['product_id']]['width']; ?> x <?php echo $products[$product['product_id']]['height']; ?></td>

        <?php } ?>

      </tr>

    </tbody>

    <?php foreach ($attribute_groups as $attribute_group) { ?>

    <thead>

      <tr>

        <td  class="compare-attribute" colspan="<?php echo count($products) + 1; ?>"><strong><?php echo $attribute_group['name']; ?></strong></td>

      </tr>

    </thead>

    <?php foreach ($attribute_group['attribute'] as $key => $attribute) { ?>

    <tbody>

      <tr>

        <td ><?php echo $attribute['name']; ?></td>

        <?php foreach ($products as $product) { ?>

        <?php if (isset($products[$product['product_id']]['attribute'][$key])) { ?>

        <td ><?php echo $products[$product['product_id']]['attribute'][$key]; ?></td>

        <?php } else { ?>

        <td ></td>

        <?php } ?>

        <?php } ?>

      </tr>

    </tbody>

    <?php } ?>

    <?php } ?>

    <tr>

      <td ></td>

      <?php foreach ($products as $product) { ?>

      <td style="text-align:center;">
      <a type="button" value="" onclick="addToCart('<?php echo $product['product_id']; ?>');" class="button cart continue-btn" style=""/><?php echo $button_cart; ?></a>
      
      <a href="<?php echo $product['remove']; ?>" class="removec-btn" style=""><?php echo $button_remove; ?></a>
      
      </td>

      <?php } ?>

    </tr>

  </table>
  </div>
  </div>
  </div>

  <div class="buttons mb28 text-center">

    <div style="float:left; margin-top:20px"><a href="<?php echo $continue; ?>" class="button continue-btn"><?php echo $button_continue; ?></a></div>

  </div>

  <?php } else { ?>

  <div class="content"><?php echo $text_empty; ?></div>

  <div class="buttons mt25">

    <div class="right"><a href="<?php echo $continue; ?>" class="button continue-btn"><?php echo $button_continue; ?></a></div>

  </div>

  <?php } ?>

  <?php echo $content_bottom; ?></div>

<?php echo $footer; ?>