<?php echo $header; ?>
<?php ini_set("memory_limit","12M"); ?> 
<!-- <link href="catalog/view/javascript/myzoom/jquery.wm-zoom-1.0.css" rel="stylesheet" type="text/css" /> -->
<script type="text/javascript" src="catalog/view/javascript/imagezoom.js"></script>
<div class="container">
  <div class="row">
    <ul class="crumb col-xs-12 mb20">
      <?php foreach ($breadcrumbs as $breadcrumb) { ?> 
      <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
      <?php } ?>
    </ul>
  </div>
</div>
<?php //echo $column_left; ?>
<?php echo $column_right; ?>
<div id="content">
<?php echo $content_top; ?> 
<div class="container">
<div class="row mb20">
  <div class="dvproduct-view col-lg-6 col-md-6 col-sm-12 col-xs-12">
    <div class="product-view col-xs-12">
      
      <!-- thumbnail images -->
      <?php if ($images) { ?>
      <div class="image-additional col-md-3 col-lg-3 col-sm-3 col-xs-12">
        <div class="view row" style="padding:10px;">
          <?php 
            $z =1;
            foreach ($images as $image) { ?>
          <div class="thumbnail">
            <a><img class="zoom-tiny-image leftImg" src="<?php echo $image['thumb']; ?>" alt = "Thumbnail <?=$z?>" value='<?php echo $image['popup']; ?>'/></a>
          </div>
          <?php $z++; } ?>
        </div>
      </div>
      <?php } ?>

     
          <?php if ($thumb || $images) { ?>
           <div class="col-xs-12">
        <div class="full-view " style="margin: 12px 0px;">
          <div class="left">
            <?php if ($thumb) { ?>
            <div class="wm-zoom-container my-zoom">
              <div class="wm-zoom-box">
                <!-- <a href='<?php echo $thumb; ?>' > -->
                <img id='zoom-img' src="<?php echo $thumb; ?>" data-imagezoom="true" data-magnification="2" class="img-responsive" />
                </a> 
              </div>
            </div>
             </div>
        </div>
            <?php } ?>
         
        <?php } ?>
      </div>

    </div>
  </div>

  <!-- start of product details -->
  <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
    <div class="cart-detail col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <div class="right">
        
        <div class="description row">
          <div class="col-xs-12">
            <h1 style="margin-top:0px;"><?php echo $heading_title; ?></h1>
            <hr class="cart-hr" />
          </div>
          <div class="col-xs-12">
            <p class="product-detail" style="display:block;">
              <?php if ($manufacturer) { ?>
              <?php echo $text_manufacturer; ?>
              <span><?php echo $manufacturer; ?></span><br>
              <?php } ?>
              <span><?php echo $text_model; ?></span> <?php echo $model; ?><br>
              <?php if ($reward) { ?>
              <span><?php echo $text_reward; ?></span> <?php echo $reward; ?><br>
              <?php } ?>
              <span><?php echo $text_stock; ?></span> <?php echo $stock; ?>
            <hr class="cart-hr" />
          </div>
        </div>

        <div class="row">
          <p class="col-xs-6 mb0"><span class="glyphicon glyphicon-heart-empty fs14"></span><a onclick="addToWishList('<?php echo $product_id; ?>');"><?php echo $button_wishlist; ?></a>
          </p>
          <p class="col-xs-6 mb0"><span class="glyphicon glyphicon-refresh fs14"></span>
            <a onclick="addToCompare('<?php echo $product_id; ?>');"><?php echo $button_compare; ?></a>
          </p>
          <div class="col-xs-12">
            <hr class="cart-hr" />
          </div>

        </div>

        
        <div class="row">
          <p class="col-lg-6 col-md-6 col-sm-6 col-xs-6 fs36">
            <?php if ($price) { ?>
            <?php //echo $text_price; ?>
            <?php if (!$special) { ?>
            <?php echo $price; ?>
            <?php } else { ?>
            <span class="price-old"><?php echo $price; ?></span> <span class="price-new"><?php echo $special; ?></span>
            <?php } ?>
            <br>
            <?php if ($tax) { ?>
            <span class="price-tax"><?php echo $text_tax; ?> <?php echo $tax; ?></span><br>
            <?php } ?>
            <?php if ($points) { ?>
            <span class="reward"><small><?php echo $text_points; ?> <?php echo $points; ?></small></span><br>
            <?php } ?>
            <?php if ($discounts) { ?>
            <br>
          <div class="discount">
            <?php foreach ($discounts as $discount) { ?>
            <?php echo sprintf($text_discount, $discount['quantity'], $discount['price']); ?><br>
            <?php } ?>
          </div>
          <?php } ?>
          <?php } ?>
          </p>
        </div>
        <?php if ($profiles): ?>
        <div class="option">
          <h2><span class="required">*</span><?php echo $text_payment_profile ?></h2>
          <br>
          <select name="profile_id">
            <option value=""><?php echo $text_select; ?></option>
            <?php foreach ($profiles as $profile): ?>
            <option value="<?php echo $profile['profile_id'] ?>"><?php echo $profile['name'] ?></option>
            <?php endforeach; ?>
          </select>
          <br>
          <br>
          <span id="profile-description"></span>
          <br>
          <br>
        </div>
        <?php endif; ?>
        <?php if ($options) { ?>
        <div class="options">
          <h2><?php echo $text_option; ?></h2>
          <br>
          <?php foreach ($options as $option) { ?>
          <?php if ($option['type'] == 'select') { ?>
          <div id="option-<?php echo $option['product_option_id']; ?>" class="option">
            <?php if ($option['required']) { ?>
            <span class="required">*</span>
            <?php } ?>
            <b><?php echo $option['name']; ?>:</b><br>
            <select name="option[<?php echo $option['product_option_id']; ?>]">
              <option value=""><?php echo $text_select; ?></option>
              <?php foreach ($option['option_value'] as $option_value) { ?>
              <option value="<?php echo $option_value['product_option_value_id']; ?>"><?php echo $option_value['name']; ?>
                <?php if ($option_value['price']) { ?>
                (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                <?php } ?>
              </option>
              <?php } ?>
            </select>
          </div>
          <br>
          <?php } ?>
          <?php if ($option['type'] == 'radio') { ?>
          <div id="option-<?php echo $option['product_option_id']; ?>" class="option">
            <?php if ($option['required']) { ?>
            <span class="required">*</span>
            <?php } ?>
            <b><?php echo $option['name']; ?>:</b><br>
            <?php foreach ($option['option_value'] as $option_value) { ?>
            <input type="radio" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" id="option-value-<?php echo $option_value['product_option_value_id']; ?>" />
            <label for="option-value-<?php echo $option_value['product_option_value_id']; ?>"><?php echo $option_value['name']; ?>
            <?php if ($option_value['price']) { ?>
            (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
            <?php } ?>
            </label>
            <br>
            <?php } ?>
          </div>
          <br>
          <?php } ?>
          <?php if ($option['type'] == 'checkbox') { ?>
          <div id="option-<?php echo $option['product_option_id']; ?>" class="option">
            <?php if ($option['required']) { ?>
            <span class="required">*</span>
            <?php } ?>
            <b><?php echo $option['name']; ?>:</b><br>
            <?php foreach ($option['option_value'] as $option_value) { ?>
            <input type="checkbox" name="option[<?php echo $option['product_option_id']; ?>][]" value="<?php echo $option_value['product_option_value_id']; ?>" id="option-value-<?php echo $option_value['product_option_value_id']; ?>" />
            <label for="option-value-<?php echo $option_value['product_option_value_id']; ?>"><?php echo $option_value['name']; ?>
            <?php if ($option_value['price']) { ?>
            (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
            <?php } ?>
            </label>
            <br>
            <?php } ?>
          </div>
          <br>
          <?php } ?>
          <?php if ($option['type'] == 'image') { ?>
          <div id="option-<?php echo $option['product_option_id']; ?>" class="option">
            <?php if ($option['required']) { ?>
            <span class="required">*</span>
            <?php } ?>
            <b><?php echo $option['name']; ?>:</b><br>
            <table class="option-image">
              <?php foreach ($option['option_value'] as $option_value) { ?>
              <tr>
                <td style="width: 1px;"><input type="radio" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" id="option-value-<?php echo $option_value['product_option_value_id']; ?>" /></td>
                <td><label for="option-value-<?php echo $option_value['product_option_value_id']; ?>"><img src="<?php echo $option_value['image']; ?>" alt="<?php echo $option_value['name'] . ($option_value['price'] ? ' ' . $option_value['price_prefix'] . $option_value['price'] : ''); ?>" /></label></td>
                <td><label for="option-value-<?php echo $option_value['product_option_value_id']; ?>"><?php echo $option_value['name']; ?>
                  <?php if ($option_value['price']) { ?>
                  (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                  <?php } ?>
                  </label>
                </td>
              </tr>
              <?php } ?>
            </table>
          </div>
          <br>
          <?php } ?>
          <?php if ($option['type'] == 'text') { ?>
          <div id="option-<?php echo $option['product_option_id']; ?>" class="option">
            <?php if ($option['required']) { ?>
            <span class="required">*</span>
            <?php } ?>
            <b><?php echo $option['name']; ?>:</b><br>
            <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['option_value']; ?>" />
          </div>
          <br>
          <?php } ?>
          <?php if ($option['type'] == 'textarea') { ?>
          <div id="option-<?php echo $option['product_option_id']; ?>" class="option">
            <?php if ($option['required']) { ?>
            <span class="required">*</span>
            <?php } ?>
            <b><?php echo $option['name']; ?>:</b><br>
            <textarea name="option[<?php echo $option['product_option_id']; ?>]" cols="40" rows="5"><?php echo $option['option_value']; ?></textarea>
          </div>
          <br>
          <?php } ?>
          <?php if ($option['type'] == 'file') { ?>
          <div id="option-<?php echo $option['product_option_id']; ?>" class="option">
            <?php if ($option['required']) { ?>
            <span class="required">*</span>
            <?php } ?>
            <b><?php echo $option['name']; ?>:</b><br>
            <input type="button" value="<?php echo $button_upload; ?>" id="button-option-<?php echo $option['product_option_id']; ?>" class="button">
            <input type="hidden" name="option[<?php echo $option['product_option_id']; ?>]" value="" />
          </div>
          <br>
          <?php } ?>
          <?php if ($option['type'] == 'date') { ?>
          <div id="option-<?php echo $option['product_option_id']; ?>" class="option">
            <?php if ($option['required']) { ?>
            <span class="required">*</span>
            <?php } ?>
            <b><?php echo $option['name']; ?>:</b><br>
            <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['option_value']; ?>" class="date" />
          </div>
          <br>
          <?php } ?>
          <?php if ($option['type'] == 'datetime') { ?>
          <div id="option-<?php echo $option['product_option_id']; ?>" class="option">
            <?php if ($option['required']) { ?>
            <span class="required">*</span>
            <?php } ?>
            <b><?php echo $option['name']; ?>:</b><br>
            <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['option_value']; ?>" class="datetime" />
          </div>
          <br>
          <?php } ?>
          <?php if ($option['type'] == 'time') { ?>
          <div id="option-<?php echo $option['product_option_id']; ?>" class="option">
            <?php if ($option['required']) { ?>
            <span class="required">*</span>
            <?php } ?>
            <b><?php echo $option['name']; ?>:</b><br>
            <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['option_value']; ?>" class="time" />
          </div>
          <br>
          <?php } ?>
          <?php } ?>
        </div>
        <?php } ?>
        <div class="product-info row">
          <div class="col-lg-4 col-md-4 col-sm-4 col-xs-5 dvCartNo">
            <form style="margin-top:40px">
              <fieldset>
                <div class="form-group cart-add-btn">
                  <!--<input id="colorful" class="form-control no-of-cart" type="number" value="0" min="1" max="10" />-->
                  <div><?php //echo $text_qty; ?>
                    <input id="colorful"  type="text" name="quantity" size="2" value="<?php echo $minimum; ?>" class="form-control no-of-cart"  style="text-align:center"/>
                    <input type="hidden" name="product_id" size="2" value="<?php echo $product_id; ?>" />
                  </div>
              </fieldset>
            </form>
            </div>
            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-7 pull-right dvCartButton">
              <div class="row">
                <div class="col-lg-12 col-md-12 col-xs-12" style="margin-bottom:16px">
                  <input type="button" value="<?php echo $button_cart; ?>" id="button-cart" class="add-cart" />
                </div>
              </div>
              <div class="row">
                <div class="col-lg-12 col-md-12 col-xs-12">
                  <input type="button" value="<?php //echo $button_cart; ?>Buy Now" id="button-carts" class="buy-cart" />
                </div>
              </div>
            </div>
          </div>
          <div>
            <?php if ($minimum > 1) { ?>
            <div class="minimum"><?php echo $text_minimum; ?></div>
            <?php } ?>
          </div>
        

        <div class="row">

        <!-- write a review -->
        <?php if ($review_status) { ?>
        <div class="review col-sm-8">
          <div>
            <img src="catalog/view/theme/default/image/stars-<?php echo $rating; ?>.png" alt="<?php echo $reviews; ?>" />&nbsp;&nbsp;<a onclick="$('a[href=\'#tab-review\']').trigger('click');"><?php echo $reviews; ?></a>&nbsp;&nbsp;|&nbsp;&nbsp;<a onclick="$('a[href=\'#tab-review\']').trigger('click');"><?php echo $text_write; ?></a>
          </div>
        </div>
        <?php } ?>
        <div class="share col-sm-4">
          <!-- AddThis Button BEGIN -->
          <div class="addthis_default_style">
            <a class="addthis_button_facebook" target="_blank"></a> 
            <a class="addthis_button_twitter"></a>
            <a class="addthis_button_google"></a> 
          </div>
          <script type="text/javascript" src="//s7.addthis.com/js/250/addthis_widget.js"></script> 
          <!-- AddThis Button END --> 
        </div>
      </div>
      </div>
      </div>

    </div>
  </div>

  <!-- description reviews specs -->

  <div id="tabs" class="htabs detail-tab dvProdDes-DetailTab row">
    <div class="col-md-2 col-sm-3 col-lg-2 col-xs-12"><a href="#tab-description" class="tabselected"><?php echo $tab_description; ?></a></div>
    <?php if ($attribute_groups) { ?>
    <div class="col-md-2 col-sm-3 col-lg-2 col-xs-12"><a href="#tab-attribute"><?php echo $tab_attribute; ?></a></div>
    <?php } ?>
    <?php if ($review_status) { ?>
    <div class="col-md-3 col-sm-3 col-lg-3 col-xs-12"><a href="#tab-review"><?php echo $tab_review; ?></a></div>
    <?php } ?>
    <?php if ($products) { ?>
    <a href="#tab-related"><?php echo $tab_related; ?> (<?php echo count($products); ?>)</a>
    <?php } ?>
  </div>
  <div id="tab-description" class="tab-content mb28">
    <?php echo $description; ?>
  </div>
  <?php if ($attribute_groups) { ?>
  <div id="tab-attribute" class="tab-content dvTblScroll" style="margin-bottom:30px;width:100%;">
    <table class="attribute table-bordered" style="width:100%" cellpadding="10" cellspacing="5">
      <?php foreach ($attribute_groups as $attribute_group) { ?>
      <thead style="background-color: #f2f2f2;
        font-weight: bold;
        text-transform: uppercase;">
        <tr>
          <td colspan="2" style="height: 30px; padding-left: 7px;"><?php echo $attribute_group['name']; ?></td>
        </tr>
      </thead>
      <tbody>
        <?php foreach ($attribute_group['attribute'] as $attribute) { ?>
        <tr style="height: 28px;">
          <td style="padding: 3px 8px;"><?php echo $attribute['name']; ?></td>
          <td style="padding: 3px 8px;"><?php echo $attribute['text']; ?></td>
        </tr>
        <?php } ?>
      </tbody>
      <?php } ?>
    </table>
  </div>
  <?php } ?>
  <?php if ($review_status) { ?>
  <div id="tab-review" class="tab-content mb28">
    <div id="review"></div>
    <h4 id="review-title"><strong><?php echo $text_write; ?></strong></h4>
    <p style="margin: 10px 0 5px;"><?php echo $entry_name; ?></p>
    <p><input type="text" name="name" value="" class="login col-lg-4 col-md-5 col-sm-6 col-xs-9" /></p>
    <div class="clearfix"></div>
    <p style="margin: 10px 0 5px;"><?php echo $entry_review; ?></p>
    <p><textarea name="text" cols="40" rows="8" style="width: 98%;"></textarea></p>
    <p style="font-size: 14px;"><?php /*echo $text_note; */ ?><span style="font-style:bold;">Note:</span> Review Text must be between 25 and 1000 characters!</p>
    <div class="clearfix"></div>
    <p><strong><?php echo $entry_rating; ?></strong> <span><?php echo $entry_bad; ?></span>
      <input type="radio" name="rating" value="1" style="top: 2px;position: relative;" />
      &nbsp;
      <input type="radio" name="rating" value="2" style="top: 2px;position: relative;" />
      &nbsp;
      <input type="radio" name="rating" value="3" style="top: 2px;position: relative;" />
      &nbsp;
      <input type="radio" name="rating" value="4" style="top: 2px;position: relative;" />
      &nbsp;
      <input type="radio" name="rating" value="5" style="top: 2px;position: relative;" />
      &nbsp;<span><?php echo $entry_good; ?></span>
    </p>
    <div class="clearfix"></div>
    <p style="margin: 10px 0 5px;"><?php echo $entry_captcha; ?></p>
    <p><input type="text" name="captcha" value="" class="login col-lg-3" /></p>
    <div class="clearfix" style="margin-bottom: 20px;"></div>
    <p><img src="index.php?route=product/product/captcha" alt="" id="captcha" /></p>
    <div class="clearfix" style="margin-bottom: 10px;"></div>
    <div class="buttons">
      <div class="right"><a id="button-review" class="button continue-btn">Submit</a></div>
    </div>
  </div>
  <?php } ?>
  <?php if ($products) { ?>
  <div id="tab-related" class="tab-content">
    <div class="box-product">
      <?php foreach ($products as $product) { ?>
      <div>
        <?php if ($product['thumb']) { ?>
        <div class="image"><a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" /></a></div>
        <?php } ?>
        <div class="name"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></div>
        <?php if ($product['price']) { ?>
        <div class="price">
          <?php if (!$product['special']) { ?>
          <?php echo $product['price']; ?>
          <?php } else { ?>
          <span class="price-old"><?php echo $product['price']; ?></span> <span class="price-new"><?php echo $product['special']; ?></span>
          <?php } ?>
        </div>
        <?php } ?>
        <?php if ($product['rating']) { ?>
        <div class="rating"><img src="catalog/view/theme/default/image/stars-<?php echo $product['rating']; ?>.png" alt="<?php echo $product['reviews']; ?>" /></div>
        <?php } ?>
        <a onclick="addToCart('<?php echo $product['product_id']; ?>');" class="button"><?php echo $button_cart; ?></a>
      </div>
      <?php } ?>
    </div>
  </div>
  <?php } ?>
  <?php if ($tags) { ?>
  <div class="tags"><b><?php echo $text_tags; ?></b>
    <?php for ($i = 0; $i < count($tags); $i++) { ?>
    <?php if ($i < (count($tags) - 1)) { ?>
    <a href="<?php echo $tags[$i]['href']; ?>"><?php echo $tags[$i]['tag']; ?></a>,
    <?php } else { ?>
    <a href="<?php echo $tags[$i]['href']; ?>"><?php echo $tags[$i]['tag']; ?></a>
    <?php } ?>
    <?php } ?>
  </div>
  <?php } ?>
  <?php echo $content_bottom; ?>
</div>

<script type="text/javascript">
  $(document).ready(function(e) {
      
  }); 
  
</script>  

<script type="text/javascript"><!--
  $(document).ready(function() {
    $('.colorbox').colorbox({
      overlayClose: true,
      opacity: 0.5,
      rel: "colorbox"
    });
  });
  //--></script> 

<script type="text/javascript"><!--
  $('select[name="profile_id"], input[name="quantity"]').change(function(){
      $.ajax({
      url: 'index.php?route=product/product/getRecurringDescription',
      type: 'post',
      data: $('input[name="product_id"], input[name="quantity"], select[name="profile_id"]'),
      dataType: 'json',
          beforeSend: function() {
              $('#profile-description').html('');
          },
      success: function(json) {
        $('.success, .warning, .attention, information, .error').remove();
              
        if (json['success']) {
                  $('#profile-description').html(json['success']);
        } 
      }
    });
  });
      
  $('#button-cart,#button-carts').bind('click', function() {
  var thisid=$(this).attr('id');
  console.log(thisid);
  var btnname = $(this).val();
  
    $.ajax({
      url: 'index.php?route=checkout/cart/add',
      type: 'post',
      data: $('.product-info input[type=\'text\'], .product-info input[type=\'hidden\'], .product-info input[type=\'radio\']:checked, .product-info input[type=\'checkbox\']:checked, .product-info select, .product-info textarea'),
      dataType: 'json',
      success: function(json) {
      
        $('.success, .warning, .attention, information, .error').remove();
        
        if (json['error']) {
          if (json['error']['option']) {
            for (i in json['error']['option']) {
              $('#option-' + i).after('<span class="error">' + json['error']['option'][i] + '</span>');
            }
          }
                  
                  if (json['error']['profile']) {
                      $('select[name="profile_id"]').after('<span class="error">' + json['error']['profile'] + '</span>');
                  }
        } 
        
        if (json['success']) {
  
          if(thisid!='button-carts'){
  
          $('#notification').html('<div class="success" style="display: none;">' + json['success'] + '<img src="catalog/view/theme/default/image/close.png" alt="" class="close" /></div>');
            
          $('.success').fadeIn('slow');
  $('html, body').animate({ scrollTop: 0 }, 'slow');
  }
          
          
                  
          $('#cart-total').html(json['total']);
          
          
  
                                  if(btnname == 'Buy Now')
          {
          window.location.href = "http://www.cronaz.com/index.php?route=checkout/cart";
          } 
                  
        } 
      }
    });
  });
  //--></script>
<?php if ($options) { ?>
<!--<script type="text/javascript" src="catalog/view/javascript/jquery/ajaxupload.js"></script>
  <?php foreach ($options as $option) { ?>
  <?php if ($option['type'] == 'file') { ?>
  <script type="text/javascript"><!--
  new AjaxUpload('#button-option-<?php echo $option['product_option_id']; ?>', {
    action: 'index.php?route=product/product/upload',
    name: 'file',
    autoSubmit: true,
    responseType: 'json',
    onSubmit: function(file, extension) {
      $('#button-option-<?php echo $option['product_option_id']; ?>').after('<img src="catalog/view/theme/default/image/loading.gif" class="loading" style="padding-left: 5px;" />');
      $('#button-option-<?php echo $option['product_option_id']; ?>').attr('disabled', true);
    },
    onComplete: function(file, json) {
      $('#button-option-<?php echo $option['product_option_id']; ?>').attr('disabled', false);
      
      $('.error').remove();
      
      if (json['success']) {
        alert(json['success']);
        
        $('input[name=\'option[<?php echo $option['product_option_id']; ?>]\']').attr('value', json['file']);
      }
      
      if (json['error']) {
        $('#option-<?php echo $option['product_option_id']; ?>').after('<span class="error">' + json['error'] + '</span>');
      }
      
      $('.loading').remove(); 
    }
  });
  //--></script>-->
<?php } ?>
<?php } ?>
<?php } ?>
<script type="text/javascript"><!--
  $('#review .pagination a').live('click', function() {
    $('#review').fadeOut('slow');
      
    $('#review').load(this.href);
    
    $('#review').fadeIn('slow');
    
    return false;
  });     
  
  $('#review').load('index.php?route=product/product/review&product_id=<?php echo $product_id; ?>');
  
  $('#button-review').bind('click', function() {
    $.ajax({
      url: 'index.php?route=product/product/write&product_id=<?php echo $product_id; ?>',
      type: 'post',
      dataType: 'json',
      data: 'name=' + encodeURIComponent($('input[name=\'name\']').val()) + '&text=' + encodeURIComponent($('textarea[name=\'text\']').val()) + '&rating=' + encodeURIComponent($('input[name=\'rating\']:checked').val() ? $('input[name=\'rating\']:checked').val() : '') + '&captcha=' + encodeURIComponent($('input[name=\'captcha\']').val()),
      beforeSend: function() {
        $('.success, .warning').remove();
        $('#button-review').attr('disabled', true);
        $('#review-title').after('<div class="attention"><img src="catalog/view/theme/default/image/loading.gif" alt="" /> <?php echo $text_wait; ?></div>');
      },
      complete: function() {
        $('#button-review').attr('disabled', false);
        $('.attention').remove();
      },
      success: function(data) {
        if (data['error']) {
          $('#review-title').after('<div class="warning alert-warning">' + data['error'] + '</div>');
        }
        
        if (data['success']) {
          $('#review-title').after('<div class="success alert-success">' + data['success'] + '</div>');
                  
          $('input[name=\'name\']').val('');
          $('textarea[name=\'text\']').val('');
          $('input[name=\'rating\']:checked').attr('checked', '');
          $('input[name=\'captcha\']').val('');
          //$('html, body').animate({ scrollTop: 0 }, 'slow'); 
        }
      }
    });
  });
  //--></script> 
<script type="text/javascript"><!--
  $('#tabs a').tabs();
  //--></script> 
<script type="text/javascript" src="catalog/view/javascript/jquery/ui/jquery-ui-timepicker-addon.js"></script> 
<script type="text/javascript"><!--
  $(document).ready(function() {
    if ($.browser.msie && $.browser.version == 6) {
      $('.date, .datetime, .time').bgIframe();
    }
  
    $('.date').datepicker({dateFormat: 'yy-mm-dd'});
    $('.datetime').datetimepicker({
      dateFormat: 'yy-mm-dd',
      timeFormat: 'h:m'
    });
    $('.time').timepicker({timeFormat: 'h:m'});
    
    $("#tabs a").click(function(e) {
       $("#tabs a").removeClass("tabselected");
          $(this).addClass("tabselected");
      });
    
    
    $(".thumbimg").click(function(e) {
    e.preventDefault();
      var timg=$(this).attr("href");
    //alert(timg);
    $("#mainimg").attr("href",timg)
    $("#mainimg img").attr("src",timg);
    
  }); 
    
    
  });
  //--></script> 

  <script type="text/javascript">

$(document).ready(function () {
  
  $("#colorful").keypress(function (e) {
  
     if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
               return false;
    }
   });
});
</script>
<?php echo $footer; ?>

