<div class="container">
<div class="row mb20">

<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
<ul class="list-group">


<?php foreach ($categories as $category) { ?>

<li class="list-group-item">

<?php if ($category['category_id'] == $category_id) { ?>

<a href="<?php echo $category['href']; ?>" class="hilight"><?php echo $category['name']; ?></a>

<?php } else { ?>

<a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a>

<?php } ?>

<?php if ($category['children']) { ?>
<span class="glyphicon glyphicon-plus plus catog"></span>

<ul class="list-group m0" style="display:none;">

<?php foreach ($category['children'] as $child) { ?>

<li  class="list-group-item">

<?php if ($child['category_id'] == $child_id) { ?>

<a href="<?php echo $child['href']; ?>" class="hilight"> - <?php echo $child['name']; ?></a>

<?php } else { ?>

<a href="<?php echo $child['href']; ?>"> - <?php echo $child['name']; ?></a>

<?php } ?>
</li>

<?php } ?>

</ul>
<?php } ?>

</li>



<?php } ?>
</ul></div>        

 <script type="text/javascript">
$(document).ready(function(e) {
       
  $(".catog").click(function(e) {
   $(this).next().slideToggle("fast");
}); 
});


</script>