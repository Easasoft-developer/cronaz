<?php echo $header; ?>

<style>
@media(max-width:991px)
{
  .dvLoginReg 
  {
    float:left !important;
  }
}
</style>

<div class="container">
   <ul class="crumb">

    <?php foreach ($breadcrumbs as $breadcrumb) { ?>

    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>

    <?php } ?>

  </ul>
</div>

<div class="container">  
  <h1 class="clrorg mt53 tittle cps"><?php echo $heading_title; ?></h1>
</div>



<?php echo $column_left; ?><?php echo $column_right; ?>

<div id="content" class="col-lg-9 col-md-9 col-sm-9 col-xs-12"><?php echo $content_top; ?>

 <!--  <ul class="crumb">

    <?php foreach ($breadcrumbs as $breadcrumb) { ?>

    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>

    <?php } ?>

  </ul> -->

  <!-- <h1 class="clrorg mt53 tittle cps"><?php echo $heading_title; ?></h1> -->

  <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">

    <div class="content row">

      <table class="form">

        <tr>

          <td><p class="mr10" style="margin-top: 6px;"><?php echo $entry_newsletter; ?></p></td>

          <td><?php if ($newsletter) { ?>

            <input type="radio" name="newsletter" value="1" checked="checked" />

            <span><?php echo $text_yes; ?></span>&nbsp;

            <input type="radio" name="newsletter" value="0" />

            <span><?php echo $text_no; ?></span>

            <?php } else { ?>

            <input type="radio" name="newsletter" value="1" />

            <span><?php echo $text_yes; ?></span>&nbsp;

            <input type="radio" name="newsletter" value="0" checked="checked" />

            <span><?php echo $text_no; ?></span>

            <?php } ?></td>

        </tr>

      </table>

    </div>

    <div class="buttons mt25 row">

      <div class="pull-left"><!-- <a href="<?php echo $back; ?>" class="button continue-btn"><?php echo $button_back; ?></a> -->
      
      <input type="submit" value="<?php //echo $button_continue; ?>Update" class="button continue-btn" />
      </div>

      <div class="pull-right"></div>

    </div>

  </form>
  </div>

  <?php echo $content_bottom; ?></div>

<?php echo $footer; ?>