<?php echo $header; ?>

<div class="container"><!--Custom Code-->
 <ul class="crumb hidden-xs">

    <?php foreach ($breadcrumbs as $breadcrumb) { ?>

    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>

    <?php } ?>

  </ul>
</div>

<div class="container"><h1 style="margin-top:10px;margin-bottom:30px;" class="clrorg mt53 tittle cps"><?php echo $heading_title; ?></h1></div>

<?php echo $column_left; ?><?php echo $column_right; ?>

<div id="content" class="row col-lg-9 col-md-9 col-sm-9 col-xs-12"><?php echo $content_top; ?>

 

  <!-- <h1 class="clrorg mt53 tittle cps"><?php echo $heading_title; ?></h1> -->

  <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">

    <p class="f14"><?php echo $text_email; ?></p>
    
    <?php if ($error_warning) { ?>

<div class="warning cred"><?php echo $error_warning; ?></div>

<?php } ?>

    <h4><strong><?php echo $text_your_email; ?></strong></h4>

    <div class="content">

      <div class="form">

        <div class="row">

          <div class="col-md-3 col-lg-3 col-sm-3 col-xs-12"><p class="mr10"><?php echo $entry_email; ?></p></div>

          <div class="col-md-6 col-lg-6 col-sm-6 col-xs-12"><input type="text" name="email" class="col-md-12 col-lg-12 col-sm-12 col-xs-12" value="" size="50" /></div>

        </div>

      </div>

    </div>

    <div class="buttons mt25">

      <div class="pull-left mr10"><a href="<?php echo $back; ?>" class="button continue-btn"><?php echo $button_back; ?></a></div>

      <div class="pull-left">

        <input type="submit" value="Submit" class="button continue-btn" />

      </div>

    </div>

  </form>
  </div>

  <?php echo $content_bottom; ?></div>

<?php echo $footer; ?>