<?php echo $header; ?><?php echo $column_left; ?><?php echo $column_right; ?>

<div id="content" class="col-lg-9"><?php echo $content_top; ?>

  <ul class="crumb">

    <?php foreach ($breadcrumbs as $breadcrumb) { ?>

    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>

    <?php } ?>

  </ul>

  <h1 class="clrorg mt53 tittle cps"><?php echo $heading_title; ?></h1>

  <table class="list table-striped mb20" width="100%">

    <thead>

      <tr>

        <td class="left" colspan="2"><h4><strong><?php echo $text_order_detail; ?></strong></h4></td>

      </tr>

    </thead>

    <tbody>

      <tr>

        <td style="padding:10px;" class="left" style="width: 50%;"><?php if ($invoice_no) { ?>

          <b style="display: inline-block;padding-bottom: 10px;"><?php echo $text_invoice_no; ?></b> <?php echo $invoice_no; ?><br />

          <?php } ?>

          <b><?php echo $text_order_id; ?></b> #<?php echo $order_id; ?><br />

          <b><?php echo $text_date_added; ?></b> <?php echo $date_added; ?></td>

        <td style="padding:10px;" class="left" style="width: 50%;"><?php if ($payment_method) { ?>

          <b style="display: inline-block;padding-bottom: 10px;"><?php echo $text_payment_method; ?></b> <?php echo $payment_method; ?><br />

          <?php } ?>

          <?php if ($shipping_method) { ?>

          <b><?php echo $text_shipping_method; ?></b> <?php echo $shipping_method; ?>

          <?php } ?></td>

      </tr>

    </tbody>

  </table>

  <table class="list table-bordered mb20" width="100%">

    <thead>

      <tr>

        <td style="padding:10px;" class="left"><?php echo $text_payment_address; ?></td>

        <?php if ($shipping_address) { ?>

        <td style="padding:10px;" class="left"><?php echo $text_shipping_address; ?></td>

        <?php } ?>

      </tr>

    </thead>

    <tbody>

      <tr>

        <td style="padding:10px;" class="left"><?php echo $payment_address; ?></td>

        <?php if ($shipping_address) { ?>

        <td style="padding:10px;" class="left"><?php echo $shipping_address; ?></td>

        <?php } ?>

      </tr>

    </tbody>

  </table>

  <table class="list table-bordered" width="100%">

    <thead>

      <tr>

        <td style="padding:10px;" class="left"><?php echo $column_name; ?></td>

        <td style="padding:10px;" class="left"><?php echo $column_model; ?></td>

        <td style="padding:10px;" class="right"><?php echo $column_quantity; ?></td>

        <td style="padding:10px;" class="right"><?php echo $column_price; ?></td>

        <td style="padding:10px;" class="right"><?php echo $column_total; ?></td>

        <?php if ($products) { ?>

        <td style="padding:10px;" style="width: 1px;"></td>

        <?php } ?>

      </tr>

    </thead>

    <tbody>

      <?php foreach ($products as $product) { ?>

      <tr>

        <td style="padding:10px;" class="left"><?php echo $product['name']; ?>

          <?php foreach ($product['option'] as $option) { ?>

          <br />

          &nbsp;<small> - <?php echo $option['name']; ?>: <?php echo $option['value']; ?></small>

          <?php } ?></td>

        <td style="padding:10px;" class="left"><?php echo $product['model']; ?></td>

        <td style="padding:10px;" class="right"><?php echo $product['quantity']; ?></td>

        <td style="padding:10px;" class="right"><?php echo $product['price']; ?></td>

        <td style="padding:10px;" class="right"><?php echo $product['total']; ?></td>

        <td style="padding:10px;" class="right"><a href="<?php echo $product['return']; ?>"><img src="catalog/view/theme/default/image/return.png" alt="<?php echo $button_return; ?>" title="<?php echo $button_return; ?>" /></a></td>

      </tr>

      <?php } ?>

      <?php foreach ($vouchers as $voucher) { ?>

      <tr>

        <td style="padding:10px;" class="left"><?php echo $voucher['description']; ?></td>

        <td style="padding:10px;" class="left"></td>

        <td class="right">1</td>

        <td style="padding:10px;" class="right"><?php echo $voucher['amount']; ?></td>

        <td style="padding:10px;" class="right"><?php echo $voucher['amount']; ?></td>

        <?php if ($products) { ?>

        <td style="padding:10px;"></td>

        <?php } ?>

      </tr>

      <?php } ?>

    </tbody>

    <tfoot>

      <?php foreach ($totals as $total) { ?>

      <tr>

        <td style="padding:10px;" colspan="3"></td>

        <td style="padding:10px;" class="right"><b><?php echo $total['title']; ?>:</b></td>

        <td style="padding:10px;" class="right"><?php echo $total['text']; ?></td>

        <?php if ($products) { ?>

        <td style="padding:10px;"></td>

        <?php } ?>

      </tr>

      <?php } ?>

    </tfoot>

  </table>

  <?php if ($comment) { ?>

  <table class="list">

    <thead>

      <tr>

        <td style="padding:10px;" class="left"><?php echo $text_comment; ?></td>

      </tr>

    </thead>

    <tbody>

      <tr>

        <td style="padding:10px;" class="left"><?php echo $comment; ?></td>

      </tr>

    </tbody>

  </table>

  <?php } ?>

  <?php if ($histories) { ?>

  <h4 class="mt20"><strong><?php echo $text_history; ?></strong></h4>

  <table class="list table-bordered mb20" width="100%">

    <thead>

      <tr>

        <td style="padding:10px;" class="left"><?php echo $column_date_added; ?></td>

        <td style="padding:10px;" class="left"><?php echo $column_status; ?></td>

        <td style="padding:10px;" class="left"><?php echo $column_comment; ?></td>

      </tr>

    </thead>

    <tbody>

      <?php foreach ($histories as $history) { ?>

      <tr>

        <td style="padding:10px;" class="left"><?php echo $history['date_added']; ?></td>

        <td style="padding:10px;" class="left"><?php echo $history['status']; ?></td>

        <td style="padding:10px;" class="left"><?php echo $history['comment']; ?></td>

      </tr>

      <?php } ?>

    </tbody>

  </table>

  <?php } ?>

  <div class="buttons">

    <div class="right"><a href="<?php echo $continue; ?>" class="button continue-btn"><?php echo $button_continue; ?></a></div>

  </div>
  </div>

  <?php echo $content_bottom; ?></div>

<?php echo $footer; ?> 