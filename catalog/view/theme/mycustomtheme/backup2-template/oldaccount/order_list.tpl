<?php echo $header; ?><?php echo $column_left; ?><?php echo $column_right; ?>

<div id="content" class="col-lg-9"><?php echo $content_top; ?>

  <ul class="crumb">

    <?php foreach ($breadcrumbs as $breadcrumb) { ?>

    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>

    <?php } ?>

  </ul>

  <h1 class="clrorg"><?php echo $heading_title; ?></h1>
  
  <?php if ($orders) { ?>

  <?php foreach ($orders as $order) { ?>

  <div class="order-list">
  <table class="table-striped" width="100%">
  <tbody>
  <tr>

    <td class="order-id"><b><?php echo $text_order_id; ?></b> #<?php echo $order['order_id']; ?></td>
	
    <td class="order-status"><b><?php echo $text_status; ?></b> <?php echo $order['status']; ?></td>
    
</tr>
<tr>
    <div class="order-content">

      <td><div><b><?php echo $text_date_added; ?></b> <?php echo $order['date_added']; ?><br />

        <b><?php echo $text_products; ?></b> <?php echo $order['products']; ?></div></td>

      <td><div><b><?php echo $text_customer; ?></b> <?php echo $order['name']; ?><br />

        <b><?php echo $text_total; ?></b> <?php echo $order['total']; ?></div></td>
       

      <td><div class="order-info"><a href="<?php echo $order['href']; ?>"><img src="catalog/view/theme/default/image/info.png" alt="<?php echo $button_view; ?>" title="<?php echo $button_view; ?>" /></a>&nbsp;&nbsp;<a href="<?php echo $order['reorder']; ?>"><img src="catalog/view/theme/default/image/reorder.png" alt="<?php echo $button_reorder; ?>" title="<?php echo $button_reorder; ?>" /></a></div></td>
</tr>
    </div>
    </tbody>
  </table>

  </div>
  

  <?php } ?>

  

  <?php } else { ?>

  <div class="content"><?php echo $text_empty; ?></div>

  <?php } ?>
  

  <div class="buttons mt25">

    <div class="right"><a href="<?php echo $continue; ?>" class="button continue-btn"><?php echo $button_continue; ?></a></div>

  </div>
  </div>
<div class="pagination"><?php echo $pagination; ?></div>
  <?php echo $content_bottom; ?></div>

<?php echo $footer; ?>