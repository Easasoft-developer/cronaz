<?php echo $header; ?>

<?php if ($error_warning) { ?>

<div class="warning"><?php echo $error_warning; ?></div>

<?php } ?>

<?php echo $column_left; ?><?php echo $column_right; ?>

<div id="content" class="col-lg-9"><?php echo $content_top; ?>

  <ul class="crumb">

    <?php foreach ($breadcrumbs as $breadcrumb) { ?>

    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>

    <?php } ?>

  </ul>

  <h1 class="clrorg mt53"><?php echo $heading_title; ?></h1>

  <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">

    <p><?php echo $text_email; ?></p>

    <h2><?php echo $text_your_email; ?></h2>

    <div class="content">

      <table class="form">

        <tr>

          <td><?php echo $entry_email; ?></td>

          <td><input type="text" name="email" value="" /></td>

        </tr>

      </table>

    </div>

    <div class="buttons mt25">

      <div class="pull-left"><a href="<?php echo $back; ?>" class="button continue-btn"><?php echo $button_back; ?></a></div>

      <div class="pull-right">

        <input type="submit" value="<?php echo $button_continue; ?>" class="button continue-btn" />

      </div>

    </div>

  </form>
  </div>

  <?php echo $content_bottom; ?></div>

<?php echo $footer; ?>