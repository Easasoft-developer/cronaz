<?php
class ModelPaymentDandc extends Model {
  public function getMethod($address, $total) {
    $this->load->language('payment/dandc');


    if ($total <= 0) {
			$status = true;
		} else {
			$status = false;
		}
  
    $method_data = array(
      'code'     => 'dandc',
      'title'    => $this->language->get('text_title'),
      'sort_order' => $this->config->get('dandc_sort_order')
    );
  
    return $method_data;
  }
}