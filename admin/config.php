<?php
// HTTP
define('HTTP_SERVER', 'http://www.cronaz.com/admin/');
define('HTTP_CATALOG', 'http://www.cronaz.com/');
//define('HTTP_SERVER', 'http://localhost/cronaz/admin/');
//define('HTTP_CATALOG', 'http://localhost/cronaz/');

// HTTPS
define('HTTPS_SERVER', 'http://www.cronaz.com/admin/');
define('HTTPS_CATALOG', 'http://www.cronaz.com/');

// DIR

define('DIR_APPLICATION', dirname(__FILE__) . '/../admin/');
define('DIR_SYSTEM', dirname(__FILE__) . '/../system/');
define('DIR_DATABASE', dirname(__FILE__) . '/../system/database/');
define('DIR_LANGUAGE', dirname(__FILE__) . '/../admin/language/');
define('DIR_TEMPLATE', dirname(__FILE__) . '/../admin/view/template/');
define('DIR_CONFIG', dirname(__FILE__) . '/../system/config/');
define('DIR_CONTROLLER',dirname(__FILE__) . '/../system/config/');
define('DIR_IMAGE', dirname(__FILE__) . '/../image/');
define('DIR_CACHE', dirname(__FILE__) . '/../system/cache/');
define('DIR_DOWNLOAD', dirname(__FILE__) . '/../download/');
define('DIR_LOGS', dirname(__FILE__) . '/../system/logs/');
define('DIR_CATALOG', dirname(__FILE__) . '/../catalog/');
/*

define('DIR_APPLICATION', 'C:/xampp/htdocs/cronaz/admin/');
define('DIR_SYSTEM', 'C:/xampp/htdocs/cronaz/system/');
define('DIR_DATABASE', 'C:/xampp/htdocs/cronaz/system/database/');
define('DIR_LANGUAGE', 'C:/xampp/htdocs/cronaz/admin/language/');
define('DIR_TEMPLATE', 'C:/xampp/htdocs/cronaz/admin/view/template/');
define('DIR_CONFIG', 'C:/xampp/htdocs/cronaz/system/config/');
define('DIR_CONTROLLER','C:/xampp/htdocs/cronaz/system/config/');
define('DIR_IMAGE', 'C:/xampp/htdocs/cronaz/image/');
define('DIR_CACHE', 'C:/xampp/htdocs/cronaz/system/cache/');
define('DIR_DOWNLOAD', 'C:/xampp/htdocs/cronaz/download/');
define('DIR_LOGS', 'C:/xampp/htdocs/cronaz/system/logs/');
define('DIR_CATALOG', 'C:/xampp/htdocs/cronaz/catalog/');
*/
// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'cronard1_nazuser');
define('DB_PASSWORD', ']0dD$0)8n0vI');
define('DB_DATABASE', 'cronard1_nazdb');
define('DB_PREFIX', 'oc_');
/*

define('DB_DRIVER', 'mysql');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', '');
define('DB_DATABASE', 'cronard1_nazdb');
define('DB_PREFIX', 'oc_');
*/

?>